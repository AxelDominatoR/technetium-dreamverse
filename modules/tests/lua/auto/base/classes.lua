local Tests
local Test

function register_callbacks()
	return
	{
		register_tests = on_register
	}
end

function on_register()
	Tests = _G.Tests
	Test = _G.Tests.Test

	Tests:Add("classes", run_test )
end

function run_test()
	test_class_basics()
	test_class_inheritance()
	test_class_modifiers()
end

-- Common stuff
Class("TestClass")
function TestClass:_init( ... )
	self.property = "TestClass"
	self.overriden_property = "TestClass"
end

function TestClass:Method()
	return "TestClass"
end

function TestClass:OverridenMethod()
	return "TestClass"
end

Class("TestChildClass", TestClass )
function TestChildClass:_init( ... )
	self.overriden_property = "TestChildClass"
end

function TestChildClass:OverridenMethod()
	return "TestChildClass"
end

Class("TestChildChildClass", TestChildClass )

-- Tests
function test_class_basics()
	local T = Test({ description = "Classes - basic functionality"})

	-- Declaration
	T:Run( function() return is_class( TestClass ) end )

	-- Instancing
	local C = TestClass()
	T:Run( function() return C:is_instance_of( TestClass ) end )

	T:Results()
end

function test_class_inheritance()
	local T = Test({ description = "Classes - inheritance"})

	-- Class inheritance
	T:Run( function() return TestChildClass:is_a( TestClass ) end )
	T:Run( function() return TestChildChildClass:is_a( TestClass ) end )
	T:Run( function() return TestChildChildClass:is_a( TestChildClass ) end )

	-- Instance inheritance
	local C = TestChildClass()
	T:Run( function() return C:is_instance_of( TestClass ) end )

	local CC = TestChildChildClass()
	T:Run( function() return CC:is_instance_of( TestClass ) end )
	T:Run( function() return CC:is_instance_of( TestChildClass ) end )
	T:Run( function() return CC:is_instance_of( TestChildChildClass ) end )

	-- Method inheritance
	T:Run( function() return C:Method() == "TestClass" end )
	T:Run( function() return C:OverridenMethod() == "TestChildClass" end )

	-- Methods location
	T:Run( function() return rawget( TestClass, "Method") ~= nil end )
	T:Run( function() return rawget( TestChildClass, "Method") == nil end )
	T:Run( function() return rawget( C, "Method") == nil end )

	T:Run( function() return rawget( TestChildClass, "OverridenMethod") ~= nil end )

	-- Properties location
	local CP = TestClass()
	T:Run( function() return CP.property == "TestClass" end )
	T:Run( function() return C.property == "TestClass" end )
	T:Run( function() return CC.property == "TestClass" end )

	T:Run( function() return CP.overriden_property == "TestClass" end )
	T:Run( function() return C.overriden_property == "TestChildClass" end )
	T:Run( function() return CC.overriden_property == "TestChildClass" end )

	T:Results()
end

function test_class_modifiers()
	local T = Test({ description = "Classes - modifiers"})

	T:Run( function() return false end )

	T:Results()
end