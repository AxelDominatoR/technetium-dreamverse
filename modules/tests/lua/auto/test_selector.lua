--[[
	Test selector
--]]

function module_init()
	log.info("Initializing test loader")

	_G.Tests = Tests()

	CallbackSendOnce("register_tests")
end

function module_load()
	_G.Tests:Run( CommandLine.tests )
end

function register_callbacks()
	return
	{
		framework_end_frame = terminate_tests
	}
end

function terminate_tests()
	log.info("Forcefully terminating tests")
	os.exit()
end

Tests =
{
	_tests = {}
}
Class("Tests")
function Tests:_init( ... )
	self.Test = Test
end

function Tests:Add( name, cb )
	self._tests[ name ] = cb
end

function Tests:Run( requested_tests )
	local tests_to_run = {}

	if ( next( requested_tests ) == nil ) then
		tests_to_run = self._tests
	else
		local test
		for _, t in ipairs( requested_tests ) do
			test = self._tests[ t ]
			if test ~= nil then
				tests_to_run[ t ] = test
			end
		end
	end

	log.banner("Running tests...")

	for k, v in pairs( tests_to_run ) do
		v()
	end

	log.banner_rtl("...done")
end

Test =
{
	description = "Unnamed test",
	passed = true
}
Class("Test")

function Test:Run( test )
	self.passed = self.passed and test()
	if ( self.passed ~= true ) then
		local di = debug.getinfo( 2 )
		log.error("Test failed at", di.source, "line", di.currentline )
	end
end

function Test:Results()
	if self.passed == true then
		log.info("Passed test:", self.description )
	else
		log.error("Failed test:", self.description )
	end
end