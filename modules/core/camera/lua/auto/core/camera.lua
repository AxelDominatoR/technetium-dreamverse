--[[
viewMatrix = {
	rightx,	righty,	rightz,	0
	upx,	upy,	upz,	0
	frontx,	fronty,	frontz,	0
	posx,	posy,	posz,	1
}
--]]
_active					= nil
cache 					= {}

function framework_run()
	CallbackSet("framework_resize",resize)
end

function resize(w,h)
	for alias,camera in pairs(cache) do
		if (camera.onWindowResize) then
			camera:onWindowResize(w,h)
		end
	end
end

-- core.camera(alias,fullpath_to_prefab)
getmetatable(this).__call = function(self,alias,fullpath)
	local str,len = filesystem.read(fullpath)
	if (len == nil or len <= 0) then
		return
	end
	
	local status,result = pcall(assert(loadstring(str,fullpath:sub(-45))))
	if not (status) then
		print("[ERROR] failed to import camera " .. fullpath .. "\n" .. result)
		return
	end

	if (cache[ alias ]) then
		print("Camera already exists with this alias!",alias)
		return
	end
	
	cache[ alias ] = result
	
	return result
end

-- get or set active camera
function active(alias)
	_active = cache[ alias ] or _active
	return _active
end