--[[
	Communication layer with renderer and asset manager to load and cache textures.
	Image loading is multithreaded. A texture that isn't loaded or non-existant should be treated as 'ok' behavior not an error.
	Any systems that display a texture should default to the 'default' texture if image is missing.
--]]
-------------------------------------------------------------------------------
-- API
-------------------------------------------------------------------------------
-- core.import.image(filename)
getmetatable(this).__call = function (self,fname)
	local cache = core.manager.assets.texture.cache
	local texture_info = cache[ fname ]
	if (texture_info ~= nil) then
		return texture_info
	end
	texture_info = cTextureInfo(fname)
	texture_info:open(fname)
	return texture_info
end

function exists(fname_or_alias)
	local cache = core.manager.assets.texture.cache
	return cache[ fname_or_alias ] ~= nil
end

---------------------------------------------------------------------------
-- Callbacks
---------------------------------------------------------------------------
local processing_list = {}
local function framework_update()
	-- further image processing
	for i=#processing_list,1,-1 do
		if (processing_list[ i ]() == true) then
			table.remove(processing_list,i)
		end
	end
end

local function framework_load()
	-- Create a default texture 1x1 pixels of black color
	local w,h = 1,1
	local default_texture_info = cTextureInfo("default")
	default_texture_info.storageSize = w*h*4
	default_texture_info.width = w
	default_texture_info.height = h
	default_texture_info.w = w
	default_texture_info.h = h

	local default_pixels = ffi.new("char[4]",0xffffffff)
	core.renderer.create_texture(default_pixels,default_texture_info)
		
	-- Autoload all texture atlases
	local function on_execute(path,fname,fullpath)
		local xml = core.import.xml(fullpath)
		for i,element in ipairs(xml.data.atlas) do
			if (element.texture) then
				local fname = element[ 1 ].file
				local src_texture_info = this(fname)
				
				-- subimage from src
				for j,texture in ipairs(element.texture) do
					local node = texture[ 1 ]
					local texture_info = core.manager.assets.texture.cache[ node.name ]
					if (texture_info) then
						print("[Warning] Texture alias redifinition!",fname,node.name)
					else
					
						local function process_subimage()
							if (src_texture_info) then
								if (src_texture_info.loaded) then
									local texture_info = cTextureInfo(node.name)
									-- copy info from source texture
									for k,v in pairs(src_texture_info) do
										texture_info[ k ] = v
									end
									local x,y,w,h = tonumber(texture[ 1 ].x),tonumber(texture[ 1 ].y),tonumber(texture[ 1 ].w),tonumber(texture[ 1 ].h)
									if (node.blit == "true") then
										core.renderer.blit_texture(texture_info,src_texture_info,x,y,w,h)
										return true
									end
									texture_info.x = x
									texture_info.y = y
									texture_info.w = w
									texture_info.h = h
								end
							else
								return true -- can't finish processing because image doesn't exist
							end
							return false
						end
						
						-- insert into a list to check each frame
						table.insert(processing_list,process_subimage)
					end
				end
			-- Subimage from given tile width and height
			elseif (element[ 1 ].width and element[ 1 ].height) then
				local fname = element[ 1 ].file
				local src_texture_info = this(fname)
				
				local w = tonumber(element[ 1 ].width)
				local h = tonumber(element[ 1 ].height)
				
				local x,y,id = 0,0,1
				for i=1, src_texture_info.height/h do
					x = 0
					for j=1, src_texture_info.width/w do
						local name = fname .. "." .. id -- Autogenerate alias name for each tile to be *.[index] (ex. tileset1.0, tileset1.20, etc.)
						local function process_subimage()
							if (src_texture_info) then
								if (src_texture_info.loaded) then
									local texture_info = cTextureInfo(name)
									-- copy info from source texture
									for k,v in pairs(src_texture_info) do
										texture_info[ k ] = v
									end
									texture_info.x = x
									texture_info.y = y
									texture_info.w = w
									texture_info.h = h
									return true
								end
							else
								return true -- can't finish processing because image doesn't exist
							end
							return false
						end
						
						-- insert into a list to check each frame
						table.insert(processing_list,process_subimage)
						
						x = x + w
						id = id + 1
					end
					y = y + h
				end
			end
		end
	end
	filesystem.fileForEach("config/texture/atlas",true,{"xml"},on_execute)
end

local function framework_exit()
	local cache = core.manager.assets.texture.cache
	for alias,texture_info in pairs(cache) do
		texture_info:destroy()
	end
end

function framework_run()
	CallbackSet("framework_load",framework_load)
	CallbackSet("framework_update",framework_update,9999)
	CallbackSet("framework_exit",framework_exit)
end

------------------------------------------------------------------------
-- cTextureInfo
------------------------------------------------------------------------
Class "cTextureInfo"
function cTextureInfo:__init(fname_or_alias)
	self.name = fname_or_alias
	self.id = 65535 -- uint16(-1) invalid
	self.format = nil -- handled by renderer open/create image
	self.storageSize = 0
	self.width = 0
	self.height = 0
	self.depth = 1
	self.numLayers = 1
	self.numMips =  1
	self.bitsPerPixel = 32
	self.cubeMap = false
	self.flags = 0
	
	-- uv region
	self.x = 0
	self.y = 0
	self.w = 0
	self.h = 0
	
	self.loaded = false
	core.manager.assets.texture.add(self)
end

function cTextureInfo:open(fname)
	-- TODO: Maybe a skip-thread-pool option?
	core.renderer.open_texture_threaded(fname)
end

function cTextureInfo:draw()
	if not (self.loaded) then
		return
	end
	
	core.renderer.draw_texture(self)
end

function cTextureInfo:getTextureID()
	if (self.loaded) then
		return self.id
	end
	local default_texture_info = core.manager.assets.texture.cache.default
	return default_texture_info.id
end

function cTextureInfo:getRegion()
	return (self.x or 0),(self.y or 0),(self.w or 0),(self.h or 0)
end

function cTextureInfo:getTextureSize()
	return (self.width or 0),(self.height or 0)
end

function cTextureInfo:destroy()
	core.renderer.destroy_texture(self)
	local cache = core.manager.assets.texture.cache
	cache[ self.name ] = nil
end

-- No GC!!! Class creates dummy userdata for __gc metamethod which will prevent passing/copying cTextureInfo to a thread pool
-- function cTextureInfo:__gc()
	-- self:destroy()
-- end