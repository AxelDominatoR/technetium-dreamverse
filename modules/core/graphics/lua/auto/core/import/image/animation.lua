cache = {}

globalState = 1			-- 0- stop  1- play
globalTimeFactor = 1

-- core.import.image.animation(name)
getmetatable(this).__call = function (self,name,no_callbacks)
	if not (cache[name]) then
		print("[Warning] image animation does not exist!",name)
		return -- XXX do we want copies?
	end
	-- Create a copy of the animation data
	return cAnimation(cache[name],not no_callbacks)
end

function framework_run()
	CallbackSet("framework_load",framework_load)
	CallbackSet("framework_update",framework_update)
end

function framework_load()
	-- Autoload all texture animations
	local function on_execute(path,fname,fullpath)
		local xml = core.import.xml(fullpath)
		for i,element in ipairs(xml.data.animation) do
			if (element.frame) then
				if (cache[ element[ 1 ].name ]) then 
					print("Animation redifinition!",fname,element[ 1 ].name)
				else
					local anim = cAnimation(element[ 1 ])
					cache[ element[ 1 ].name ] = anim
					
					for j,frame in ipairs(element.frame) do 
						anim:add(frame[ 1 ])
					end
				end
			end
		end
	end
	filesystem.fileForEach("config/texture/animation",true,{"xml"},on_execute)
end

local lastFrameTime = 0
function framework_update(dt,mx,my)
	if (globalState == 1) then
		local tme = glfw.GetTime()
		local dt = tme - lastFrameTime
		
		-- tick at 1/20th of a second
		if (dt >= 0.05) then
			CallbackSend("image_animation_update",dt,mx,my)
			lastFrameTime = tme
		end
	end
end

function exists(alias)
	return cache[alias] ~= nil
end
-------------------------------------------------
-- cAnimation
-------------------------------------------------
Class "cAnimation"
function cAnimation:__init(copy_from,set_callbacks)
	self.name = copy_from.name
	self.frame = copy_from.frame or {}
	self.durationTotal = copy_from.durationTotal or 0
	self.timer = 0
	self.index = 1
	self.size = copy_from.size or 0
	self.loop = copy_from.loop
	self.state = 1		-- 0-stop 1-play
	self.timeFactor = 1
	if (set_callbacks) then
		CallbackSet("image_animation_update",self)
	end
end

function cAnimation:reset()
	self.timer = 0
	self.index = 1
end

function cAnimation:add(frame)
	local duration = tonumber(frame.duration) or 1
	self.durationTotal = self.durationTotal + duration
	local t = {
		filename = frame.file,
		duration = duration,
		interval = self.durationTotal
	}
	table.insert(self.frame,t)
	self.size = self.size + 1
end

function cAnimation:image_animation_update(dt,mx,my)
	if (self.state == 0) then 
		return false
	end
	
	self.timer = self.timer + (globalTimeFactor * self.timeFactor * dt)
	
	local index = 1
	local last_interval = 0
	for i=self.index,self.size do
		local interval = self.frame[ i ].interval
		if (self.timer > last_interval and self.timer <= interval) then 
			self.index = i
			last_interval = interval
		elseif (self.timer > interval) then
			if (i >= self.size) then
			
				if (self.loop == "true") then --TODO: maybe allow specific number with -1 being loop
					self.index = 1
					self.timer = 0
				end
				
				if (self.onAnimationEnd) then 
					self:onAnimationEnd()
				end
				
				return true
			end
		end
	end
	return false
end

function cAnimation:draw(...)
	if (self.frame[ self.index ]) then 
		local image = core.import.image(self.frame[ self.index ].filename)
		if (image) then
			image:draw(...)
		end
	end
end

function cAnimation:getTextureID()
	local image = core.import.image(self.frame[ self.index ].filename)
	return image:getTextureID()
end

function cAnimation:getRegion()
	local image = core.import.image(self.frame[ self.index ].filename)
	return image:getRegion()
end

function cAnimation:getTextureSize()
	local image = core.import.image(self.frame[ self.index ].filename)
	return image:getTextureSize()
end

function cAnimation:__gc()
	CallbackUnset("image_animation_update",self)
end