local IL = require("ffi_devil") ; if not (IL) then error("Failed to load ffi_devil") end
IL.ilInit() 
local bgfx = require("ffi_bgfx") ; if not (bgfx) then error("Failed to load ffi_bgfx") end

local _caps = 0 -- capabilities flags

local vdecl = nil
local vdecl_h = nil

------------------------------------------------
-- API
------------------------------------------------
function get_video_options()
	local config = core.config.data.renderer
	local vsync = config and config.vsync == true or false
	local msaa = config and tonumber(config.msaa) or 0
	if not (msaa == 2 or msaa == 4 or msaa == 8 or msaa == 16) then
		msaa = 0
	end
	local video_flags = vsync and bgfx.BGFX_RESET_VSYNC or bgfx.BGFX_RESET_NONE
	if (msaa > 0) then
		video_flags = video_flags + bgfx[ "BGFX_RESET_MSAA_X"..msaa ]
	end
	--video_flags = video_flags + bgfx.BGFX_RESET_MAXANISOTROPY
	return video_flags
end

function hasCaps(flag)
	return bit.band(_caps,flag) == 1
end

--[[	create_texture
@ param1: pixel data
@ param2: table containing texture parameters
--]]
function create_texture(pixels,texture_info)
	texture_info.format = texture_info.format or bgfx.BGFX_TEXTURE_FORMAT_RGBA8
	local w = texture_info.width
	local h = texture_info.height
	local layers = hasCaps(bgfx.BGFX_CAPS_TEXTURE_2D_ARRAY) and texture_info.numLayers or 1
	
	local bgfx_owned_mem = bgfx.bgfx_copy(pixels,w*h*4)
	local tex_h = bgfx.bgfx_create_texture_2d(w,h,texture_info.numMips > 1,layers,texture_info.format,texture_info.flags,bgfx_owned_mem)
	texture_info.id = tex_h.idx
	
	--core.manager.assets.texture.add(texture_info)
	
	texture_info.loaded = true
	
	return texture_info
end

--[[	open_texture
@ param1: vfs file path
@ param2: table that will be filled with texture parameters
--]]
function open_texture(filename,texture_info)
	if not (filesystem.exists(filename)) then
		print("doesn't exist",filename)
		return
	end
	
	local buffer,length = filesystem.read(filename)

	local ilImage = ffi.new("ILuint[1]")
	IL.ilGenImages(1, ilImage)
	IL.ilBindImage(ilImage[ 0 ])

	IL.ilLoadL(IL.IL_TYPE_UNKNOWN,buffer,length)
	local err = IL.ilGetError()
	if (err ~= 0) then
		error(IL.iluErrorString(err))
	end

	texture_info = texture_info or {}
	texture_info.name = filename
	texture_info.format = bgfx.BGFX_TEXTURE_FORMAT_RGBA8 -- TODO: Maybe IL to BGFX conversion?
	texture_info.storageSize = IL.ilGetInteger(IL.IL_IMAGE_SIZE_OF_DATA)
	texture_info.width = IL.ilGetInteger(IL.IL_IMAGE_WIDTH)
	texture_info.height = IL.ilGetInteger(IL.IL_IMAGE_HEIGHT)
	texture_info.depth = IL.ilGetInteger(IL.IL_IMAGE_DEPTH)
	texture_info.numLayers = IL.ilGetInteger(IL.IL_NUM_LAYERS) --hasCaps(bgfx.BGFX_CAPS_TEXTURE_2D_ARRAY) and IL.ilGetInteger(IL.IL_NUM_LAYERS) or 1
	texture_info.numMips =  IL.ilGetInteger(IL.IL_NUM_MIPMAPS)
	texture_info.bitsPerPixel = IL.ilGetInteger(IL.IL_IMAGE_BITS_PER_PIXEL)
	texture_info.cubeMap = false -- TODO: IL_IMAGE_CUBEFLAGS
	texture_info.flags = 0 -- bgfx.BGFX_TEXTURE_NONE

	-- uv region
	texture_info.x = 0
	texture_info.y = 0
	texture_info.w = texture_info.width
	texture_info.h = texture_info.height

	local pixels = IL.ilGetData()
	
	local bgfx_owned_mem = bgfx.bgfx_copy(pixels,texture_info.w*texture_info.h*4)
	local tex_h = bgfx.bgfx_create_texture_2d(texture_info.width,texture_info.height,texture_info.numMips > 1,texture_info.numLayers,texture_info.format,texture_info.flags,bgfx_owned_mem)
	
	-- pixel data is copied to bgfx, free IL image data
	IL.ilDeleteImages(1,ilImage)
	
	texture_info.id = tex_h.idx

	--core.manager.assets.texture.add(texture_info)
	
	texture_info.loaded = true
	
	return texture_info
end

--[[	open_texture
@ param1: vfs file path
Sends an operation to the thread pool to open a texture and fill a table with texture information and copy it to main thread's cached texture info data
--]]
function open_texture_threaded(filename)
	if not (filesystem.exists(filename)) then
		return
	end
	core.thread.pool.linda:send("pool-functor",{function(a,b,c)
		local buffer,length = filesystem.read(filename)

		local IL = require("ffi_devil")
		local bgfx = require("ffi_bgfx")
		
		local ilImage = ffi.new("ILuint[1]")
		IL.ilGenImages(1, ilImage)
		IL.ilBindImage(ilImage[ 0 ])

		IL.ilLoadL(IL.IL_TYPE_UNKNOWN,buffer,length)
		local err = IL.ilGetError()
		if (err ~= 0) then
			error(IL.iluErrorString(err))
		end

		local new_texture_info = {}
		new_texture_info.name = filename
		new_texture_info.format = bgfx.BGFX_TEXTURE_FORMAT_RGBA8 -- TODO: Maybe IL to BGFX conversion?
		new_texture_info.storageSize = IL.ilGetInteger(IL.IL_IMAGE_SIZE_OF_DATA)
		new_texture_info.width = IL.ilGetInteger(IL.IL_IMAGE_WIDTH)
		new_texture_info.height = IL.ilGetInteger(IL.IL_IMAGE_HEIGHT)
		new_texture_info.depth = IL.ilGetInteger(IL.IL_IMAGE_DEPTH)
		new_texture_info.numLayers = IL.ilGetInteger(IL.IL_NUM_LAYERS) --hasCaps(bgfx.BGFX_CAPS_TEXTURE_2D_ARRAY) and IL.ilGetInteger(IL.IL_NUM_LAYERS) or 1
		new_texture_info.numMips =  IL.ilGetInteger(IL.IL_NUM_MIPMAPS)
		new_texture_info.bitsPerPixel = IL.ilGetInteger(IL.IL_IMAGE_BITS_PER_PIXEL)
		new_texture_info.cubeMap = false -- TODO: IL_IMAGE_CUBEFLAGS
		new_texture_info.flags = 0 -- 64 bit integers (bgfx.BGFX_TEXTURE_NONE) break linda:send?

		-- uv region
		new_texture_info.x = 0
		new_texture_info.y = 0
		new_texture_info.w = new_texture_info.width
		new_texture_info.h = new_texture_info.height

		local pixels = IL.ilGetData()
		
		local bgfx_owned_mem = bgfx.bgfx_copy(pixels,new_texture_info.w*new_texture_info.h*4)
		local tex_h = bgfx.bgfx_create_texture_2d(new_texture_info.width,new_texture_info.height,new_texture_info.numMips > 1,new_texture_info.numLayers,new_texture_info.format,new_texture_info.flags,bgfx_owned_mem)
		
		-- pixel data is copied to bgfx, free IL image data
		IL.ilDeleteImages(1,ilImage)
		
		new_texture_info.id = tex_h.idx
		
		new_texture_info.loaded = true
		
		linda:send("main-functor",{function()
			local cache = core.manager.assets.texture.cache
			local texture_info = cache[ new_texture_info.name ]
			for k,v in pairs(new_texture_info) do
				texture_info[ k ] = v
			end
		end})
	end})
end

--[[	blit_texture
@ param1: table containing new texture info
@ param2: table containing texture info of a source texture
@ param3: x offset of source image
@ param4: y offset of source image
@ param5: w offset of source image
@ param6: h offset of source image
--]]
function blit_texture(texture_info,src_texture_info,x,y,w,h)
	local bgfx_owned_mem = bgfx.bgfx_alloc(w*h*4)
	local tex_h = bgfx.bgfx_create_texture_2d(w,h,false,1,bgfx.BGFX_TEXTURE_FORMAT_RGBA8,bgfx.BGFX_TEXTURE_BLIT_DST,bgfx_owned_mem)
	local src_tex_h = ffi.new("bgfx_texture_handle_t",src_texture_info.id)
	bgfx.bgfx_blit(0,tex_h,0,0,0,0,src_tex_h,0,x,y,0,w,h,1)
	
	texture_info.id = tex_h.idx

	texture_info.format = bgfx.BGFX_TEXTURE_FORMAT_RGBA8 -- TODO: Maybe IL to BGFX conversion?
	texture_info.storageSize = w*h*4
	texture_info.width = w
	texture_info.height = h
	texture_info.depth = 1
	texture_info.numLayers = 1
	texture_info.numMips =  1
	texture_info.bitsPerPixel = 32
	texture_info.cubeMap = false
	texture_info.flags = 0 -- bgfx.BGFX_TEXTURE_NONE
	
	-- uv region
	texture_info.x = 0
	texture_info.y = 0
	texture_info.w = w
	texture_info.h = h
end

--[[	destroy_texture
@ param1: table containing texture parameters
--]]
function destroy_texture(texture_info)
	texture_info.loaded = false
	if (texture_info.id and texture_info.id ~= 65535) then
		local tex_h = ffi.new("bgfx_texture_handle_t",texture_info.id)
		bgfx.bgfx_destroy_texture(tex_h)
		texture_info.id = 65535
	end
	core.manager.assets.texture.remove(texture_info)
end

--[[	draw_texture
@ param1: table containing texture parameters
@ param2: optional encoder created with 'bgfx_begin_encoder'
Quick and dirty! Mainly use for debug/prototyping.
Create your own optimized draw method suited
to your design.
--]]
function draw_texture(texture_info,encoder)
	if not (texture_info.loaded) then
		return
	end
	
	local ax = texture_info.x / texture_info.width
	local ay = texture_info.y / texture_info.height
	
	local tx = ax + texture_info.w / texture_info.width
	local ty = ay + texture_info.h / texture_info.height

	local tvb = ffi.new('bgfx_transient_vertex_buffer_t[1]')
	bgfx.bgfx_alloc_transient_vertex_buffer(tvb,6,vdecl)
	
	local cnt = 0
	local function pushv(x,y,u,v,rgba)
		local vert = ffi.cast("float *",tvb[ 0 ].data+cnt)
		vert[0] = x
		vert[1] = y
		vert[2] = u
		vert[3] = v
		vert = ffi.cast("uint32_t *",tvb[ 0 ].data+cnt+16)
		vert[0] = rgba
		cnt = cnt + 20
	end
	local function pushquad(x, y, w, h)
		pushv(x,y,ax,ay,0xffffffff)
		pushv(x,y+h,ax,ty,0xffffffff)
		pushv(x+w,y,tx,ay,0xffffffff)
		pushv(x+w,y,tx,ay,0xffffffff)
		pushv(x,y+h,ax,ty,0xffffffff)
		pushv(x+w,y+h,tx,ty,0xffffffff)
	end

	pushquad(0,0,texture_info.w,texture_info.h)
	
	if (encoder) then
		bgfx.bgfx_encoder_set_state(encoder,bgfx.BGFX_STATE_WRITE_RGBA+bgfx.BGFX_STATE_BLEND_ALPHA,0)
		bgfx.bgfx_encoder_set_transient_vertex_buffer(encoder,0,tvb,0,6,vdecl_h)
		
		local u_scale = core.renderer.shader.uniform.u_scale
		local scale = ffi.new("kmVec4",1,1,1,1)
		bgfx.bgfx_encoder_set_uniform(encoder,u_scale,scale,1)
		
		local tex_h = ffi.new("bgfx_texture_handle_t",texture_info.id)
		local s_texColor = core.renderer.shader.uniform.s_texColor
		bgfx.bgfx_encoder_set_texture(encoder,0,s_texColor,tex_h,0xffffffff)
		
		local prog = core.renderer.shader.programs.default
		bgfx.bgfx_encoder_submit(encoder,0,prog,0,false)
	else
		bgfx.bgfx_set_state(bgfx.BGFX_STATE_WRITE_RGBA+bgfx.BGFX_STATE_BLEND_ALPHA,0)
		bgfx.bgfx_set_transient_vertex_buffer(0,tvb,0,6)
		
		local u_scale = core.renderer.shader.uniform.u_scale
		local scale = ffi.new("kmVec4",1,1,1,1)
		bgfx.bgfx_set_uniform(u_scale,scale,1)
		
		local tex_h = ffi.new("bgfx_texture_handle_t",texture_info.id)
		local s_texColor = core.renderer.shader.uniform.s_texColor
		bgfx.bgfx_set_texture(0,s_texColor,tex_h,0xffffffff)
		
		local prog = core.renderer.shader.programs.default
		bgfx.bgfx_submit(0,prog,0,false)	
	end
end

------------------------------------------------
-- Callbacks 
------------------------------------------------
local function on_framework_render(winW,winH,fbW,fbH)
	local encoder = bgfx.bgfx_encoder_begin(true)
	bgfx.bgfx_encoder_touch(encoder,0)
	bgfx.bgfx_encoder_end(encoder)
	
	bgfx.bgfx_dbg_text_printf(0,0,0x2f,"FPS: "..tostring(core.getFPS()))
end

local function on_framework_frame_end(winW,winH,fbW,fbH)
	bgfx.bgfx_frame(false)
end

local function on_framework_window_creation(wnd,width,height)
	local bgfx_init_s = ffi.new("bgfx_init_t[1]")
	bgfx.bgfx_init_ctor(bgfx_init_s)
	bgfx_init_s[0].type = bgfx.BGFX_RENDERER_TYPE_OPENGL -- TODO: TEMPORARY! set to BGFX_RENDERER_TYPE_COUNT for default renderer when shaders are made for all renderers
	bgfx_init_s[0].vendorId = bgfx.BGFX_PCI_ID_NONE
	bgfx_init_s[0].deviceId = 0
	bgfx_init_s[0].debug = false
	bgfx_init_s[0].profile = false
	bgfx_init_s[0].resolution.width = width
	bgfx_init_s[0].resolution.height = height
	bgfx_init_s[0].resolution.reset = bgfx.BGFX_RESET_VSYNC
	
	-- Retrieve platform data
	if (ffi.os == "Windows") then
		local nwh = ffi.cast("void*",glfw.GetWin32Window(wnd))
		bgfx_init_s[0].platformData.nwh = nwh
	elseif (ffi.os == "OSX") then
		local nwh = ffi.cast("void*",glfw.GetCocoaWindow(wnd))
		bgfx_init_s[0].platformData.nwh = nwh
	elseif (ffi.os == "Linux") then
		-- TODO: Determine if these libs are necessary
		ffi.load('/usr/lib/libX11.so.6', true)
		ffi.load('/usr/lib/libGL.so', true)
		
		local nwh = ffi.cast("void *",glfw.GetX11Window(wnd))
		local ndt = ffi.cast("void *",glfw.GetX11Display())
		bgfx_init_s[0].platformData.nwh = nwh
		bgfx_init_s[0].platformData.ndt = ndt
	else
		error("Unsupported platform: " .. ffi.os)
	end
	
	-- Initialize bgfx
	bgfx.bgfx_init(bgfx_init_s[0])
	
	-- Fetch capabilities flags
	local caps = bgfx.bgfx_get_caps()
	_caps = caps.formats[0]
	
	local renderer = bgfx.bgfx_get_renderer_name(bgfx.bgfx_get_renderer_type())
	print("renderer:",renderer ~= nil and ffi.string(renderer) or "unknown")

	local video_flags = get_video_options()
	bgfx.bgfx_reset(width,height,video_flags,bgfx_init_s[0].resolution.format)
	bgfx.bgfx_set_view_rect(0,0,0,width,height)

	bgfx.bgfx_set_debug(bgfx.BGFX_DEBUG_TEXT)
	
	bgfx.bgfx_set_view_clear(0,bgfx.BGFX_CLEAR_COLOR + bgfx.BGFX_CLEAR_DEPTH,0x999966,1.0,0)
end

local function on_framework_resize(w,h)
	bgfx.bgfx_reset(width,height,get_video_options(),bgfx_init_s[0].resolution.format)
	bgfx.bgfx_set_view_rect(0,0,0,width,height)
end

local function on_framework_window_destroy(wnd)
	bgfx.bgfx_shutdown()
end

local function on_framework_exit()
	bgfx.bgfx_destroy_vertex_layout(vdecl_h)
end

local function on_framework_load()
	vdecl = ffi.new('bgfx_vertex_layout_t[1]')
	bgfx.bgfx_vertex_layout_begin(vdecl,bgfx.BGFX_RENDERER_TYPE_NOOP)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_POSITION,2,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_TEXCOORD0,2,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_COLOR0,4,bgfx.BGFX_ATTRIB_TYPE_UINT8,true,false)
	bgfx.bgfx_vertex_layout_end(vdecl)
	vdecl_h = bgfx.bgfx_create_vertex_layout(vdecl)
end

function framework_run()
	local section = "renderer"
	core.config.set_default(section,"renderer_type",bgfx.BGFX_RENDERER_TYPE_OPENGL)
	
	CallbackSet("framework_window_creation",on_framework_window_creation,9999)
	CallbackSet("framework_window_destroy",on_framework_window_destroy,9999)
	CallbackSet("framework_resize",on_framework_resize,9999)
	CallbackSet("framework_render",on_framework_render,9999)
	CallbackSet("framework_render",on_framework_frame_end,-9999)
	CallbackSet("framework_exit",on_framework_exit)
	CallbackSet("framework_load",on_framework_load)
end