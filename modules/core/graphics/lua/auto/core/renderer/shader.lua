programs = {}
_program = nil
shaders = {}
uniform = {}

local bgfx = require("ffi_bgfx")

function create_shader(alias,source)
	local mem = bgfx.bgfx_copy(ffi.cast('char *', source), #source)
	local shader = bgfx.bgfx_create_shader(mem)
	if (shader == nil) then
		print("failed to compile shader:",alias)
		return
	end
	shaders[ alias ] = shader
	return shader
end

function create_program(alias,vs_fname,fs_fname)
	local vs_shader = vs_fname and shaders[ vs_fname ]
	local fs_shader = fs_fname and shaders[ fs_fname ]
	if (vs_shader and fs_shader) then
		local prog = bgfx.bgfx_create_program(vs_shader,fs_shader,true)
		programs[ alias ] = prog
		return prog
	end
end

-- Get or Set active shader program
function active(alias)
	if (alias and programs[ alias ]) then
		_program = programs[ alias ]
	end
	return _program
end

local render_type_to_path = {
	[bgfx.BGFX_RENDERER_TYPE_DIRECT3D9] 	= "shaders/dx9/",
	[bgfx.BGFX_RENDERER_TYPE_DIRECT3D11] 	= "shaders/dx11/",
	[bgfx.BGFX_RENDERER_TYPE_DIRECT3D12] 	= "shaders/dx11/",
	[bgfx.BGFX_RENDERER_TYPE_GNM] 			= "shaders/pssl/",
	[bgfx.BGFX_RENDERER_TYPE_METAL] 		= "shaders/metal/",
	[bgfx.BGFX_RENDERER_TYPE_OPENGLES] 		= "shaders/essl/",
	[bgfx.BGFX_RENDERER_TYPE_OPENGL] 		= "shaders/glsl/",
	[bgfx.BGFX_RENDERER_TYPE_VULKAN] 		= "shaders/spirv/"
}

function parse_shaders_from_config()
	local rt = bgfx.bgfx_get_renderer_type()
	local prefix = render_type_to_path[ tonumber(rt) ]

	if not (prefix) then
		error("Unrecognized platform cannot generate shaders")
	end

	local xml = core.import.xml("config/shaders.xml")
	for i,element in ipairs(xml.data.shader) do
		local alias = element[ 1 ].name
		if (programs[ alias ]) then 
			print("shader redifinition!",element[ 1 ].name)
			return
		end
		
		local fragmentShader = nil
		local vertexShader = nil
		
		-- fragment shader
		local fs_fname = prefix .. element[ 1 ].fragment
		if (fs_fname and filesystem.exists(fs_fname)) then
			local fragmentSource = filesystem.read(fs_fname)
			create_shader(fs_fname,fragmentSource)
		end

		-- vertex shader
		local vs_fname = prefix .. element[ 1 ].vertex
		if (vs_fname and filesystem.exists(vs_fname)) then
			local vertexSource = filesystem.read(vs_fname)
			create_shader(vs_fname,vertexSource)
		end
		
		create_program(alias,vs_fname,fs_fname)
	end
	
	active("default")
end

function framework_run()
	CallbackSet("framework_load",on_load,9999) -- high priority
	CallbackSet("framework_renderer_changed",renderer_changed)
	CallbackSet("framework_exit",framework_exit)
end

function framework_exit()
	-- release uniforms
	for alias,handle in pairs(uniform) do
		bgfx.bgfx_destroy_uniform(handle)
	end
	-- release shaders
	-- for alias,handle in pairs(shaders) do
		-- bgfx.bgfx_destroy_shader(handle)
	-- end
	-- release shader programs
	for alias,handle in pairs(programs) do
		bgfx.bgfx_destroy_program(handle)
	end
	
	uniform = nil
	shaders = nil
	programs = nil
end

function renderer_changed()
	programs = {}
	shaders = {}
	parse_shaders_from_config()
end

function on_load()
	-- TODO: Find out best way to implement these for multiple shaders. Maybe try load script for each shader?
	-- Create Uniform handles
	uniform.s_texColor = bgfx.bgfx_create_uniform("s_texColor",bgfx.BGFX_UNIFORM_TYPE_SAMPLER,1)
	
	uniform.u_scale = bgfx.bgfx_create_uniform("u_scale",bgfx.BGFX_UNIFORM_TYPE_VEC4,1)
	local val = ffi.new("kmVec4",1,1,1,1)
	bgfx.bgfx_set_uniform(uniform.u_scale,val,1)
	
	parse_shaders_from_config()
end