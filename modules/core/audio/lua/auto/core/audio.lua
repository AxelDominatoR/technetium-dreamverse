local thread = nil -- The main audio thread
local linda = nil

function framework_run()
	linda = lanes.linda()
	local function loop(linda,package_path)
		package.path = package_path
		require("filesystem")
		require("global")
		_G.linda = linda
		invoke("thread_audio")
	end
	setfenv(loop,{})
	thread = lanes.gen("*",loop)(linda,package.path)
	
	CallbackSet("framework_exit",framework_exit)
end

function framework_exit()
	thread:cancel(10)
end

-------------------------
-- Dummy Bit Stream
-------------------------
Class "cBitStreamDummy"
function cBitStreamDummy:__init(filename,isStreamed)
	if not (linda) then
		return
	end
	linda:send("thread-call",{"newBitStream",filename,isStreamed == true})
	self.alias = filename
end

function cBitStreamDummy:play(x,y,z,volume,loop,relative,mandatory)
	if not (linda) then
		return
	end
	linda:send("thread-call",{"playSound",self.alias,x or 0,y or 0,z or 0,volume or 1,loop == true,relative == true, mandatory == true})
	if (mandatory) then
		--- XXX: todo
	end
end

function cBitStreamDummy:stop()
	if not (linda) then
		return
	end
	linda:send("thread-call",{"stopSound",self.alias})
end

-------------------------
-- API
-------------------------
function suspend()
	if not (linda) then
		return
	end
	linda:send("thread-call","suspend")
end

function resume()
	if not (linda) then
		return
	end
	linda:send("thread-call","resume")
end

function getDeviceList()
	if not (linda) then
		return
	end
	linda:send("thread-call","getDeviceList")
	local k,v = linda:receive(30,"getDeviceList") -- Wait for results until timeout
	if (v ~= nil) then
		return v
	end
	return {}
end

function getCurrentDevice()
	if not (linda) then
		return
	end
	linda:send("thread-call","getCurrentDevice")
	local k,v = linda:receive(30,"getCurrentDevice") -- Wait for results until timeout
	if (v ~= nil) then
		return v
	end
	return {}
end

function getCaptureDeviceList()
	if not (linda) then
		return
	end
	-- XXX: unimplemented
end

local listener = {} -- To avoid constant table creation
function updateListener(x,y,z,vx,vy,vz,m4,m5,m6,m8,m9,m10)
	if not (linda) then
		return
	end
	listener[1] = x
	listener[2] = y
	listener[3] = z
	listener[4] = vx
	listener[5] = vy
	listener[6] = vz
	listener[7] = m4
	listener[8] = m5
	listener[9] = m6
	listener[10] = m8
	listener[11] = m9
	listener[12] = m10
	linda:set("updateListener",listener)
end