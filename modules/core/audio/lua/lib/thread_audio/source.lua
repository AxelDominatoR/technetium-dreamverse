queueBuffersNum = 4

--------------------
-- Class cSource
--------------------
Class "cSource"
function cSource:__init()
	self.pBuffer = ffi.new("ALuint[?]",queueBuffersNum)
	self.pSource = ffi.new("ALuint[1]")
	AL.alGenSources(1,self.pSource)
	AL.alGenBuffers(queueBuffersNum,self.pBuffer)
	ffi.gc(self.pBuffer,function(buffers) return AL.alDeleteBuffers(queueBuffersNum,buffers) end)
	ffi.gc(self.pSource,function(source) return AL.alDeleteSources(1,source) end)
end

function cSource:setSound(snd)
	assert(snd)
	self.sound = snd
	if (snd.isStreamed) then 
		snd:setStreamPosition(0)
		self:initBuffers()
	else
		AL.alBufferData(self.pBuffer[0],snd.format,snd.pStreamBuffer,snd.pStreamBufferLength,snd.frequency)
		AL.alSourcei(self.pSource[0],AL.AL_BUFFER,self.pBuffer[0])
	end
end

function cSource:setGain(val)
	AL.alSourcef(self.pSource[0], AL.AL_GAIN, val or 1)
end

function cSource:setPosition(x,y,z)
	AL.alSource3f(self.pSource[0],AL.AL_POSITION,x,y,z)
end

function cSource:reset()
	AL.alSource3f(self.pSource[0],AL.AL_POSITION,0,0,0)
	AL.alSourcei(self.pSource[0],AL.AL_LOOPING,AL.AL_FALSE)
	AL.alSourcef(self.pSource[0],AL.AL_GAIN,1)
	AL.alSourcei(self.pSource[0],AL.AL_BUFFER,AL.AL_NONE)
end

function cSource:play()
	AL.alSourcePlay(self.pSource[0])
end

function cSource:pause()
	AL.alSourcePause(self.pSource[0])
end

function cSource:stop()
	AL.alSourceStop(self.pSource[0])
	if (self.sound and self.sound.isStreamed) then
		self.sound:setStreamPosition(0)
	end
end

function cSource:isPlaying()
	local state = ffi.new("ALint[1]")
	AL.alGetSourcei(self.pSource[0],AL.AL_SOURCE_STATE,state);
	return state[0] == AL.AL_PLAYING
end

function cSource:isPaused()
	local state = ffi.new("ALint[1]")
	AL.alGetSourcei(self.pSource[0],AL.AL_SOURCE_STATE,state);
	return state[0] == AL.AL_PAUSED
end

function cSource:isStopped()
	local state = ffi.new("ALint[1]")
	AL.alGetSourcei(self.pSource[0],AL.AL_SOURCE_STATE,state);
	return state[0] == AL.AL_STOPPED
end 

function cSource:isActive()
	local state = ffi.new("ALint[1]")
	AL.alGetSourcei(self.pSource[0],AL.AL_SOURCE_STATE,state);
	return state[0] == AL.AL_PLAYING or state[0] == AL.AL_PAUSED
end 

function cSource:isInactive()
	local state = ffi.new("ALint[1]")
	AL.alGetSourcei(self.pSource[0],AL.AL_SOURCE_STATE,state);
	return state[0] == AL.AL_INITIAL or state[0] == AL.AL_STOPPED
end

function cSource:seekTime(pos)
	assert(self:isInactive(),"cSource:seekTime: Source is playing!")
	if (self.sound.isStreamed) then 
		self.sound:setStreamPosition(pos*self.sound.frequency)
		AL.alSourcei(self.pSource[0],AL.AL_BUFFER,AL.AL_NONE)
		self:initBuffers()
	else
		AL.alSourcef(self.pSource[0],AL.AL_SEC_OFFSET,pos)
	end
end

function cSource:update()
	if (self.sound and self.sound.isStreamed) then
		local processed = ffi.new("ALint[1]")
		AL.alGetSourcei(self.pSource[0],AL.AL_BUFFERS_PROCESSED,processed)
		while (processed > 0) do
			local buffer = ffi.new("ALuint[1]")
			AL.alSourceUnqueueBuffers(self.pSource[0],1,buffer)
			if (self:fillBuffer(buffer)) then
				AL.alSourceQueueBuffers(self.pSource[0],1,buffer)
			end
			-- TODO: looping music stream
			processed = processed - 1
		end
		-- prevent the source from stopping
		local state, queued = ffi.new("ALint[1]"), ffi.new("ALint[1]")
		AL.alGetSourcei(self.pSource[0],AL.AL_SOURCE_STATE,state)
		AL.alGetSourcei(self.pSource[0],AL.AL_BUFFERS_QUEUED,queued)
		if (state[0] == AL.AL_STOPPED and queued[0] > 0) then
			AL.alSourcePlay(self.pSource[0])
		end
	end
end

function cSource:fillBuffer(buffer)
	local read = self.sound:readStream()
	AL.alBufferData(buffer[0],self.sound.format,self.sound.pStreamBuffer,read,self.sound.frequency)
	return read ~= 0
end

function cSource:initBuffers()
	for i=1,queueBuffersNum do 
		self:fillBuffer(self.pBuffer[i])
		AL.alSourceQueueBuffers(self.pSource,queueBuffersNum,self.pBuffer[i][0])
	end
end

function cSource:cleanup()
	self.sound = nil
end

function cSource:__gc()
	self.sound = nil
end