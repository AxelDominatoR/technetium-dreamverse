ffi 			= require("ffi")
Class			= require("class")
AL 				= require("ffi_openal") 		; if not (AL) then error("failed to load libopenal") end
vorbis 			= require("ffi_libvorbis") 		; if not (vorbis) then error("failed to load libvorbis") end
physfs 			= require("ffi_physfs") 		; if not (physfs) then error("failed to load phsyicsfs") end
kazmath			= require("ffi_kazmath")		; if not (kazmath) then error("failed to load kazmath") end

invoke("thread_audio.source")
invoke("thread_audio.bitstream")
invoke("thread_audio.manager")

ALSoundManager = cSoundManager()
if not (ALSoundManager.initialized) then
	ALSoundManager = nil
end

invoke("thread_audio.communication")
invoke("thread_audio.main")