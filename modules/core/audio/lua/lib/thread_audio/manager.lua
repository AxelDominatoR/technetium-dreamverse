queueSourcesNum = 32

Class "cSoundManager"
function cSoundManager:__init()
	self.deviceCount = 0
	self.devices = {}
	
	if (AL.alcIsExtensionPresent(nil,"ALC_ENUMERATION_EXT") == 0) then
		printe("OpenAL: EnumerationExtension not present!")
		return
	else
		local pDevices = AL.alcGetString(nil,AL.ALC_DEVICE_SPECIFIER)
		local i = 1
		while (pDevices[0] ~= 0) do
			local str = ffi.string(pDevices)
			pDevices = pDevices + #str + 1
			
			local pDevice = AL.alcOpenDevice(str)
			if (pDevice ~= nil) then
				local pContext = AL.alcCreateContext(pDevice,nil)
				if (pContext ~= nil and AL.alcMakeContextCurrent(pContext) == 1) then
					local actualDeviceName = AL.alcGetString(pDevice,AL.ALC_DEVICE_SPECIFIER)	
					if (actualDeviceName ~= nil and actualDeviceName[0] ~= 0) then
						local alcMajor,alcMinor = ffi.new("ALCint[1]"),ffi.new("ALCint[1]")
						AL.alcGetIntegerv(pDevice, AL.ALC_MAJOR_VERSION, 1, alcMajor)
						AL.alcGetIntegerv(pDevice, AL.ALC_MINOR_VERSION, 1, alcMinor)
						self.devices[ffi.string(actualDeviceName)] = tonumber(alcMajor[0].."."..alcMinor[0])
						self.deviceCount = self.deviceCount + 1
					end
					AL.alcMakeContextCurrent(nil)
					AL.alcDestroyContext(pContext)
				end
				AL.alcCloseDevice(pDevice)
			end

			i = i + 1
		end
	end
	
	if (self.deviceCount == 0) then
		printe("OpenAL: Cannot create sound device.")
		return
	end
	
	print("OpenAL: All available devices:")
	
	for name,version in pairs(self.devices) do
		print("",name,version)
	end
	
	if not (self:selectBestDevice()) then
		return
	end
	
	if not (self.selected) then
		printe("OpenAL: Failed to select a device.")
		return
	end
	
	-- Initialize listener
	self.listener = ffi.new("kmVec3",0,0,0)
	AL.alListener3f(AL.AL_POSITION,0,0,0)
	AL.alListener3f(AL.AL_VELOCITY,0,0,0)
	AL.alListenerfv(AL.AL_ORIENTATION,ffi.new("float[6]", {0,0,1,0,1,0}))
	AL.alListenerf(AL.AL_GAIN,1)
	
	-- Number of max sounds, software it's high, hardware this is limited
	local pNumMono = ffi.new("ALCint[1]")
	AL.alcGetIntegerv(self.pDevice,AL.ALC_MONO_SOURCES,1,pNumMono)
	self.maxSoundTargets = pNumMono[0] 
	
	self.bitstreams = {}
	self.sources = {}
	for i=1,math.min(queueSourcesNum,self.maxSoundTargets) do
		self.sources[i] = cSource()
	end
	
	self.initialized = true
end

function cSoundManager:selectBestDevice()
	local selected = nil
	local best_version = -1
	for name,version in pairs(self.devices) do
		if (version > best_version) then
			selected = name
			best_version = version
		end
	end
	
	print("OpenAL: selected device is",selected)
	return self:setDevice(selected)
end

function cSoundManager:getCurrentDevice()
	return self.selected
end

function cSoundManager:setDevice(name)
	self.selected = nil
	
	if (self.pDevice ~= nil) then
		AL.alcCloseDevice(self.pDevice)
		self.pDevice = nil
	end
	if (self.pContext ~= nil) then
		AL.alcMakeContextCurrent(nil)
		AL.alcDestroyContext(self.pContext)
		self.pContext = nil
	end
		
	if not (name and self.devices[name]) then
		return false
	end
	
	self.pDevice = AL.alcOpenDevice(name)
	if (self.pDevice == nil) then
		printe("OpenAL: Failed to create device.")
		return false
	end
	
	self.pContext = AL.alcCreateContext(self.pDevice,nil)
	if (self.pContext == nil) then
		printe("OpenAL: Failed to create context.")
		return false
	end

	if (AL.alcMakeContextCurrent(self.pContext) == 0) then
		printe("OpenAL: Failed to set default context.")
		return false
	end
	
	-- Clear errors
	AL.alGetError()
	AL.alcGetError(self.pDevice)
	
	self.selected = name
	
	return true
end

function cSoundManager:getNumDevices()
	return self.deviceCount
end

function cSoundManager:getDeviceList()
	return self.devices
end

function cSoundManager:setMasterVolume(amt)
	AL.alListenerf(AL.AL_GAIN,amt or 1)
end

function cSoundManager:updateListener(x,y,z,vx,vy,vz,m1,m2,m3,m4,m5,m6)
	local pos = ffi.new("kmVec3",x,y,z)
	if (kazmath.kmVec3AreEqual(self.listener,pos) == 0) then
		self.listener = pos
		-- TODO: Listener moved send callback
	end
 	AL.alListener3f(AL.AL_POSITION,x,y,z)
	AL.alListener3f(AL.AL_VELOCITY,vx,vy,vz)
	AL.alListenerfv(AL.AL_ORIENTATION,ffi.new("float[6]", {m1,m2,m3,m4,m5,m6}))
end

function cSoundManager:playSound(alias,x,y,z,volume,loop,relative,mandatory)
	local snd = self.bitstreams[alias]
	if not (snd and snd.pStreamBuffer) then
		-- if (mandatory) then
			-- eventWait:set()
		-- end
		return
	end
	for i,src in ipairs(self.sources) do
		if (src:isInactive()) then
			AL.alSourcei(src.pSource[0], AL.AL_SOURCE_RELATIVE,relative and AL.AL_TRUE or AL.AL_FALSE)
			AL.alSource3f(src.pSource[0],AL.AL_POSITION,x or 0,y or 0,z or 0)
			AL.alSourcei(src.pSource[0],AL.AL_LOOPING,loop and AL.AL_TRUE or AL.AL_FALSE)
			AL.alSourcef(src.pSource[0],AL.AL_GAIN,volume or 1)
			AL.alSourcei(src.pSource[0],AL.AL_BUFFER,AL.AL_NONE)
			src:setSound(snd)
			src:play()
			-- if (mandatory) then
				-- eventWait:set()
			-- end
			return src
		end
	end
end

function cSoundManager:stop(alias)
	local snd = self.bitstreams[alias]
	if not (snd and snd.pStreamBuffer) then
		return
	end
	for i,src in ipairs(self.sources) do
		if (src:isActive()) then
			if (src.sound == snd) then
				src:stop()
			end
		end
	end
end

function cSoundManager:suspend()
	for i,src in ipairs(self.sources) do 
		if (src:isActive()) then
			src:pause()
		end
	end
	AL.alcSuspendContext(self.pContext)
	AL.alcMakeContextCurrent(nil)
end

function cSoundManager:resume()
	AL.alcProcessContext(self.pContext)
	AL.alcMakeContextCurrent(self.pContext)
	for i,src in ipairs(self.sources) do 
		if (src:isActive()) then
			src:play()
		end
	end
end 

function cSoundManager:newBitStream(filename,isStreamed)
	self.bitstreams[filename] = cBitStream(filename,isStreamed)
end

function cSoundManager:cleanup()
	if (self.sources) then
		for i,src in ipairs(self.sources) do 
			if (src:isActive()) then
				src:pause()
			end
			src:cleanup()
		end
		self.sources = nil
	end
	if (self.bitstreams) then
		for fname,bitstream in pairs(self.bitstreams) do
			bitstream:closeStream()
		end
		self.bitstreams = nil
	end
	if (self.pDevice ~= nil) then
		AL.alcCloseDevice(self.pDevice)
		self.pDevice = nil
	end
	if (self.pContext ~= nil) then
		AL.alcMakeContextCurrent(nil)
		AL.alcDestroyContext(self.pContext)
		self.pContext = nil
	end
end

function cSoundManager:__gc()
	if (self.sources) then
		for i,src in ipairs(self.sources) do 
			if (src:isActive()) then
				src:pause()
			end
			src:cleanup()
		end
		self.sources = nil
	end
	if (self.bitstreams) then
		for fname,bitstream in pairs(self.bitstreams) do
			bitstream:closeStream()
		end
		self.bitstreams = nil
	end
	if (self.pDevice ~= nil) then
		AL.alcCloseDevice(self.pDevice)
		self.pDevice = nil
	end
	if (self.pContext ~= nil) then
		AL.alcMakeContextCurrent(nil)
		AL.alcDestroyContext(self.pContext)
		self.pContext = nil
	end
end