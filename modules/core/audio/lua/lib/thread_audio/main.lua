local time = require("time")

set_finalizer(function(err,stk)
	--print("thread_audio: attempting to exit cleanly...")
	
	OggVorbisCallbacks.read_func:free()
	OggVorbisCallbacks.seek_func:free()
	OggVorbisCallbacks.close_func:free()
	OggVorbisCallbacks.tell_func:free()
	
	if (ALSoundManager) then
		ALSoundManager:cleanup()
		ALSoundManager = nil
	end
	
	if err and type( err) ~= "userdata" then
		-- no special error: true error
		print("thread_audio: error: "..tostring(err))
	elseif type(err) == "userdata" then
		-- lane cancellation is performed by throwing a special userdata as error
		print("thread_audio: after cancel")
	else
		-- no error: we just got finalized
		--print("thread_audio: finalized")
	end
end)
	
-- Main loop
repeat
	local k,v = linda:receive(0,"thread-call")
	if (v ~= nil) then
		if (type(v) == "table" and type(v[ 1 ]) == "string" and _G[ v[ 1 ] ]) then
			_G[ v[ 1 ] ](select(2, unpack(v)))
		elseif (type(v) == "string" and _G[ v ]) then
			_G[ v ]()
		end
	end

	v = linda:get("updateListener")
	if (v ~= nil) then
		updateListener(unpack(v))
	end
	
	local playing = false
	if (ALSoundManager and ALSoundManager.initialized) then
		for i,src in ipairs(ALSoundManager.sources) do
			src:update()
			playing = true
		end
	end
	if not (playing) then
		time.sleep(0.020)
	end
until (cancel_test() == true)