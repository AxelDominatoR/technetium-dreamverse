local glfw = Libs.glfw

aliases = nil
joyToAlias = {}
buttonStates = {}
axesStates = {}

function framework_run()
	core.config.set_default("keybinding","joystick",{
		default = {
		["pause"] 		= {1,true},
		["action"] 		= {2,true},
		["up"] 			= {3,true},
		["left"]		= {4,true},
		["down"] 		= {5,true},
		["right"] 		= {6,true}
		}
	})
	
	-- joystick polling
	CallbackSet("framework_update",function(dt)
		local id = 1
		while (glfw.JoystickPresent(id) == 1) do
			local count = ffi.new('int[1]')
			-- update button states
			local cbuttons = glfw.GetJoystickButtons(id,count)
			for i = 1, count do
				buttonStates[ id ][ i ] = cbuttons[i - 1]
			end
			-- update axis states
			local caxes = glfw.GetJoystickAxes(id,count)
			for i = 1, count do
			  axesStates[ id ][ i ] = caxes[i - 1]
			end
			id = id + 1
		end
	end,9999)
end

function pressed(id,button_or_alias)
	if not (buttonStates[ id ]) then
		return false
	end
	
	local guid = ffi.string(glfw.GetJoystickGUID(id))

	button_or_alias = aliases[ guid ] and aliases[ guid ][ key_or_alias ] and aliases[ guid ][ key_or_alias ][ 1 ]
	if not (type(button_or_alias) == "number") then
		return false
	end
	
	return buttonStates[ id ][ button_or_alias ] == glfw.PRESSED
end

function axes(id,axis)
	if not (axesStates[ id ] and axesStates[ id ][ axis ]) then
		return 0
	end
	return axesStates[ id ][ axis ]
end

function connect(id)
	buttonStates[ id ] = {}
	axesStates[ id ] = {}
	
	-- keybinding
	local guid = ffi.string(glfw.GetJoystickGUID(id))
	if (core.config.data.keybinding.joystick and core.config.data.keybinding.joystick[ guid ]) then
		aliases[ guid ] = core.config.data.keybinding.joystick[ guid ]
	else 
		aliases[ guid ] = {}
		core.config.data.keybinding.joystick[ guid ] = aliases[ guid ]
		for alias,t in pairs(core.config.data.keybinding.joystick.default) do 
			aliases[ guid ][ alias ] = {t[ 1 ],t[ 2 ]}
			if not (joyToAlias[ guid ]) then 
				joyToAlias[ guid ] = {}
			end
			if not (joyToAlias[ guid ][ t[ 1 ] ]) then 
				joyToAlias[ guid ][ t[ 1 ] ] = {}
			end
			joyToAlias[ guid ][ t[ 1 ] ][ alias ] = true
		end
	end
end

function disconnect(id)
	buttonStates[ id ] = nil
	axesStates[ id ] = nil
end