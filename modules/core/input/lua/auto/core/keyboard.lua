local glfw = Libs.glfw

aliases = nil 
keyToAlias = {}

function framework_run()
	core.config.set_default("keybinding","keyboard",{
		["pause"] 		= {glfw.KEY_ESCAPE,false},
		["action"] 		= {glfw.KEY_E,true},
		["up"] 			= {glfw.KEY_W,true},
		["left"]		= {glfw.KEY_A,true},
		["down"] 		= {glfw.KEY_S,true},
		["right"] 		= {glfw.KEY_D,true}
	})
	aliases = core.config.data.keybinding.keyboard
	for alias,t in pairs(aliases) do 
		if not (keyToAlias[ t[ 1 ] ]) then 
			keyToAlias[ t[ 1 ] ] = {}
		end
		keyToAlias[ t[ 1 ] ][ alias ] = true
	end
end

function pressed(key_or_alias)
	key_or_alias = aliases[ key_or_alias ] and aliases[ key_or_alias ][ 1 ]
	if not (type(key_or_alias) == "number") then
		return false
	end
	return glfw.GetKey(Managers.Windows.Windows.Main:Ref(),key_or_alias) == glfw.PRESS
end

function name(key_or_alias)
	key_or_alias =aliases[ key_or_alias ] and aliases[ key_or_alias ][ 1 ]
	if not (type(key_or_alias) == "number") then
		return "KEY_UNKNOWN"
	end
	return ffi.string(glfw.GetKeyName(key_or_alias,0))
end