is_double_click_down = false
double_click_pos = {0,0}
scroll_pos = {0,0}

function pressed(button)
	return glfw.GetMouseButton(Managers.Windows.Windows.Main:Ref(),button) == glfw.PRESS
end

-- get or set mouse position
function position(x,y)
	local _x = ffi.new('double[1]')
	local _y = ffi.new('double[1]')

    glfw.GetCursorPos( Managers.Windows.Current.Ref(),_x,_y)
	if (x or y) then
		x = x or _x[ 0 ]
		y = y or _y[ 0 ]
		glfw.SetCursorPos( Managers.Windows.Current.Ref(),x,y)
		return x,y
	end
	return _x[ 0 ],_y[ 0 ]
end

-- get or set cursor visible
function visible(b)
	if (b ~= nil) then
		glfw.SetInputMode(Managers.Windows.Windows.Main:Ref(),glfw.CURSOR,b == true and glfw.CURSOR_NORMAL or glfw.CURSOR_HIDDEN)
	end
	local mode = glfw.GetInputMode(Managers.Windows.Windows.Main:Ref(),glfw.CURSOR)
	return mode == glfw.CURSOR_NORMAL
end