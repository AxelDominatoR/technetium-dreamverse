local bgfx = require("ffi_bgfx") -- TODO: remove this dependancy; rendering commands to come from graphics module so that rendering backend can be changed without affecting other modules

-- core.entity.component.spatial()
getmetatable(this).__call = function (self,entity)
	return cSpatialManager(entity)
end

----------------------------------
-- attributes
----------------------------------
Class "cSpatialManager"
function cSpatialManager:__init(entity)
	self.entity = entity
	local mat4 = ffi.new("kmMat4")
	kazmath.kmMat4Identity(mat4)
	self.properties = { transform=mat4, angle={0,0,0} }
end

function cSpatialManager:transform()
	return self.properties.transform
end

function cSpatialManager:position(x,y,z)
	local mat = self.properties.transform.mat
	if (x or y or z) then
		mat[ 12 ] = x or mat[ 12 ]
		mat[ 13 ] = y or mat[ 13 ]
		mat[ 14 ] = z or mat[ 14 ]
	end
	return mat[ 12 ],mat[ 13 ],mat[ 14 ]
end

function cSpatialManager:angle(p,y,r)
	local ang = self.properties.angle
	if (p or y or r) then
		ang[ 1 ] = utils.wrap_minmax(p or ang[ 1 ],-2*math.pi,2*math.pi)
		ang[ 2 ] = utils.wrap_minmax(y or ang[ 2 ],-2*math.pi,2*math.pi)
		ang[ 3 ] = utils.wrap_minmax(r or ang[ 3 ],-2*math.pi,2*math.pi)
		kazmath.kmMat4RotationYawPitchRoll(self.properties.transform,unpack(ang))
	end
	return ang
end

function cSpatialManager:set_transform()
	bgfx.bgfx_set_transform(self.properties.transform.mat,1)
end

-------------------------------
-- persistance
-------------------------------
function cSpatialManager:stateWrite(data)
	local p = self.properties.transform.mat
	data.transform = {p[ 1 ],p[ 2 ],p[ 3 ],p[ 4 ],p[ 5 ],p[ 6 ],p[ 7 ],p[ 8 ],p[ 9 ],p[ 10 ],p[ 11 ],p[ 12 ],p[ 13 ],p[ 14 ],p[ 15 ],p[ 16 ]}
	p = self.properties.angle
	data.angle = {p[ 1 ],p[ 2 ],p[ 3 ]}
end

function cSpatialManager:stateRead(data)
	self.properties.transform = data.transform and ffi.new("kmMat4",data.transform) or self.properties.transform
	self.properties.angle = data.angle or self.properties.angle
	kazmath.kmQuaternionRotationPitchYawRoll(self.properties.orientation,unpack(self.properties.angle))
end