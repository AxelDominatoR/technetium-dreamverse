-- core.entity.component.visual()
getmetatable(this).__call = function (self,entity)
	return cModelManager(entity)
end

----------------------------------
-- attributes
----------------------------------
Class "cModelManager"
function cModelManager:__init(entity)
	self.dependencies = {"spatial"}
	self.entity = entity
	self.properties = { visible=true, textures={}, shaders={} }
end

function cModelManager:visible(b)
	if (b ~= nil) then
		self.properties.visible = b
	end
	return self.properties.visible
end

function cModelManager:model(path)
	if (path == nil) then
		return self.properties.model
	end
	if (path ~= "" and filesystem.exists(path) and not filesystem.isDirectory(path)) then
		if (self.properties.model and self.properties.model.name == path) then 
			return
		end
		self.properties.model = core.import.model(path,{"PreTransformVertices","OptimizeMeshes","CalcTangentSpace","Triangulate","FlipUVs"})
	else
		self.properties.model = nil
	end
	return self.properties.model
end

function cModelManager:update(dt,mx,my)

end

function cModelManager:draw()
	if (self.properties.visible ~= true or self.properties.model == nil) then 
		return
	end
 	-- gfx.transform.push()
		-- local position = self.entity.component.spatial:position()
		-- local orientation = self.entity.component.spatial:orientation()
		
		-- gfx.transform.translate(unpack(position))
		-- gfx.transform.rotate(orientation)
		
		-- self.properties.model:draw(self.properties.textures,self.properties.shaders)
		
	-- gfx.transform.pop()
end

function cModelManager:drawBBox()
	if (self.properties.visible ~= true or self.properties.model == nil) then 
		return
	end
	-- gfx.transform.push()
		-- local position = self.entity.component.spatial:position()
		-- local orientation = self.entity.component.spatial:orientation()
		
		-- gfx.transform.translate(unpack(position))
		-- gfx.transform.rotate(orientation)
		
		-- self.properties.model:drawBBox(gfx.color.Red)
		
	-- gfx.transform.pop()
end

function cModelManager:getBoundingBox()
	if (self.properties.model) then
		return self.properties.model.bbox.min,self.properties.model.bbox.max
	end
	return {0,0,0},{0,0,0}
end

function cModelManager:stateWrite(data)
	data.visible = self.properties.visible
	data.path = self.properties.model and self.properties.model.name or nil
	if (self.properties.model and self.properties.model.meshes) then
		data.textures = {}
		data.shaders = {}
		for i,oMesh in ipairs(self.properties.model.meshes) do
			data.textures[i] = {}
			for name,image in pairs(oMesh.textures) do
				data.textures[i][name] = image.name
			end
			data.shaders[i] = oMesh.shader
		end
	end
end 

function cModelManager:stateRead(data)
	self:model(data.path)
	self.properties.visible = data.visible
	self.properties.textures = {}
	if (data.textures) then
		
		for i,t in pairs(data.textures) do
			self.properties.textures[ i ] = {}
			for name,path in pairs(t) do
				self.properties.textures[ i ][ name ] = core.import.image(path)
			end
		end
	end
	self.properties.shaders = {}
	if (data.shaders) then
		for i,v in ipairs(data.shaders) do
			self.properties.shaders[i] = v
		end
	end
end