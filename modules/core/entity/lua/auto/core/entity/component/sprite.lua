-- core.entity.component.sprite()
getmetatable(this).__call = function (self,entity)
	return cSpriteManager(entity)
end

----------------------------------
-- attributes
----------------------------------
Class "cSpriteManager"
function cSpriteManager:__init(entity)
	self.dependencies = {"spatial"}
	self.entity = entity
	self.properties = { visible=true, animate=true, image=nil, layer=nil }
end

function cSpriteManager:image(path)
	if (path == nil) then
		return self.properties.image
	end
	if (self.properties.image and self.properties.image.name == path) then 
		return self.properties.image
	end
	local image_import = core.import.image
	local image = path ~= "" and (image_import.animation.exists(path) and image_import.animation(path,false) or image_import.exists(path) and image_import(path))
	if (image) then
		self.properties.image = image
		if (image.reset) then
			image:reset()
		end
	else
		self.properties.image = nil
	end
	return self.properties.image
end

function cSpriteManager:update(dt,mx,my)
	-- if (self.properties.animate and self.properties.image and self.properties.image.image_animation_update) then
		-- self.properties.image:image_animation_update(dt,mx,my)
	-- end
end

function cSpriteManager:draw()
	if (self.properties.visible ~= true or self.properties.image == nil) then 
		return
	end
	self.entity.component.spatial:set_transform()
	self.properties.image:draw()
end

function cSpriteManager:getTextureID()
	return self.properties.image ~= nil and self.properties.image:getTextureID() or core.import.image.cache.default.texture.idx
end

function cSpriteManager:getRegion()
	if (self.properties.image == nil) then 
		return 0,0,0,0
	end
	return self.properties.image:getRegion()
end

function cSpriteManager:getTextureSize()
	if (self.properties.image == nil) then 
		return 0,0
	end
	return self.properties.image:getTextureSize()
end

function cSpriteManager:stateWrite(data)
	data.path = self.properties.image and self.properties.image.name or nil
end 

function cSpriteManager:stateRead(data)
	self:image(data.path)
end