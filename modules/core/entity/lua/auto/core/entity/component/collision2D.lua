-- core.entity.component.collision2D()
getmetatable(this).__call = function (self,entity)
	local t = {
		dependencies			= {"spatial"},
		entity					= entity,
		boundingBox				= boundingBox,
		inside					= inside,
		useImageSize			= true,
		x						= 0,
		y						= 0,
		w 						= 0,
		h						= 0
	}
	return t
end

function boundingBox(self)
	local w,h = self.w,self.h
	if (self.useImageSize and self.entity.component.sprite) then
		w,h = self.entity.component.sprite:getTextureSize()
	end
	local x,y = unpack(self.entity.component.spatial:position())
	x = x + self.x
	y = y + self.y
	return x-w/2,y-h/2,w,h
end

function inside(self,x,y)
	local x2,y2,w,h = self:boundingBox()
	return x >= x2 and y >= y2 and x <= x2+w and y <= y2+h
end