local nk 				= require("ffi_nuklear")										; if not (nk) then error("failed to load nuklear") end
local bgfx				= require("ffi_bgfx")
cache 				= {} -- for fonts  *TODO* Figure out how to use the baked fonts for graphics.import.font

device = {}
device.view = ffi.new("kmMat4")
device.projection = ffi.new("kmMat4")

-- typedef function pointers are necessary because LuaJIT FFI is not capable of converting functions that pass structs; so instead we use void type.
-- typdef for nk_context because anonymous structs are wonky in FFI
ffi.cdef[[
	typedef void (*nk_clipboard_paste_cb)(void *usr,void *edit);
	typedef void (*nk_clipboard_copy_cb)(void *usr, const char *text, int len);
]]
	
function context()
	return device.context
end

function framework_run()
	CallbackSet("framework_load",on_load)
	CallbackSet("framework_update",on_update,9999)
	CallbackSet("framework_render",framework_render,-9999)
	CallbackSet("framework_exit",on_exit)
	CallbackSet("framework_textinput",framework_textinput)
end

local _textinput = ffi.new("uint16_t[256]")
local _textinput_len = 0
function framework_textinput(codepoint)
	if (_textinput_len < 256) then
		_textinput[_textinput_len] = codepoint
		_textinput_len = _textinput_len + 1
	end
end

function on_update(dt,mx,my)
	local ctx = device.context
	local win = device.win
	nk.nk_input_begin(ctx)
		-- capture text input
		for i=0,_textinput_len-1,1 do
			nk.nk_input_unicode(ctx, _textinput[i])
		end
		_textinput_len = 0
		
		-- capture keyboard input
		nk.nk_input_key(ctx,nk.NK_KEY_DEL,glfw.GetKey(win,glfw.KEY_DELETE) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_ENTER,glfw.GetKey(win,glfw.KEY_ENTER) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_TAB,glfw.GetKey(win,glfw.KEY_TAB) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_BACKSPACE,glfw.GetKey(win,glfw.KEY_BACKSPACE) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_UP,glfw.GetKey(win,glfw.KEY_UP) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_DOWN,glfw.GetKey(win,glfw.KEY_DOWN) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_TEXT_START,glfw.GetKey(win,glfw.KEY_HOME) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_TEXT_END,glfw.GetKey(win,glfw.KEY_END) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_SCROLL_START,glfw.GetKey(win,glfw.KEY_HOME) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_SCROLL_END,glfw.GetKey(win,glfw.KEY_END) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_SCROLL_DOWN,glfw.GetKey(win,glfw.KEY_PAGE_DOWN) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_SCROLL_UP,glfw.GetKey(win,glfw.KEY_PAGE_UP) == glfw.PRESS)
		nk.nk_input_key(ctx,nk.NK_KEY_SHIFT,glfw.GetKey(win,glfw.KEY_LEFT_SHIFT) == glfw.PRESS or glfw.GetKey(win,glfw.KEY_RIGHT_SHIFT) == glfw.PRESS)
				
		if (glfw.GetKey(win,glfw.KEY_LEFT_CONTROL) == glfw.PRESS or glfw.GetKey(win,glfw.KEY_RIGHT_CONTROL) == glfw.PRESS)then
			nk.nk_input_key(ctx,nk.NK_KEY_COPY,glfw.GetKey(win,glfw.KEY_C) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_PASTE,glfw.GetKey(win,glfw.KEY_V) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_CUT,glfw.GetKey(win,glfw.KEY_X) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_TEXT_UNDO,glfw.GetKey(win,glfw.KEY_Z) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_TEXT_REDO,glfw.GetKey(win,glfw.KEY_R) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_TEXT_WORD_LEFT,glfw.GetKey(win,glfw.KEY_LEFT) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_TEXT_WORD_RIGHT,glfw.GetKey(win,glfw.KEY_RIGHT) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_TEXT_LINE_START,glfw.GetKey(win,glfw.KEY_B) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_TEXT_LINE_END,glfw.GetKey(win,glfw.KEY_E) == glfw.PRESS)
		else
			nk.nk_input_key(ctx,nk.NK_KEY_LEFT,glfw.GetKey(win,glfw.KEY_LEFT) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_RIGHT,glfw.GetKey(win,glfw.KEY_RIGHT) == glfw.PRESS)
			nk.nk_input_key(ctx,nk.NK_KEY_COPY,0)
			nk.nk_input_key(ctx,nk.NK_KEY_PASTE,0)
			nk.nk_input_key(ctx,nk.NK_KEY_CUT,0)
			nk.nk_input_key(ctx,nk.NK_KEY_SHIFT,0)
		end
	
		-- capture mouse input
		nk.nk_input_motion(ctx,mx,my)
		nk.nk_input_button(ctx,nk.NK_BUTTON_LEFT,mx,my,glfw.GetMouseButton(win,glfw.MOUSE_BUTTON_LEFT) == glfw.PRESS)
		nk.nk_input_button(ctx,nk.NK_BUTTON_RIGHT,mx,my,glfw.GetMouseButton(win,glfw.MOUSE_BUTTON_RIGHT) == glfw.PRESS)
		nk.nk_input_button(ctx,nk.NK_BUTTON_MIDDLE,mx,my,glfw.GetMouseButton(win,glfw.MOUSE_BUTTON_MIDDLE) == glfw.PRESS)
		nk.nk_input_button(ctx,nk.NK_BUTTON_DOUBLE,mx,my,core.mouse.is_double_click_down)
		local vec2 = ffi.new("struct nk_vec2",core.mouse.scroll_pos)
		nk.nk_input_scroll(ctx,vec2)
		core.mouse.scroll_pos[ 1 ] = 0
		core.mouse.scroll_pos[ 2 ] = 0
		
	nk.nk_input_end(ctx)
end

function on_load()
	print("--------nuklear backend.lua: interface test--------")
	-- setup view and projection
	kazmath.kmMat4Identity(device.projection)
	kazmath.kmMat4Identity(device.view)
	local w,h = core.window.framebufferSize()
	kazmath.kmMat4OrthographicProjection(device.projection,0,w,h,0,-1024,1024)
	
	-- initialize nuklear
	device.win = core.window()
	device.context = ffi.new("struct nk_context")
	nk.nk_init_default(device.context,nil)
	device.context.clip.copy = ffi.cast("nk_clipboard_copy_cb",function(userdata,txt,length)
		local str = ffi.string(txt,length)
		glfw.SetClipboardString(device.win,str)
	end)
	device.context.clip.paste = ffi.cast("nk_clipboard_paste_cb",function(userdata,edit)
		local str = glfw.GetClipboardString(device.win)
		if (str) then
			nk.nk_textedit_paste(edit,str,nk.nk_strlen(str))
		end
	end)
	
	-- Setup font atlas and load fonts from config
	device.null = ffi.new("struct nk_draw_null_texture")
	device.atlas = ffi.new("struct nk_font_atlas[1]")
	nk.nk_font_atlas_init_default(device.atlas)
    nk.nk_font_atlas_begin(device.atlas)
	
	local xml = core.import.xml("config/ui/fonts.xml")
	if (xml and xml.data.font) then
		for i,element in ipairs(xml.data.font) do
			local file = physfs.PHYSFS_openRead(element[ 1 ].filename)
			if (file ~= nil) then
				local length = physfs.PHYSFS_fileLength(file)
				local buffer = ffi.new("char[?]",length)
				physfs.PHYSFS_read(file,buffer,1,length)
				physfs.PHYSFS_close(file)

				local height = tonumber(element[ 1 ].height) or 12
				local pFont = nk.nk_font_atlas_add_from_memory(device.atlas,buffer,length,height,nil)
				cache[ element[ 1 ].name ] = pFont
			end
		end
	end
	
	local w,h = ffi.new("int[1]"),ffi.new("int[1]")
	local data = nk.nk_font_atlas_bake(device.atlas,w,h,nk.NK_FONT_ATLAS_RGBA32)
	local default_font_texture_info = core.import.image.cTextureInfo("default_font")
	default_font_texture_info.storageSize = w[0]*h[0]*4
	default_font_texture_info.width = w[0]
	default_font_texture_info.height = h[0]
	default_font_texture_info.w = w[0]
	default_font_texture_info.h = h[0]
	core.renderer.create_texture(data,default_font_texture_info)
	
	local tex_h = nk.nk_handle_id(default_font_texture_info.id)
	nk.nk_font_atlas_end(device.atlas,tex_h,device.null)
	nk.nk_font_atlas_cleanup(device.atlas)
	
	if (device.atlas[0].default_font ~= nil) then
		nk.nk_style_set_font(device.context,device.atlas[0].default_font.handle)
	end

	-- Set style default font
	if (xml and xml.data.default) then
		local alias = xml.data.default[ 1 ][ 1 ].name
		local default = cache[ alias ]
		assert(default,"ERROR: No default font set!")
		cache.default = default
		nk.nk_style_set_font(device.context,default.handle)
		core.interface._font = "default"
	end

	-- Create the command buffer
	device.cmds = ffi.new("struct nk_buffer[1]")
	nk.nk_buffer_init_default(device.cmds)
	
	-- Generate the vertex declaration
	local vdecl = ffi.new('bgfx_vertex_layout_t[1]')
	bgfx.bgfx_vertex_layout_begin(vdecl,bgfx.BGFX_RENDERER_TYPE_NOOP)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_POSITION,2,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_TEXCOORD0,2,bgfx.BGFX_ATTRIB_TYPE_FLOAT,false,false)
	bgfx.bgfx_vertex_layout_add(vdecl,bgfx.BGFX_ATTRIB_COLOR0,4,bgfx.BGFX_ATTRIB_TYPE_UINT8,true,false)
	bgfx.bgfx_vertex_layout_end(vdecl)
	device.vdecl = vdecl
	
	device.vdecl_h = bgfx.bgfx_create_vertex_layout(vdecl)

	-- Create buffers
	device.maxVertexCount = 65536
	device.maxVertexBufferSize = vdecl[0].stride * device.maxVertexCount
	device.maxElementCount = device.maxVertexCount * 4
	device.maxElementBufferSize = device.maxElementCount * ffi.sizeof("uint16_t")
	
	device.dvb = bgfx.bgfx_create_dynamic_vertex_buffer(device.maxVertexBufferSize,device.vdecl,0)--bgfx.BGFX_BUFFER_ALLOW_RESIZE)
	device.dib = bgfx.bgfx_create_dynamic_index_buffer(device.maxElementCount,0)--bgfx.BGFX_BUFFER_ALLOW_RESIZE)
	device.vbuf = ffi.new("struct nk_buffer[1]")
	device.ebuf = ffi.new("struct nk_buffer[1]")
	local vmem = bgfx.bgfx_alloc(device.maxVertexBufferSize)
	local emem = bgfx.bgfx_alloc(device.maxElementBufferSize)
	nk.nk_buffer_init_fixed(device.vbuf,vmem.data,vmem.size)
	nk.nk_buffer_init_fixed(device.ebuf,emem.data,emem.size)
	device.vmem = vmem
	device.emem = emem
		
	-- Generate the vertex declaration for nuklear
	device.nk_vdecl = ffi.new("struct nk_draw_vertex_layout_element[4]",{
		{ nk.NK_VERTEX_POSITION, nk.NK_FORMAT_FLOAT, vdecl[0].offset[bgfx.BGFX_ATTRIB_POSITION] },
		{ nk.NK_VERTEX_TEXCOORD, nk.NK_FORMAT_FLOAT, vdecl[0].offset[bgfx.BGFX_ATTRIB_TEXCOORD0] },
		{ nk.NK_VERTEX_COLOR, nk.NK_FORMAT_R8G8B8A8, vdecl[0].offset[bgfx.BGFX_ATTRIB_COLOR0] },
		{ nk.NK_VERTEX_ATTRIBUTE_COUNT, nk.NK_FORMAT_COUNT, 0}
	})
	
	-- ffi.cdef([[
		-- struct nk_bgfx_vertex {
			-- float position[2];
			-- float uv[2];
			-- uint8_t col[4];
		-- };	
	-- ]])
	-- Setup nuklear configuration for drawing
	local config = ffi.new("struct nk_convert_config[1]")
	config[0].vertex_layout = device.nk_vdecl --ffi.cast("struct nk_draw_vertex_layout_element *",device.nk_vdecl[0])
	config[0].vertex_size = device.vdecl[0].stride
	config[0].vertex_alignment = 4--ffi.alignof("struct nk_bgfx_vertex")
	config[0].null = device.null
	config[0].circle_segment_count = 22
	config[0].curve_segment_count = 22
	config[0].arc_segment_count = 22
	config[0].global_alpha = 1
	config[0].shape_AA = nk.NK_ANTI_ALIASING_ON
	config[0].line_AA = nk.NK_ANTI_ALIASING_ON
	device.config = config

	print("interface backend.lua test load end")
end

function handle_event(e)
	-- nk.nk_nuklear_handle_event(e)
	
	-- if (nk.nk_item_is_any_active(_context) == 1) then
		-- core.disableInput = true
	-- end
end


function framework_render()
	-- Sets view and projection matrix for view_id 0
	bgfx.bgfx_set_view_transform(0,device.view.mat,device.projection.mat)
	
	local vbuf = device.vbuf --ffi.new("struct nk_buffer[1]")
	local ebuf = device.ebuf --ffi.new("struct nk_buffer[1]")
	-- local tvb = ffi.new("bgfx_transient_vertex_buffer_t[1]")
	-- local tib = ffi.new("bgfx_transient_index_buffer_t[1]")

	
	-- bgfx.bgfx_alloc_transient_vertex_buffer(tvb,device.maxVertexCount,device.vdecl)
	-- bgfx.bgfx_alloc_transient_index_buffer(tib,device.maxElementCount)
	
	-- nk.nk_buffer_init_fixed(vbuf,tvb[0].data,device.maxVertexBufferSize)
	-- nk.nk_buffer_init_fixed(ebuf,tib[0].data,device.maxElementBufferSize)
	-- Fill buffers up with nuklear draw commands
	local result = nk.nk_convert(device.context,device.cmds,vbuf,ebuf,device.config)
	if (result == nk.NK_CONVERT_SUCCESS) then
		-- print("success")
	elseif (result == nk.NK_CONVERT_COMMAND_BUFFER_FULL) then
		print("nk_convert: command buffer full")
	elseif (result == nk.NK_CONVERT_VERTEX_BUFFER_FULL) then
		print("nk_convert: vertex buffer full")
	elseif (result == nk.NK_CONVERT_ELEMENT_BUFFER_FULL) then
		print("nk_convert: element buffer full")
	elseif (result == nk.NK_CONVERT_INVALID_PARAM) then
		print("nk_convert: invalid param")
	else
		print("nk_convert: unknown error")
	end

	local cmd = nk.nk__draw_begin(device.context,device.cmds)
	if (cmd ~= nil) then
		-- update buffers with data from nk buffers filled by nk_convert
		local vmem = bgfx.bgfx_make_ref(vbuf[0].memory.ptr,vbuf[0].allocated)
		local emem = bgfx.bgfx_make_ref(ebuf[0].memory.ptr,ebuf[0].allocated)
		bgfx.bgfx_update_dynamic_vertex_buffer(device.dvb,0,vmem)
		bgfx.bgfx_update_dynamic_index_buffer(device.dib,0,emem)
		
		local vertexCount = vbuf[0].allocated/device.vdecl[0].stride
	
		-- calculate scale and set scissor
		local ww, wh = core.window.size()
		local dw, dh = core.window.framebufferSize() -- TODO: Should be viewport size
		local sx, sy = dw/ww, dh/wh

		-- Set active Shader program to 'nuklear'
		local shader_prog_h = core.renderer.shader.active("nuklear")
		
		local encoder = bgfx.bgfx_encoder_begin(true)
		local count = 0
		local offset = 0
		repeat
			if (cmd.elem_count > 0) then
				local sc_x = math.max(cmd.clip_rect.x*sx,0)
				local sc_y = math.max(cmd.clip_rect.y*sy,0)
				local sc_w = math.max(cmd.clip_rect.w*sx,0)
				local sc_h = math.max(cmd.clip_rect.h*sy,0)
							
				bgfx.bgfx_encoder_set_scissor(encoder,sc_x,sc_y,sc_w,sc_h)
				bgfx.bgfx_encoder_set_state(encoder,bgfx.BGFX_STATE_WRITE_RGBA+bgfx.BGFX_STATE_BLEND_ALPHA,0)
				
				if (cmd.texture.ptr ~= nil) then
					local s_texColor = core.renderer.shader.uniform.s_texColor
					local tex_h = ffi.new("bgfx_texture_handle_t",cmd.texture.id)
					bgfx.bgfx_encoder_set_texture(encoder,0,s_texColor,tex_h,0xffffffff)
				end
				
				bgfx.bgfx_encoder_set_dynamic_vertex_buffer(encoder,0,device.dvb,0,device.maxVertexCount,device.vdecl_h)
				bgfx.bgfx_encoder_set_dynamic_index_buffer(encoder,device.dib,offset,cmd.elem_count)
				-- bgfx.bgfx_encoder_set_transient_vertex_buffer(encoder,0,tvb,0,vertexCount,device.vdecl_h)
				-- bgfx.bgfx_encoder_set_transient_index_buffer(encoder,tib,offset,cmd.elem_count)
			
				offset = offset + cmd.elem_count
				count = count + 1
				-- submit state to active shader program
				bgfx.bgfx_encoder_submit(encoder,0,shader_prog_h,0,false)
			end
			cmd = nk.nk__draw_next(cmd,device.cmds,device.context)
		until (cmd == nil)
		bgfx.bgfx_dbg_text_printf(0,1,0x2f,strformat("NuklearDrawCalls: %s | VertexCount: %s | ElementCount: %s",count,tonumber(vertexCount),offset))
		bgfx.bgfx_encoder_end(encoder)
	end
	
	nk.nk_clear(device.context)
	nk.nk_buffer_clear(vbuf)
	nk.nk_buffer_clear(ebuf)
end

function on_exit()
	if (device.atlas ~= nil) then
		nk.nk_font_atlas_clear(device.atlas)
		device.atlas = nil
	end
	if (device.cmds ~= nil) then
		nk.nk_buffer_free(device.cmds)
		device.cmds = nil
	end
	if (device.context ~= nil) then
		nk.nk_free(device.context)
		device.context = nil
	end
	if (device.vbuf ~= nil) then
		nk.nk_buffer_free(device.vbuf)
		device.vbuf = nil	
	end
	if (device.ebuf ~= nil) then
		nk.nk_buffer_free(device.ebuf)
		device.ebuf = nil	
	end
	if (device.dvb ~= nil) then
		bgfx.bgfx_destroy_dynamic_vertex_buffer(device.dvb)
		device.dvb = nil
	end
	if (device.dib ~= nil) then
		bgfx.bgfx_destroy_dynamic_index_buffer(device.dib)
		device.dib = nil
	end
	if (device.vdecl_h ~= nil) then
		bgfx.bgfx_destroy_vertex_layout(device.vdecl_h)
		device.vdecl_h = nil
	end
end