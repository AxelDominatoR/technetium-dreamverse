local nk 			= require("ffi_nuklear")										; if not (nk) then error("failed to load nuklear") end
_font				= nil 			-- Current font

function setFont(alias)
	alias = alias or "default"
	local pFont = backend.cache[alias]
	if (pFont) then
		nk.nk_style_set_font(backend.context(),pFont.handle)
		_font = alias
	end
end

function getFont()
	return _font and backend.cache[_font]
end