local nk 				= require("ffi_nuklear")										; if not (nk) then error("failed to load nuklear") end

cache = {} -- for xml data
_curStyle = nil

-- core.interface.import.style(path)
getmetatable(this).__call = function(self,path)
	local ctx = core.interface.backend.context()
	nk.nk_style_default(ctx)
	if (path) then
		set_style(path,ctx)
	end
	_curStyle = path
end

----------------------------------------
-- Helpers
----------------------------------------
local defines = {} -- Temporary 'defines' to replace text inline
								
function getRGBA(str)
	str = defines[str] or str
	local r,g,b,a = 0,0,0,255
	if (str) then
		r,g,b,a = string.match(str,"([%d%s]*),([%d%s]*),([%d%s]*),([%d%s]*)")
		r,g,b,a = (tonumber(r) or 0),(tonumber(g) or 0),(tonumber(b) or 0),(tonumber(a) or 255)
	end
	return nk.nk_rgba(r,g,b,a)
end

function getXY(str)
	str = defines[str] or str
	local x,y = string.match(str,"([%d%s]*),([%d%s]*)")
	return nk.nk_vec2(tonumber(x) or 0,tonumber(y) or 0)
end

function getFlags(str)
	str = defines[str] or str
	local flag = 0
	if (str) then
		--for s in str:gsplit("|",true) do
		for s in str:gmatch("([^|]+)") do
			if (nk[s]) then
				flag = bit.bor(flag,nk[s])
			end
		end
	end
	return flag
end

function getEnum(str)
	str = defines[str] or str
	return nk[str]
end

function getValue(str)
	str = defines[str] or str
	return tonumber(str) or 0
end 

function getBool(str)
	str = defines[str] or str
	return str == "true" and 1 or 0
end

function styleItem(str)
	str = defines[str] or str
	return styleItemImage(str) or styleItemColor(str)
end 

function styleItemColor(str)
	str = defines[str] or str
	return nk.nk_style_item_color(getRGBA(str))
end

local image_cache = {}
function styleItemImage(str)
	str = defines[str] or str
	if (str == nil or string.find(str,",")) then
		return
	end

	local image = image_cache[str] or core.import.image.animation.exists(str) and core.import.image.animation(str) or core.import.image(str)
	
	if (image) then
		local img = ffi.new("struct nk_image")
		img.handle = nk.nk_handle_id(image:getTextureID())
		--local w,h = image:getTextureSize()
		img.w = 0
		img.h = 0
		--local region = ffi.new("unsigned short[4]",{0,0,0,0})
		img.region[0] = 0
		img.region[1] = 0
		img.region[2] = 0
		img.region[3] = 0
		return nk.nk_style_item_image(img)
	end
end

---------------------------
-- Style from XML
---------------------------
local styles 					= nil
local styles_map 				= {}
styles_map.text 				= {
									color 				= getRGBA,
									padding 			= getXY
								}
styles_map.button 				= {
									normal 				= styleItem,
									hover				= styleItem,
									active 				= styleItem,
									border_color		= getRGBA,
									text_background		= getRGBA,
									text_normal			= getRGBA,
									text_active			= getRGBA,
									padding 			= getXY,
									image_padding		= getXY,
									touch_padding		= getXY,
									text_alignment		= getFlags,
									border 				= getValue,
									rounding			= getValue
								}
styles_map.contextual_button 	= styles_map.button
styles_map.menu_button 			= styles_map.button
styles_map.checkbox 			= {
									normal 				= styleItem,
									hover				= styleItem,
									active 				= styleItem,
									cursor_normal		= styleItem,
									cursor_hover		= styleItem,
									border_color		= getRGBA,
									text_background		= getRGBA,
									text_normal			= getRGBA,
									text_hover 			= getRGBA,
									text_active			= getRGBA,
									padding 			= getXY,
									touch_padding		= getXY,
									border 				= getValue,
									spacing				= getValue
								}
styles_map.option 				= styles_map.checkbox
styles_map.selectable 			= {
									normal 				= styleItem,
									hover				= styleItem,
									pressed				= styleItem,
									normal_active		= styleItem,
									hover_active		= styleItem,
									pressed_active		= styleItem,
									text_normal			= getRGBA,
									text_hover			= getRGBA,
									text_pressed		= getRGBA,
									text_normal_active	= getRGBA,
									text_hover_active	= getRGBA,
									text_pressed_active	= getRGBA,
									padding 			= getXY,
									touch_padding		= getXY,
									rounding			= getValue
								}
styles_map.slider				= {
									normal				= styleItem,
									hover 				= styleItem,
									active 				= styleItem,
									bar_normal			= getRGBA,
									bar_hover			= getRGBA,
									bar_active			= getRGBA,
									bar_filled			= getRGBA,
									cursor_normal		= styleItem,
									cursor_hover		= styleItem,
									cursor_active		= styleItem,
									inc_symbol			= getEnum,
									dec_symbol			= getEnum,
									cursor_size			= getXY,
									padding 			= getXY,
									spacing				= getXY,
									show_buttons		= getBool,
									bar_height			= getValue,
									rounding			= getValue
								}
styles_map.slider_inc 			= styles_map.button
styles_map.slider_dec 			= styles_map.button
styles_map.progress				= {
									normal				= styleItem,
									hover 				= styleItem,
									active 				= styleItem,
									cursor_normal		= styleItem,
									cursor_hover		= styleItem,
									cursor_active		= styleItem,
									border_color		= getRGBA,
									cursor_border_color = getRGBA,
									padding 			= getXY,
									rounding			= getValue,
									border				= getValue,
									cursor_rounding		= getValue,
									cursor_border		= getValue
								}
styles_map.scrollh				= {
									normal				= styleItem,
									hover 				= styleItem,
									active 				= styleItem,
									cursor_normal		= styleItem,
									cursor_hover		= styleItem,
									cursor_active		= styleItem,
									dec_symbol			= getEnum,
									inc_symbol			= getEnum,
									border_color		= getRGBA,
									cursor_border_color = getRGBA,
									padding 			= getXY,
									show_buttons		= getBool,
									rounding			= getValue,
									border				= getValue,
									border_cursor		= getValue,
									rounding_cursor		= getValue
								}
styles_map.scrollh_inc			= styles_map.button
styles_map.scrollh_dec			= styles_map.button
styles_map.scrollv				= styles_map.scrollh								
styles_map.scrollv_inc			= styles_map.button
styles_map.scrollv_dec			= styles_map.button
styles_map.edit					= {
									normal				= styleItem,
									hover				= styleItem,
									active				= styleItem,
									cursor_normal		= getRGBA,
									cursor_hover		= getRGBA,
									cursor_text_normal	= getRGBA,
									border_color		= getRGBA,
									text_normal			= getRGBA,
									text_hover			= getRGBA,
									text_active			= getRGBA,
									selected_normal		= getRGBA,
									selected_hover		= getRGBA,
									selected_text_normal= getRGBA,
									selected_text_hover	= getRGBA,
									scrollbar_size		= getXY,
									padding				= getXY,
									row_padding			= getValue,
									cursor_size			= getValue,
									border				= getValue,
									rounding			= getValue
								}
styles_map.edit_scroll			= styles_map.scrollv
styles_map.property				= {
									normal				= styleItem,
									hover				= styleItem,
									active				= styleItem,
									border_color		= getRGBA,
									label_normal		= getRGBA,
									label_hover			= getRGBA,
									label_active		= getRGBA,
									sym_left			= getEnum,
									sym_right			= getEnum,
									padding 			= getXY,
									border				= getValue,
									rounding			= getValue
								}
styles_map.property_edit		= styles_map.edit
styles_map.property_inc 		= styles_map.button
styles_map.property_dec 		= styles_map.button
styles_map.combo				= {
									normal 				= styleItem,
									hover				= styleItem,
									active				= styleItem,
									border_color		= getRGBA,
									label_normal		= getRGBA,
									label_hover			= getRGBA,
									label_active		= getRGBA,
									sym_normal			= getEnum,
									sym_hover			= getEnum,
									sym_active			= getEnum,
									content_padding		= getXY,
									button_padding		= getXY,
									spacing				= getXY,
									border				= getValue,
									rounding			= getValue
								}
styles_map.combo_button			= styles_map.button
styles_map.chart				= {
									background			= styleItem,
									border_color		= getRGBA,
									color				= getRGBA,
									padding 			= getXY,
									border				= getValue,
									rounding			= getValue
								}
styles_map.tab					= {
									background			= styleItem,
									border_color		= getRGBA,
									text				= getRGBA,
									sym_minimize		= getEnum,
									sym_maximize		= getEnum,
									padding				= getXY,
									spacing				= getXY,
									indent				= getValue,
									border				= getValue,
									rounding			= getValue
								}
styles_map.tab_minimize 		= styles_map.button
styles_map.tab_maximize 		= styles_map.button
styles_map.tab_node_minimize 	= styles_map.button
styles_map.tab_node_maximize 	= styles_map.button
styles_map.window				= {
									background				= getRGBA,
									fixed_background		= styleItem,
									border_color			= getRGBA,
									popup_border_color		= getRGBA,
									combo_border_color		= getRGBA,
									contextual_border_color	= getRGBA,
									menu_border_color		= getRGBA,
									group_border_color		= getRGBA,
									tooltip_border_color	= getRGBA,
									scaler					= styleItem,
									rounding				= getValue,
									spacing					= getXY,
									scrollbar_size			= getXY,
									min_size				= getXY,
									combo_border			= getValue,
									contextual_border		= getValue,
									menu_border				= getValue,
									group_border			= getValue,
									tooltip_border			= getValue,
									popup_border			= getValue,
									border					= getValue,
									min_row_height_padding	= getValue,
									padding					= getXY,
									group_padding			= getXY,
									popup_padding			= getXY,
									combo_padding			= getXY,
									contextual_padding		= getXY,
									menu_padding			= getXY,
									tooltip_padding			= getXY
								}
styles_map.window_header		= {
									align				= getEnum,
									close_symbol		= getEnum,
									minimize_symbol		= getEnum,
									maximize_symbol		= getEnum,
									normal				= styleItem,
									hover				= styleItem,
									active				= styleItem,
									label_normal		= getRGBA,
									label_hover			= getRGBA,
									label_active		= getRGBA,
									label_padding		= getXY,
									padding				= getXY,
									spacing				= getXY
								}
styles_map.window_close 		= styles_map.button
styles_map.window_minimize 		= styles_map.button

function set_style(path,ctx)
	-- Pointers
	if not (styles) then
		styles = {
			text				= ffi.cast("struct nk_style_text *",ctx.style.text),
			button 				= ffi.cast("struct nk_style_button *",ctx.style.button),
			contextual_button 	= ffi.cast("struct nk_style_button *",ctx.style.contextual_button),
			menu_button			= ffi.cast("struct nk_style_button *",ctx.style.menu_button),
			checkbox			= ffi.cast("struct nk_style_toggle *",ctx.style.checkbox),
			option				= ffi.cast("struct nk_style_toggle *",ctx.style.option),
			selectable			= ffi.cast("struct nk_style_selectable *",ctx.style.selectable),
			slider				= ffi.cast("struct nk_style_slider *",ctx.style.slider),
			slider_inc			= ffi.cast("struct nk_style_button *",ctx.style.slider.inc_button),
			slider_dec			= ffi.cast("struct nk_style_button *",ctx.style.slider.dec_button),
			progress			= ffi.cast("struct nk_style_progress *",ctx.style.progress),
			scrollh				= ffi.cast("struct nk_style_scrollbar *",ctx.style.scrollh),
			scrollh_inc			= ffi.cast("struct nk_style_button *",ctx.style.scrollh.inc_button),
			scrollh_dec			= ffi.cast("struct nk_style_button *",ctx.style.scrollh.dec_button),
			scrollv				= ffi.cast("struct nk_style_scrollbar *",ctx.style.scrollv),
			scrollv_inc			= ffi.cast("struct nk_style_button *",ctx.style.scrollv.inc_button),
			scrollv_dec			= ffi.cast("struct nk_style_button *",ctx.style.scrollv.dec_button),
			edit 				= ffi.cast("struct nk_style_edit *",ctx.style.edit),
			edit_scroll			= ffi.new("struct nk_style_scrollbar"),
			property 			= ffi.cast("struct nk_style_property *",ctx.style.property),
			property_inc		= ffi.cast("struct nk_style_button *",ctx.style.property.inc_button),
			property_dec		= ffi.cast("struct nk_style_button *",ctx.style.property.dec_button),
			property_edit		= ffi.cast("struct nk_style_edit *",ctx.style.property.edit),
			combo 				= ffi.cast("struct nk_style_combo *",ctx.style.combo),
			combo_button		= ffi.cast("struct nk_style_button *",ctx.style.combo.button),
			chart 				= ffi.cast("struct nk_style_chart *",ctx.style.chart),
			tab 				= ffi.cast("struct nk_style_tab *",ctx.style.tab),
			tab_minimize 		= ffi.cast("struct nk_style_button *",ctx.style.tab.tab_minimize_button),
			tab_maximize 		= ffi.cast("struct nk_style_button *",ctx.style.tab.tab_maximize_button),
			tab_node_minimize 	= ffi.cast("struct nk_style_button *",ctx.style.tab.node_minimize_button),
			tab_node_maximize 	= ffi.cast("struct nk_style_button *",ctx.style.tab.node_maximize_button),
			window 				= ffi.cast("struct nk_style_window *",ctx.style.window),
			window_header		= ffi.cast("struct nk_style_window_header *",ctx.style.window.header),
			window_close 		= ffi.cast("struct nk_style_button *",ctx.style.window.header.close_button),
			window_minimize		= ffi.cast("struct nk_style_button *",ctx.style.window.header.minimize_button)
		}
	end
	
	-- Load and cache xml data
	local node = cache[path]
	if not (node) then
		local xml = core.import.xml(path)
		assert(xml,"can't load " .. path)
		node = xml.data
		cache[path] = node
	end
	
	-- Defines are for inline string replacements for simplicity sake
	if (node.defines) then
		for k,v in pairs(node.defines[ 1 ]) do
			if (v[ 1 ]) then
				local val = v[ 1 ][ 2 ] and utils.trim(v[ 1 ][ 2 ]) or ""
				val = val ~= "" and val or nil
				defines[ k ] = val -- transfer value to table
			end
		end
	end
	
	-- automate what would take a tremendous amount of time to write out manually
	for style,ptr in pairs(styles) do
		--assert(styles_map[style],style)
		for attrib,functor in pairs(styles_map[ style ]) do
			if (node[ style ] and node[ style ][ 1 ][ attrib ]) then
				local val = node[ style ][ 1 ][ attrib ][ 1 ][ 2 ]
				val = val ~= "" and val or nil
				local result = functor( val )
				ptr[ attrib ] = result
			end
		end
	end
end