-- It is up to caller to cache or not the script environment

-- core.interface.import.script(path)
getmetatable(this).__call = function(self,path)
	local chunk,err = filesystem.loadfile(path)
	if (chunk ~= nil) then
		local env = {}
		setmetatable(env,{__index=_G})
		setfenv(chunk,env)

		local status,result = pcall(chunk)
		
		if (status) then
			print("loaded interface script: " .. path)
		else
			print(result,debug.traceback(1))
		end
		
		return env
	else
		print(err,debug.traceback(1))
	end
end