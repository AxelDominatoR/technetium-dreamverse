local nk 				= require("ffi_nuklear")										; if not (nk) then error("failed to load nuklear") end
_stack					= {}
_rect 					= ffi.new("struct nk_rect",{0,0,0,0})
_queuedForRemoval		= {}

_depth = { value = 1 }
function _depth:increment()
	self.value = self.value + 1
end
function _depth:decrement()
	self.value = self.value - 1
end

function register(alias,t)
	_stack[ alias ] = t
end

function unregister(alias)
	_stack[ alias ] = nil
end

function framework_run()
	CallbackSet("framework_update",function()
		local ctx = core.interface.backend:context()
		for alias,t in pairs(_stack) do
			_depth.value = 1
			for i,v in ipairs(t) do
				local functor 	= v[ 1 ]			-- callback
				local node 		= v[ 2 ]			-- attribute table
				local data 		= v[ 3 ]			-- storage
				local depth		= v[ 4 ]			-- xml depth
				
				if (depth <= _depth.value) then
					functor(ctx,node,data,depth,_depth)
				end
			end
		end
	end,9998) -- high prior but less than backend.lua
end

local nkEnum = {
	-- Window
	["NK_WINDOW_BORDER"] 					= nk.NK_WINDOW_BORDER,
	["NK_WINDOW_MOVABLE"] 					= nk.NK_WINDOW_MOVABLE,
	["NK_WINDOW_SCALABLE"] 					= nk.NK_WINDOW_SCALABLE,
	["NK_WINDOW_CLOSABLE"] 					= nk.NK_WINDOW_CLOSABLE,
	["NK_WINDOW_MINIMIZABLE"] 				= nk.NK_WINDOW_MINIMIZABLE,
	["NK_WINDOW_NO_SCROLLBAR"] 				= nk.NK_WINDOW_NO_SCROLLBAR,
	["NK_WINDOW_TITLE"] 					= nk.NK_WINDOW_TITLE,
	["NK_WINDOW_SCROLL_AUTO_HIDE"] 			= nk.NK_WINDOW_SCROLL_AUTO_HIDE,
	["NK_WINDOW_BACKGROUND"] 				= nk.NK_WINDOW_BACKGROUND,
	["NK_WINDOW_SCALE_LEFT"] 				= nk.NK_WINDOW_SCALE_LEFT,
	["NK_WINDOW_NO_INPUT"] 					= nk.NK_WINDOW_NO_INPUT,
	-- Layout
	["NK_DYNAMIC"]							= nk.NK_DYNAMIC,
	["NK_STATIC"]							= nk.NK_STATIC,
	-- Text
	["NK_TEXT_ALIGN_LEFT"] 					= nk.NK_TEXT_ALIGN_LEFT,
	["NK_TEXT_ALIGN_CENTERED"] 				= nk.NK_TEXT_ALIGN_CENTERED,
	["NK_TEXT_ALIGN_RIGHT"] 				= nk.NK_TEXT_ALIGN_RIGHT,
	["NK_TEXT_ALIGN_TOP"] 					= nk.NK_TEXT_ALIGN_TOP,
	["NK_TEXT_ALIGN_MIDDLE"] 				= nk.NK_TEXT_ALIGN_MIDDLE,
	["NK_TEXT_ALIGN_BOTTOM"] 				= nk.NK_TEXT_ALIGN_BOTTOM,
	["NK_TEXT_LEFT"]        				= nk.NK_TEXT_LEFT,
	["NK_TEXT_CENTERED"]    				= nk.NK_TEXT_CENTERED,
	["NK_TEXT_RIGHT"]       				= nk.NK_TEXT_RIGHT,
	-- Edit
	["NK_EDIT_DEFAULT"] 					= nk.NK_EDIT_DEFAULT,
	["NK_EDIT_READ_ONLY"] 					= nk.NK_EDIT_READ_ONLY,
	["NK_EDIT_AUTO_SELECT"] 				= nk.NK_EDIT_AUTO_SELECT,
	["NK_EDIT_SIG_ENTER"] 					= nk.NK_EDIT_SIG_ENTER,
	["NK_EDIT_ALLOW_TAB"] 					= nk.NK_EDIT_ALLOW_TAB,
	["NK_EDIT_NO_CURSOR"] 					= nk.NK_EDIT_NO_CURSOR,
	["NK_EDIT_SELECTABLE"] 					= nk.NK_EDIT_SELECTABLE,
	["NK_EDIT_CLIPBOARD"] 					= nk.NK_EDIT_CLIPBOARD,
	["NK_EDIT_CTRL_ENTER_NEWLINE"] 			= nk.NK_EDIT_CTRL_ENTER_NEWLINE,
	["NK_EDIT_NO_HORIZONTAL_SCROLL"] 		= nk.NK_EDIT_NO_HORIZONTAL_SCROLL,
	["NK_EDIT_ALWAYS_INSERT_MODE"] 			= nk.NK_EDIT_ALWAYS_INSERT_MODE,
	["NK_EDIT_MULTILINE"] 					= nk.NK_EDIT_MULTILINE,
	["NK_EDIT_GOTO_END_ON_ACTIVATE"] 		= nk.NK_EDIT_GOTO_END_ON_ACTIVATE,
	["NK_EDIT_SIMPLE"]  					= nk.NK_EDIT_SIMPLE,
	["NK_EDIT_FIELD"]   					= nk.NK_EDIT_FIELD,
	["NK_EDIT_BOX"]     					= nk.NK_EDIT_BOX,
	["NK_EDIT_EDITOR"]  					= nk.NK_EDIT_EDITOR,
	-- Filter
	["nk_filter_default"]					= nk.nk_filter_default,
	["nk_filter_ascii"]						= nk.nk_filter_ascii,
	["nk_filter_float"]						= nk.nk_filter_float,
	["nk_filter_decimal"]					= nk.nk_filter_decimal,
	["nk_filter_hex"]						= nk.nk_filter_hex,
	["nk_filter_oct"]						= nk.nk_filter_oct,
	["nk_filter_binary"]					= nk.nk_filter_binary,
	-- Symbol
	["NK_SYMBOL_NONE"] 						= nk.NK_SYMBOL_NONE,
	["NK_SYMBOL_X"] 						= nk.NK_SYMBOL_X,
	["NK_SYMBOL_UNDERSCORE"] 				= nk.NK_SYMBOL_UNDERSCORE,
	["NK_SYMBOL_CIRCLE_SOLID"]				= nk.NK_SYMBOL_CIRCLE_SOLID,
	["NK_SYMBOL_CIRCLE_OUTLINE"] 			= nk.NK_SYMBOL_CIRCLE_OUTLINE,
	["NK_SYMBOL_RECT_SOLID"] 				= nk.NK_SYMBOL_RECT_SOLID,
	["NK_SYMBOL_RECT_OUTLINE"] 				= nk.NK_SYMBOL_RECT_OUTLINE,
	["NK_SYMBOL_TRIANGLE_UP"] 				= nk.NK_SYMBOL_TRIANGLE_UP,
	["NK_SYMBOL_TRIANGLE_DOWN"] 			= nk.NK_SYMBOL_TRIANGLE_DOWN,
	["NK_SYMBOL_TRIANGLE_LEFT"] 			= nk.NK_SYMBOL_TRIANGLE_LEFT,
	["NK_SYMBOL_TRIANGLE_RIGHT"] 			= nk.NK_SYMBOL_TRIANGLE_RIGHT,
	["NK_SYMBOL_PLUS"] 						= nk.NK_SYMBOL_PLUS,
	["NK_SYMBOL_MINUS"]						= nk.NK_SYMBOL_MINUS,
	["NK_SYMBOL_MAX"] 						= nk.NK_SYMBOL_MAX
}

--[[
@				<window>...</window>
@	id:			Unique name
@	title:		Text
@	x:			X pos
@	y:			Y pos
@	w:			Pixel width
@	h:			Pixel height
@	flags:		{
					NK_WINDOW_BORDER,
					NK_WINDOW_MOVABLE,
					NK_WINDOW_SCALABLE,
					NK_WINDOW_CLOSABLE,
					NK_WINDOW_MINIMIZABLE,
					NK_WINDOW_NO_SCROLLBAR,
					NK_WINDOW_TITLE,
					NK_WINDOW_SCROLL_AUTO_HIDE,
					NK_WINDOW_BACKGROUND,
					NK_WINDOW_SCALE_LEFT,
					NK_WINDOW_NO_INPUT
				}
@	action: 	functor to execute while active
@	init:		functor to execute first run
@	close:		functor to execute when closed or hidden
@ 	minimize:	functor to execute when minimized
All elements need to be children of window tags
--]]
function window(ctx,node,data,depth,curDepth)
	errcheck("window",node,"id")
	
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end

	if not (data[node.id]) then
		data[node.id] = {}
		data[node.id].title = core.translate(node.title) or " "
		data[node.id].x = tonumber(node.x) or 0
		data[node.id].y = tonumber(node.y) or 0
		data[node.id].w = tonumber(node.w) or 0
		data[node.id].h = tonumber(node.h) or 0
		data[node.id].flags = getFlags(node.flags) or 0
		
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	_rect.x = data[node.id].x
	_rect.y = data[node.id].y
	_rect.w = data[node.id].w
	_rect.h = data[node.id].h
	
	if (nk.nk_begin_titled(ctx,node.id,data[node.id].title,_rect,data[node.id].flags) == 1) then
		curDepth:increment()
		parse_action(ctx,node,data,depth,curDepth)
		return true
	else
		--nk.nk_end(ctx)
 		-- if (nk.nk_window_is_closed(ctx,node.id) == 1 or nk.nk_window_is_hidden(ctx,node.id) == 1) then
			-- print(node.id)
			-- parse_close(ctx,node,data,depth,curDepth)
		-- elseif (nk.nk_window_is_collapsed(ctx,node.id) == 1) then
			-- parse_minimize(ctx,node,data,depth,curDepth)
		-- end
	end
	return false
end

function window_end(ctx,node,data,depth,curDepth)
	--if (depth == curDepth.value - 1) then
		curDepth:decrement()
 		if (nk.nk_window_is_closed(ctx,node.id) ~= 0 or nk.nk_window_is_hidden(ctx,node.id) ~= 0 ) then
			print(node.id)
			parse_close(ctx,node,data,depth,curDepth)
			nk.nk_window_close(ctx,node.id)
		elseif (nk.nk_window_is_collapsed(ctx,node.id) ~= 0 ) then
			parse_minimize(ctx,node,data,depth,curDepth)
		end
		nk.nk_end(ctx)
	--end
end

--[[
@				<font />
@	name:		Font alias
--]]
function font(ctx,node,data,depth,curDepth)
	errcheck("font",node,"name")
	core.interface.setFont(node.name)
end

--[[
@				<row_dynamic />
@	height:		Pixel height
@	cols:		Columns
--]]
function row_dynamic(ctx,node,data,depth,curDepth)
	errcheck("row_dynamic",node,"height","cols")
	nk.nk_layout_row_dynamic(ctx,tonumber(node.height),tonumber(node.cols))
end 

--[[
@				<row_static />
@	height:		Pixel height
@	cols:		Columns
@	item_width:	Pixel width
--]]
function row_static(ctx,node,data,depth,curDepth)
	errcheck("row_static",node,"height","item_width","cols")
	nk.nk_layout_row_static(ctx,tonumber(node.height),tonumber(node.item_width),tonumber(node.cols))
end

--[[
@				<layout_row>...<layout_row>
@	format:		{
					NK_DYNAMIC,
					NK_STATIC
				}
@	height:		Pixel height
@	cols:		Columns
--]]
function layout_row(ctx,node,data,depth,curDepth)
	errcheck("layout_row",node,"format","height","cols")
	nk.nk_layout_row_begin(ctx,nkEnum[node.format],tonumber(node.height),tonumber(node.cols))
	curDepth:increment()
end

function layout_row_end(ctx,node,data,depth,curDepth)
	curDepth:decrement()
	nk.nk_layout_row_end(ctx)
end

--[[
@				<layout_row_push>...<layout_row_push>
@	ratio:		ratio of available space to take up
--]]
function layout_row_push(ctx,node,data,depth,curDepth)
	errcheck("layout_row_push",node,"ratio")
	nk.nk_layout_row_push(ctx,tonumber(node.ratio))
end

--[[
@				<layout_space>...<layout_space>
@	format:		{
					NK_DYNAMIC,
					NK_STATIC
				}
@	height:		Pixel height
@	count:		Widget count
--]]
function layout_space(ctx,node,data,depth,curDepth)
	errcheck("layout_space",node,"format","height","count")
	nk.nk_layout_space_begin(ctx,nkEnum[node.format],tonumber(node.height),tonumber(node.count))
	curDepth:increment()
end

function layout_space_end(ctx,node,data,depth,curDepth)
	curDepth:decrement()
	nk.nk_layout_space_end(ctx)
end

--[[
@				<layout_space_push>...<layout_space_push>
@	x:			X pos
@	y:			Y pos
@	w:			Width
@	h:			Height
@	init:		functor to execute first run
--]]
function layout_space_push(ctx,node,data,depth,curDepth)
	errcheck("layout_space_push",node,"x","y","w","h")
	
	_rect.x = tonumber(node.x) or 0
	_rect.y = tonumber(node.y) or 0
	_rect.w = tonumber(node.w) or 0
	_rect.h = tonumber(node.h) or 0
	
	nk.nk_layout_space_push(ctx,_rect)
end

--[[
@				<action />
@	functor:	Functor to execute when element processed
@	info:		'-text' disables global switch; '+text' creates a global switch
@ Optionally can have 'functor' or info' or both
--]]
function action(ctx,node,data,depth,curDepth)
	local script = data.__scriptEnv
	if (script and script[node.functor]) then
		script[node.functor](ctx,node,data,depth,curDepth)
	end
	if (node.info) then
		local typ,info = node.info:sub(1,1),node.info:sub(2,node.info:len())
		if (typ == "-") then 
			data.__info[info] = nil
		elseif (typ == "+") then 
			data.__info[info] = true
		end
	end
end

--[[
@				<precondition />
@	functor:	Functor; If returns 'true' child nodes will be executed
@	info:		'-text' will trigger if global switch does not exist; '+text' will trigger if switch exists
@ Optionally can have 'functor' or info' or both
--]]
function precondition(ctx,node,data,depth,curDepth)
	local success = true
	
	if (node.info) then
		local typ,info = node.info:sub(1,1),node.info:sub(2,node.info:len())
		if (typ == "-") then 
			if (data.__info[info]) then 
				success = false
			end
		elseif (typ == "+") then 
			if not (data.__info[info]) then 
				success = false
			end	
		end
	end
	
	local script = data.__scriptEnv
	if (script and script[node.functor]) then 
		if (script[node.functor](ctx,node,data,depth,curDepth) ~= true) then
			success = false
		end
	end
	
	if (success) then 
		curDepth:increment()
	end
end

function precondition_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
	end
end

--[[
@				<hover>...</hover>
@	name:		name(s) of widget(s) mouse must hover to trigger node. Separate more than one with |
@	delay: 		sec delay (can be float)
@	action: 	functor to execute while active
--]]
function hover(ctx,node,data,depth,curDepth)
	errcheck("hover",node,"name")
	
	if (node.name) then
		--for s in node.name:gsplit("|",true) do
		for s in node.name:gmatch("([^|]+)") do
			if (data[s] and data[s].bounds) then
				if (nk.nk_input_is_mouse_hovering_rect(ctx.input,data[s].bounds) == 1) then
					if not (data[s].__hover_delay) then
						data[s].__hover_delay = time.time() + (tonumber(node.delay) or 0)
					end
					if (time.time() >= data[s].__hover_delay) then
						curDepth:increment()
						parse_action(ctx,node,data,depth,curDepth)
					end
				else
					data[s].__hover_delay = nil
				end 
			end
		end
	end
end

function hover_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
	end
end

--[[
@				<label>content</label>
@	id:			Unique name
@	wrap:		'true' or 'false'
@	r:			0-255 Red
@	g:			0-255 Green
@	b:			0-255 Blue
@	a:			0-255 Alpha
@	action: 	functor to execute while active
@	flags: 		{
					NK_TEXT_ALIGN_LEFT,
					NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_ALIGN_RIGHT,
					NK_TEXT_ALIGN_TOP,
					NK_TEXT_ALIGN_MIDDLE,
					NK_TEXT_ALIGN_BOTTOM,
							or
					NK_TEXT_LEFT        = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_LEFT,
					NK_TEXT_CENTERED    = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_RIGHT       = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_RIGHT
				}
@	init:		functor to execute first run
--]]
function label(ctx,node,data,depth,curDepth)
	errcheck("label",node,"flags")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end
	if not (data[node.id]) then
		data[node.id] = {}
		
		if (node.r or node.g or node.b or node.a) then
			local color = ffi.new("struct nk_color")
			color.r = tonumber(node.r) or 0
			color.g = tonumber(node.g) or 0
			color.b = tonumber(node.b) or 0
			color.a = tonumber(node.a) or 255
		
			data[node.id].color = color
			parse_init(ctx,node,data,depth,curDepth)
		end
		
		data[node.id].text = core.translate(node.__content) or ""
		data[node.id].wrap = node.wrap == "true"
		data[node.id].flags = getFlags(node.flags) or nk.NK_TEXT_ALIGN_LEFT
	end
	
	if (data[node.id].wrap) then
		if (data[node.id].color) then
			nk.nk_label_colored_wrap(ctx,data[node.id].text,data[node.id].color)
		else 
			nk.nk_label_wrap(ctx,data[node.id].text)
		end
	else 
		if (data[node.id].color) then
			nk.nk_label_colored(ctx,data[node.id].text,data[node.id].flags,data[node.id].color)
		else
			nk.nk_label(ctx,data[node.id].text,data[node.id].flags)
		end
	end
	
	parse_action(ctx,node,data,depth,curDepth)
end

--[[
@				<button>content</button>
@	id:			Unique name
@	image:		[Optional] path or image atlas alias or image animation alias
@	symbol:	 	[Optional]{
					NK_SYMBOL_NONE,
					NK_SYMBOL_X,
					NK_SYMBOL_UNDERSCORE,
					NK_SYMBOL_CIRCLE_SOLID,
					NK_SYMBOL_CIRCLE_OUTLINE,
					NK_SYMBOL_RECT_SOLID,
					NK_SYMBOL_RECT_OUTLINE,
					NK_SYMBOL_TRIANGLE_UP,
					NK_SYMBOL_TRIANGLE_DOWN,
					NK_SYMBOL_TRIANGLE_LEFT,
					NK_SYMBOL_TRIANGLE_RIGHT,
					NK_SYMBOL_PLUS,
					NK_SYMBOL_MINUS,
					NK_SYMBOL_MAX
				}
@	flags:		{
					NK_TEXT_ALIGN_LEFT,
					NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_ALIGN_RIGHT,
					NK_TEXT_ALIGN_TOP,
					NK_TEXT_ALIGN_MIDDLE,
					NK_TEXT_ALIGN_BOTTOM,
							or
					NK_TEXT_LEFT        = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_LEFT,
					NK_TEXT_CENTERED    = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_RIGHT       = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_RIGHT
				}
@	action: 	functor to execute on state change
@	init:		functor to execute first run
--]]
function button(ctx,node,data,depth,curDepth)
	errcheck("button",node,"id")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end
	if not (data[node.id]) then
		data[node.id] = {}
		data[node.id].image = getImage(node.image,data[node.id].image)
		data[node.id].symbol = node.symbol and nkEnum[node.symbol]
		data[node.id].content = core.translate(node.__content)
		data[node.id].flags = getFlags(node.flags) or 0
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	local image = data[node.id].image
	if (image) then
		local img = ffi.new("struct nk_image")
		img.handle = nk.nk_handle_id(image:getTextureID())
		
		--local w,h = image:getTextureSize()
		img.w = 0
		img.h = 0
		
		--local region = ffi.new("unsigned short[4]",{image:getRegion()})
		img.region[0] = 0
		img.region[1] = 0
		img.region[2] = 0
		img.region[3] = 0
		
		if (data[node.id].content) then
			if (nk.nk_button_image_label(ctx,img,data[node.id].content,getFlags(flags)) == 1) then
				parse_action(ctx,node,data,depth,curDepth)
				return true
			end
		elseif (nk.nk_button_image(ctx,img) == 1) then
			parse_action(ctx,node,data,depth,curDepth)
			return true
		end
		return
	end

	local symbol = data[node.id].symbol
	if (symbol) then
		if (data[node.id].content) then 
			if (nk.nk_button_symbol_label(ctx,symbol,data[node.id].content,data[node.id].flags) == 1) then
				parse_action(ctx,node,data,depth,curDepth)
				return true
			end
		elseif (nk.nk_button_symbol(ctx,symbol) == 1) then
			parse_action(ctx,node,data,depth,curDepth)
			return true
		end
		return
	end
	
	if (nk.nk_button_label(ctx,data[node.id].content or "") == 1) then
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end
end

--[[
@			<button_color />
@	id:		Unique name
@	r:		0-255 Red
@	g:		0-255 Green
@	b:		0-255 Blue
@	a:		0-255 Alpha
@	action: functor to execute on press
@	init:		functor to execute first run
--]]
function button_color(ctx,node,data,depth,curDepth)
	errcheck("button_color",node,"id")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		
		local color = ffi.new("struct nk_color")
		color.r = tonumber(node.r) or 0
		color.g = tonumber(node.g) or 0
		color.b = tonumber(node.b) or 0
		color.a = tonumber(node.a) or 255
	
		data[node.id].color = color
		parse_init(ctx,node,data,depth,curDepth)
	end

	if (nk.nk_button_color(ctx,data[node.id].color) == 1) then
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end
end

--[[
@				<check_label>content</check_label>
@	id:			Unique name
@	active:		'true' or 'false'
@	init:		functor to execute first run
--]]
function check_label(ctx,node,data,depth,curDepth)
	errcheck("check_label",node,"id","active")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		data[node.id].active = node.active == "true"
		data[node.id].content = core.translate(node.__content) or " "
		
		parse_init(ctx,node,data,depth,curDepth)
	end 
	
	data[node.id]._active = not data[node.id].active
	
	local b = nk.nk_check_label(ctx,data[node.id].content,data[node.id]._active)
	if (b == 1) then
		if (data[node.id].active ~= false) then
			parse_action(ctx,node,data,depth,curDepth)
		end
		data[node.id]._active = true
		data[node.id].active = false
	else
		if (data[node.id].active ~= true) then
			parse_action(ctx,node,data,depth,curDepth)
		end
		data[node.id]._active = false
		data[node.id].active = true
	end
	
	return data[node.id].active
end

--[[
@				<option_label>content</option_label>
@	id:			Unique name
@	action: 	functor to execute on state change
@	active:		'true' or 'false'
@	init:		functor to execute first run
--]]
function option_label(ctx,node,data,depth,curDepth)
	errcheck("option_label",node,"id","active")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		data[node.id].active = node.active == "true"
		data[node.id].content = core.translate(node.__content) or " "
		
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	data[node.id]._active = not data[node.id].active
	
	local b = nk.nk_option_label(ctx,data[node.id].content,data[node.id]._active)
	if (b == 1) then
		if (data[node.id].active ~= false) then
			parse_action(ctx,node,data,depth,curDepth)
		end
		data[node.id]._active = true
		data[node.id].active = false
	else
		if (data[node.id].active ~= true) then
			parse_action(ctx,node,data,depth,curDepth)
		end
		data[node.id]._active = false
		data[node.id].active = true
	end
	
	return data[node.id].active
end

--[[
@				<select_label>content</select_label>
@	id:			Unique name
@	action: 	functor to execute on state change
@	active:		'true' or 'false'
@	flags:		{
					NK_TEXT_ALIGN_LEFT,
					NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_ALIGN_RIGHT,
					NK_TEXT_ALIGN_TOP,
					NK_TEXT_ALIGN_MIDDLE,
					NK_TEXT_ALIGN_BOTTOM,
							or
					NK_TEXT_LEFT        = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_LEFT,
					NK_TEXT_CENTERED    = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_RIGHT       = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_RIGHT
				}
@	image:		[Optional] path or image atlas alias or image animation alias
@	symbol:		[Optional] {
					NK_SYMBOL_NONE,
					NK_SYMBOL_X,
					NK_SYMBOL_UNDERSCORE,
					NK_SYMBOL_CIRCLE_SOLID,
					NK_SYMBOL_CIRCLE_OUTLINE,
					NK_SYMBOL_RECT_SOLID,
					NK_SYMBOL_RECT_OUTLINE,
					NK_SYMBOL_TRIANGLE_UP,
					NK_SYMBOL_TRIANGLE_DOWN,
					NK_SYMBOL_TRIANGLE_LEFT,
					NK_SYMBOL_TRIANGLE_RIGHT,
					NK_SYMBOL_PLUS,
					NK_SYMBOL_MINUS,
					NK_SYMBOL_MAX
				}
@	init:		functor to execute first run
--]]
function select_label(ctx,node,data,depth,curDepth)
	errcheck("select_label",node,"id")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		data[node.id].active = node.active == "true"
		data[node.id].content = core.translate(node.__content) or " "
		data[node.id].flags = getFlags(node.flags) or nk.NK_TEXT_LEFT
		data[node.id].symbol = node.symbol and nkEnum[node.symbol] or nil
		data[node.id].image = getImage(node.image,data[node.id].image)
		
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	-- image
	local image = data[node.id].image
	if (image) then
		local img = ffi.new("struct nk_image")
		img.handle = nk.nk_handle_id(image:getTextureID())
		
		local w,h = image:getTextureSize()
		img.w = w
		img.h = h
		
		local region = ffi.new("unsigned short[4]",{image:getRegion()})
		img.region = region
		
		if (nk.nk_select_image_label(ctx,img,data[node.id].content,data[node.id].flags,data[node.id].active) == 1) then
			if not (data[node.id].active) then 
				data[node.id].active = true
				parse_action(ctx,node,data,depth,curDepth)
				return true
			end
		else
			if (data[node.id].active) then
				data[node.id].active = false
				parse_action(ctx,node,data,depth,curDepth)
				return false
			end
		end
		return
	end
	
	-- symbol
	local symbol = data[node.id].symbol
	if (symbol) then
		if (nk.nk_select_symbol_label(ctx,symbol,data[node.id].content,data[node.id].flags,data[node.id].active) == 1) then
			if not (data[node.id].active) then 
				data[node.id].active = true
				parse_action(ctx,node,data,depth,curDepth)
				return true
			end
		else
			if (data[node.id].active) then
				data[node.id].active = false
				parse_action(ctx,node,data,depth,curDepth)
				return false
			end
		end
		return
	end
	
	-- default
	if (nk.nk_select_label(ctx,data[node.id].content,data[node.id].flags,data[node.id].active) == 1) then
		if not (data[node.id].active) then 
			data[node.id].active = true
			parse_action(ctx,node,data,depth,curDepth)
			return true
		end
	else
		if (data[node.id].active) then
			data[node.id].active = false
			parse_action(ctx,node,data,depth,curDepth)
			return false
		end
	end 
end

--[[
@				<slider />
@	id:			Unique name
@	min:		Minimum value possible
@	value:		Initial value
@	max:		Maximum value possible
@	step:		Increment
@	action: 	functor to execute on state change
@	init:		functor to execute first run
--]]
function slider(ctx,node,data,depth,curDepth)
	errcheck("slider",node,"id","min","value","max","step")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].min = tonumber(node.min) or 0
		data[node.id].max = tonumber(node.max) or 1
		data[node.id].value = ffi.new("float[1]",(tonumber(node.value) or 0))
		data[node.id].step = tonumber(node.step) or 0.1
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	if (nk.nk_slider_float(ctx,data[node.id].min,data[node.id].value,data[node.id].max,data[node.id].step) == 1) then
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end
end

--[[
@				<progress />
@	id:			Unique name
@	value:		Initial value
@	max:		Maximum value possible
@	modifiable:	'true' or 'false'
@	action: 	functor to execute on state change
@	init:		functor to execute first run
--]]
function progress(ctx,node,data,depth,curDepth)
	errcheck("progress",node,"id","value","max","modifiable")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end
	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].max = tonumber(node.max) or 1
		data[node.id].value = ffi.new("nk_size[1]",(tonumber(node.value) or 0))
		data[node.id].modifiable = node.modifiable == "true"
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	if (nk.nk_progress(ctx,data[node.id].value,data[node.id].max,data[node.id].modifiable) == 1) then
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end
end

--[[
@				<color_pick />
@	id:			Unique name
@	r:			0-255 Red
@	g:			0-255 Green
@	b:			0-255 Blue
@	a:			0-255 Alpha
@	format:		{
					NK_RGB, 
					NK_RGBA
				}
@	action: 	functor to execute on state change
@	init:		functor to execute first run
--]]
function color_pick(ctx,node,data,depth,curDepth)
	errcheck("color_pick",node,"id","format")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		
		if (node.r or node.g or node.b or node.a) then
			local color = ffi.new("struct nk_colorf[1]")
			color[0].r = tonumber(node.r)/255 or 0
			color[0].g = tonumber(node.g)/255 or 0
			color[0].b = tonumber(node.b)/255 or 0
			color[0].a = tonumber(node.a)/255 or 1
		
			data[node.id].color = color
			parse_init(ctx,node,data,depth,curDepth)
		end
		
		data[node.id].format = nkEnum[node.format] or nk.NK_RGBA
	end
	
	if (nk.nk_color_pick(ctx,data[node.id].color,data[node.id].format) == 1) then
		parse_action(ctx,node,data,depth,curDepth)
		return true		
	end
end

--[[
@				<edit_string>content</edit_string>
@	id: 		Unique name
@	max: 		Max characters allowed
@	filter:		{
					nk_filter_default,
					nk_filter_ascii,
					nk_filter_float,
					nk_filter_decimal,
					nk_filter_hex,
					nk_filter_oct,
					nk_filter_binary
				}
@	flags:		{
					NK_EDIT_DEFAULT,
					NK_EDIT_READ_ONLY,
					NK_EDIT_AUTO_SELECT,
					NK_EDIT_SIG_ENTER,
					NK_EDIT_ALLOW_TAB,
					NK_EDIT_NO_CURSOR,
					NK_EDIT_SELECTABLE,
					NK_EDIT_CLIPBOARD,
					NK_EDIT_CTRL_ENTER_NEWLINE,
					NK_EDIT_NO_HORIZONTAL_SCROLL,
					NK_EDIT_ALWAYS_INSERT_MODE,
					NK_EDIT_MULTILINE,
					NK_EDIT_GOTO_END_ON_ACTIVATE,
							or 
					NK_EDIT_SIMPLE  = NK_EDIT_ALWAYS_INSERT_MODE,
					NK_EDIT_FIELD   = NK_EDIT_SIMPLE|NK_EDIT_SELECTABLE|NK_EDIT_CLIPBOARD,
					NK_EDIT_BOX     = NK_EDIT_ALWAYS_INSERT_MODE| NK_EDIT_SELECTABLE| NK_EDIT_MULTILINE|NK_EDIT_ALLOW_TAB|NK_EDIT_CLIPBOARD,
					NK_EDIT_EDITOR  = NK_EDIT_SELECTABLE|NK_EDIT_MULTILINE|NK_EDIT_ALLOW_TAB| NK_EDIT_CLIPBOARD
				}
@	init:		functor to execute first run
@	action:		functor to execute on change
--]]
function edit_string(ctx,node,data,depth,curDepth)
	errcheck("edit_string",node,"id","max","filter")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end
	if not (data[node.id]) then
		data[node.id] = {}
		data[node.id].max = tonumber(node.max) or 255
		data[node.id].text = ffi.new("char[?]",data[node.id].max,core.translate(node.__content) or "")
		data[node.id].prevText = ffi.string(data[node.id].text)
		data[node.id].flags = getFlags(node.flags) or nk.NK_EDIT_SIMPLE
		data[node.id].filter = nkEnum[node.filter] or nk.nk_filter_default
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	nk.nk_edit_string_zero_terminated(ctx,data[node.id].flags,data[node.id].text,data[node.id].max,data[node.id].filter)
	
	local curText = ffi.string(data[node.id].text)
	if (data[node.id].prevText ~= curText) then
		parse_action(ctx,node,data,depth,curDepth)
		data[node.id].prevText = curText
		return true
	end
end

--[[
@				<property />
@	id: 		Unique name
@	title:		Text
@	min:		Min value
@	value:		Initial value
@	max: 		Max value
@	step:		Increment/Decrement on button press
@	inc:		Increment on slider change
@	action: 	functor to execute on state change
@	init:		functor to execute first run
--]]
function property(ctx,node,data,depth,curDepth)
	errcheck("property",node,"id","min","value","max","step","inc")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end
	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].min = tonumber(node.min) or 0
		data[node.id].max = tonumber(node.max) or 1
		data[node.id].value = ffi.new("float[1]",(tonumber(node.value) or 0))
		data[node.id].step = tonumber(node.step) or 0.1
		data[node.id].inc = tonumber(node.inc) or 0.1
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	local old = data[node.id].value[0]
	nk.nk_property_float(ctx,"#"..(node.title or node.id),data[node.id].min,data[node.id].value,data[node.id].max,data[node.id].step,data[node.id].inc)
	
	if (data[node.id].value[0] ~= old) then
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end
end

--[[
@				<tree>...</tree>
@id: 			Unique name
@title:			Text
@type:			{
					NK_TREE_NODE, 
					NK_TREE_TAB
				}
@flags:			{
					NK_MINIMIZED,
					NK_MAXIMIZED
				}
@	action: 	functor to execute while active
@	init:		functor to execute first run
--]]
function tree(ctx,node,data,depth,curDepth)
	errcheck("tree",node,"id","title","type","flags")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end
	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].title = node.title or " "
		data[node.id].type = nkEnum[node.type] or nk.NK_TREE_TAB
		data[node.id].flags = nkEnum[node.flags] or nk.NK_MINIMIZED
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	local uid = node.id
	if (nk.nk_tree_push_hashed(ctx,data[node.id].type,data[node.id].title,data[node.id].flags,uid,#uid,0) == 1) then 
		curDepth:increment()
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end
end

function tree_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
		nk.nk_tree_pop(ctx)
	end
end

--[[
@					<list_view>...</list_view>
@	id: 			Unique name
@	title:			Text
@	height:			Row height
@	count:			Row count
@	flags:			{
						NK_WINDOW_BORDER,
						NK_WINDOW_MOVABLE,
						NK_WINDOW_SCALABLE,
						NK_WINDOW_CLOSABLE,
						NK_WINDOW_MINIMIZABLE,
						NK_WINDOW_NO_SCROLLBAR,
						NK_WINDOW_TITLE,
						NK_WINDOW_SCROLL_AUTO_HIDE,
						NK_WINDOW_BACKGROUND,
						NK_WINDOW_SCALE_LEFT,
						NK_WINDOW_NO_INPUT
					}
@	action: 		functor to execute while active
@	init:			functor to execute first run
--]]
function list_view(ctx,node,data,depth,curDepth)
	errcheck("list_view",node,"id","height","count")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end
	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].title = node.title or " "
		data[node.id].view = ffi.new("struct nk_list_view[1]")
		data[node.id].count = tonumber(node.count) or 1024
		data[node.id].height = tonumber(node.height) or 25
		data[node.id].flags = getFlags(node.flags) or 0
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	-- TODO: Scrollbar is busted because FFI and passing nk_list_view. Just use <group>
	if (nk.nk_list_view_begin(ctx,data[node.id].view,data[node.id].title,data[node.id].flags,data[node.id].height,data[node.id].count) == 1) then 
		curDepth:increment()
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end
end

function list_view_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
		local view = data[node.id].view
		nk.nk_list_view_end(view)
	end
end

--[[
@				<popup>...</popup>
@id: 			Unique name
@type:			{
					NK_POPUP_STATIC,
					NK_POPUP_DYNAMIC
				}
@flags:			{
					NK_WINDOW_BORDER,
					NK_WINDOW_MOVABLE,
					NK_WINDOW_SCALABLE,
					NK_WINDOW_CLOSABLE,
					NK_WINDOW_MINIMIZABLE,
					NK_WINDOW_NO_SCROLLBAR,
					NK_WINDOW_TITLE,
					NK_WINDOW_SCROLL_AUTO_HIDE,
					NK_WINDOW_BACKGROUND,
					NK_WINDOW_SCALE_LEFT,
					NK_WINDOW_NO_INPUT
				}
@	action: 	functor to execute while active
@	init:		functor to execute first run
--]]
function popup(ctx,node,data,depth,curDepth)
	errcheck("popup",node,"id")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end
	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].title = core.translate(node.title) or ""
		data[node.id].x = tonumber(node.x) or 0
		data[node.id].y = tonumber(node.y) or 0
		data[node.id].w = tonumber(node.w) or 0
		data[node.id].h = tonumber(node.h) or 0
		data[node.id].type = nkEnum[node.type] or nk.NK_POPUP_STATIC
		data[node.id].flags = getFlags(node.flags) or 0
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	_rect.x = data[node.id].x
	_rect.y = data[node.id].y
	_rect.w = data[node.id].w
	_rect.h = data[node.id].h
	
	if (nk.nk_popup_begin(ctx,data[node.id].type,data[node.id].title,data[node.id].flags,_rect) == 1) then
		curDepth:increment()
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end 
end

function popup_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
		nk.nk_popup_end(ctx)
	end
end

--[[
@					<tooltip>...</tooltip>
@	id:				Unique name
@	width:			Pixel width
@	init:			functor to execute first run
@ 	action:			functor to execute when active
--]]
-- TODO: Tooltip crashes when popup is active
function tooltip(ctx,node,data,depth,curDepth)
	errcheck("tooltip",node,"id")

	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].width = tonumber(node.width) or 0
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	if (nk.nk_tooltip_begin(ctx,data[node.id].width) == 1) then 
		curDepth:increment()
		parse_action(ctx,node,data,depth,curDepth)
	end
end

function tooltip_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
		nk.nk_tooltip_end(ctx)
	end
end

--[[
@				<combo>...</combo>
@	id:			Unique name
@	selected:	Text
@	w:			Pixel width
@	h:			Pixel height
@	image:		[Optional] path or image atlas alias or image animation alias
@	action: 	functor to execute while active
@	init:		functor to execute first run
--]]
function combo(ctx,node,data,depth,curDepth)
	errcheck("combo",node,"id")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		data[node.id].image = getImage(node.image,data[node.id].image)
		data[node.id].w = tonumber(node.w) or 200
		data[node.id].h = tonumber(node.h) or 200
		data[node.id].selected = core.translate(node.selected)
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	local size = ffi.new("struct nk_vec2",{data[node.id].w,data[node.id].h})

	local image = data[node.id].image
	if (image) then
		local img = ffi.new("struct nk_image")
		img.handle = nk.nk_handle_id(image:getTextureID())
		
		local w,h = image:getTextureSize()
		img.w = w
		img.h = h
		
		local region = ffi.new("unsigned short[4]",{image:getRegion()})
		img.region = region
		
		if not (data[node.id].selected) then
			if (nk.nk_combo_begin_image(ctx,img,size) == 1) then 
				parse_action(ctx,node,data,depth,curDepth)
				curDepth:increment()
				return true
			end
		elseif (nk.nk_combo_begin_image_label(ctx,data[node.id].selected,img,size) == 1) then 
			parse_action(ctx,node,data,depth,curDepth)
			curDepth:increment()
			return true
		end
		return
	end
	
	if (nk.nk_combo_begin_label(ctx,data[node.id].selected or "",size) == 1) then
		parse_action(ctx,node,data,depth,curDepth)
		curDepth:increment()
		return true
	end
end

function combo_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
		nk.nk_combo_end(ctx)
	end
end

--[[
@				<combo_color>...</combo_color>
@	id:			Unique name
@	r:			0-255 Red
@	g:			0-255 Green
@	b:			0-255 Blue
@	a:			0-255 Alpha
@	action: 	functor to execute while active
@	init:		functor to execute first run
--]]
function combo_color(ctx,node,data,depth,curDepth)
	errcheck("combo_color",node,"id")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		
		local color = ffi.new("struct nk_color[1]")
		color[0].r = tonumber(node.r) or 0
		color[0].g = tonumber(node.g) or 0
		color[0].b = tonumber(node.b) or 0
		color[0].a = tonumber(node.a) or 255
		
		data[node.id].color = color
		data[node.id].w = tonumber(node.w) or 200
		data[node.id].h = tonumber(node.h) or 200
		
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	local size = ffi.new("struct nk_vec2",{data[node.id].w,data[node.id].h})
	
	if (nk.nk_combo_begin_color(ctx,data[node.id].color[0],size) == 1) then 
		parse_action(ctx,node,data,depth,curDepth)
		curDepth:increment()
		return true
	end
end

function combo_color_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
		nk.nk_combo_end(ctx)
	end
end

--[[
@				<context_menu>...</context_menu>
@	id:			Unique name
@	bound:		ID of widget to right-click or omit for entire screen
@	w			Pixel width
@	h			Pixel height
@	flags:		{
					NK_WINDOW_BORDER,
					NK_WINDOW_MOVABLE,
					NK_WINDOW_SCALABLE,
					NK_WINDOW_CLOSABLE,
					NK_WINDOW_MINIMIZABLE,
					NK_WINDOW_NO_SCROLLBAR,
					NK_WINDOW_TITLE,
					NK_WINDOW_SCROLL_AUTO_HIDE,
					NK_WINDOW_BACKGROUND,
					NK_WINDOW_SCALE_LEFT,
					NK_WINDOW_NO_INPUT
				}
@	action: 	functor to execute while active
@	init:		functor to execute first run
--]]
function context_menu(ctx,node,data,depth,curDepth)
	errcheck("context_menu",node,"id","w","h")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].w = tonumber(node.w) or 200
		data[node.id].h = tonumber(node.h) or 200
		data[node.id].flags = getFlags(node.flags) or 0
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	local size = ffi.new("struct nk_vec2",{data[node.id].w,data[node.id].h})
	
	local rect = data[node.bound] and data[node.bound].bounds
	if not (rect) then
		rect = ffi.new("struct nk_rect")
		local w,h = core.window.framebufferSize() -- TODO: Should be viewport size
		rect.x = 0
		rect.y = 0
		rect.w = w
		rect.h = h
	end
	
	if (rect and nk.nk_contextual_begin(ctx,data[node.id].flags,size,rect) == 1) then
		data[node.id].isActive = true
		parse_action(ctx,node,data,depth,curDepth)
		curDepth:increment()
		return true
	else
		data[node.id].isActive = false
	end
end

function context_menu_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
		nk.nk_contextual_end(ctx)
	end
end
--[[
@				<contextual_label>content</contextual_label>
@	id:			Unique name
@	flags:		{
					NK_TEXT_ALIGN_LEFT,
					NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_ALIGN_RIGHT,
					NK_TEXT_ALIGN_TOP,
					NK_TEXT_ALIGN_MIDDLE,
					NK_TEXT_ALIGN_BOTTOM,
							or
					NK_TEXT_LEFT        = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_LEFT,
					NK_TEXT_CENTERED    = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_RIGHT       = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_RIGHT
				}
@	image:		[Optional] path or image atlas alias or image animation alias
@	action: 	functor to execute on state change
@	init:		functor to execute first run
--]]
function contextual_label(ctx,node,data,depth,curDepth)
	errcheck("contextual_label",node,"id")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		data[node.id].image = getImage(node.image,data[node.id].image)
		data[node.id].content = core.translate(node.__content) or " "
		data[node.id].flags = getFlags(node.flags) or nk.NK_TEXT_CENTERED
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	local image = data[node.id].image
	if (image) then
		local img = ffi.new("struct nk_image")
		img.handle = nk.nk_handle_id(image:getTextureID())
		
		local w,h = image:getTextureSize()
		img.w = w
		img.h = h
		
		local region = ffi.new("unsigned short[4]",{image:getRegion()})
		img.region = region

		if (nk.nk_contextual_item_image_label(ctx,img,data[node.id].content,data[node.id].flags) == 1) then
			parse_action(ctx,node,data,depth,curDepth)
			return true
		end
	
		return
	end
	
	if (nk.nk_contextual_item_label(ctx,data[node.id].content,data[node.id].flags) == 1) then
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end	
end

--[[
@				<group>...</group>
@	id:			Unique name
@ 	title:		Text
@	flags:		{
					NK_WINDOW_BORDER,
					NK_WINDOW_MOVABLE,
					NK_WINDOW_SCALABLE,
					NK_WINDOW_CLOSABLE,
					NK_WINDOW_MINIMIZABLE,
					NK_WINDOW_NO_SCROLLBAR,
					NK_WINDOW_TITLE,
					NK_WINDOW_SCROLL_AUTO_HIDE,
					NK_WINDOW_BACKGROUND,
					NK_WINDOW_SCALE_LEFT,
					NK_WINDOW_NO_INPUT
				}
@	action: 	functor to execute while active
@	init:		functor to execute first run
--]]
function group(ctx,node,data,depth,curDepth)
	errcheck("group",node,"id")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end
	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].title = node.title or ""
		data[node.id].flags = getFlags(node.flags) or 0
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	if (nk.nk_group_begin_titled(ctx,node.id,data[node.id].title,data[node.id].flags) == 1) then
		parse_action(ctx,node,data,depth,curDepth)
		curDepth:increment()
		return true
	end
end

function group_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
		nk.nk_group_end(ctx)
	end
end

--[[
@				<menubar>...</menubar>
--]]
function menubar(ctx,node,data,depth,curDepth)
	nk.nk_menubar_begin(ctx)
	curDepth:increment()
end

function menubar_end(ctx,node,data,depth,curDepth)
	curDepth:decrement()
	nk.nk_menubar_end(ctx)
end

--[[
@				<menu_label>...</menu_label>
@	id:			Unique name
@	title:		Text
@	w:			Pixel width
@	h:			Pixel height
@	flags:		{
					NK_TEXT_ALIGN_LEFT,
					NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_ALIGN_RIGHT,
					NK_TEXT_ALIGN_TOP,
					NK_TEXT_ALIGN_MIDDLE,
					NK_TEXT_ALIGN_BOTTOM,
							or
					NK_TEXT_LEFT        = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_LEFT,
					NK_TEXT_CENTERED    = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_CENTERED,
					NK_TEXT_RIGHT       = NK_TEXT_ALIGN_MIDDLE|NK_TEXT_ALIGN_RIGHT
				}
@	image:		[Optional] path or image atlas alias or image animation alias
@	action: 	functor to execute while active
@	init:		functor to execute first run
--]]
function menu_label(ctx,node,data,depth,curDepth)
	errcheck("menu_label",node,"id","w","h")
	if (current() ~= nil and data[node.id]) then
		data[node.id].bounds = get_bounds()
	end	
	if not (data[node.id]) then
		data[node.id] = {}
		data[node.id].image = getImage(node.image,data[node.id].image)
		data[node.id].title = core.translate(node.title) or " "
		data[node.id].w = tonumber(node.w) or 200
		data[node.id].h = tonumber(node.h) or 200
		data[node.id].flags = getFlags(node.flags) or nk.NK_TEXT_LEFT
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	local size = ffi.new("struct nk_vec2",{data[node.id].w,data[node.id].h})
	
	local image = data[node.id].image
	if (image) then
		local img = ffi.new("struct nk_image")
		img.handle = nk.nk_handle_id(image:getTextureID())
		
		local w,h = image:getTextureSize()
		img.w = w
		img.h = h
		
		local region = ffi.new("unsigned short[4]",{image:getRegion()})
		img.region = region	
		
		if (data[node.id].title) then
			if (nk.nk_menu_begin_image_label(ctx,data[node.id].title,data[node.id].flags,img,size) == 1) then
				curDepth:increment()
				parse_action(ctx,node,data,depth,curDepth)
				return true
			end
		elseif (nk.nk_menu_begin_image(ctx,node.id,img,size) == 1) then
			curDepth:increment()
			parse_action(ctx,node,data,depth,curDepth)
			return true
		end
		return
	end
	
	if (nk.nk_menu_begin_label(ctx,data[node.id].title,data[node.id].flags,size) == 1) then
		curDepth:increment()
		parse_action(ctx,node,data,depth,curDepth)
		return true
	end
end

function menu_label_end(ctx,node,data,depth,curDepth)
	if (depth == curDepth.value - 1) then
		curDepth:decrement()
		nk.nk_menu_end(ctx)
	end
end

--[[
@				<style>...</style>
@	id:			Unique name
@	filename:	Path
@	init:		functor to execute first run
--]]
function style(ctx,node,data,depth,curDepth)
	errcheck("style",node,"id","filename")
	
	if not (data[node.id]) then
		data[node.id] = {}
		
		data[node.id].filename = node.filename or nil
		parse_init(ctx,node,data,depth,curDepth)
	end
	
	data[node.id].prev = core.interface.import.style._curStyle
	core.interface.import.style._curStyle = data[node.id].filename
	
	core.interface.import.style(data[node.id].filename)
	
	curDepth:increment()
end

function style_end(ctx,node,data,depth,curDepth)
	curDepth:decrement()
	core.interface.import.style._curStyle = data[node.id].prev
	
	core.interface.import.style(data[node.id].prev)
end

---------------------------------------------
-- Custom drawing
---------------------------------------------
--[[
@				<stroke_line>x1,y1,x2,y2,...</stroke_line>
@	r:			0-255 Red
@	g:			0-255 Green
@	b:			0-255 Blue
@	a:			0-255 Alpha
@	width:		Line width
@	Draw a line from point to point(s)
--]]
function stroke_line(ctx,node,data,depth,curDepth)
	local color = ffi.new("struct nk_color")
	color.r = tonumber(node.r) or 0
	color.g = tonumber(node.g) or 0
	color.b = tonumber(node.b) or 0
	color.a = tonumber(node.a) or 255
	local width = tonumber(node.width) or 1
	local pos = nk.nk_window_get_position(ctx)
	local buffer = nk.nk_window_get_canvas(ctx)
	
 	local px,py = nil,nil -- previous coord
	if (node.__content) then
		for x,y in string.gmatch(node.__content,"(%d+).-(%d+)") do
			x = tonumber(x) or 0
			y = tonumber(y) or 0
			if not (px) then 
				px,py = x,y
			else 
				nk.nk_stroke_line(buffer,pos.x+px,pos.y+py,pos.x+x,pos.y+y,width,color)
				px = x
				py = y
			end
		end
	end
end 

--[[
@				<stroke_curve>x1,y1,x2,y2</stroke_curve>
@	r:			0-255 Red
@	g:			0-255 Green
@	b:			0-255 Blue
@	a:			0-255 Alpha
@	mouse:	'true' or 'false' to follow mouse
@	width:		Line width
@	radius:		How far the curve can bend
@	Draw a curve from point to point
--]]
function stroke_curve(ctx,node,data,depth,curDepth)
	local color = ffi.new("struct nk_color")
	color.r = tonumber(node.r) or 0
	color.g = tonumber(node.g) or 0
	color.b = tonumber(node.b) or 0
	color.a = tonumber(node.a) or 255
	local radius = tonumber(node.radius) or 50
	local width = tonumber(node.width) or 1
	local pos = nk.nk_window_get_position(ctx)
	local buffer = nk.nk_window_get_canvas(ctx)
	local x1,y1,x2,y2 = string.match(node.__content or "","(%d+).-(%d+).-(%d+).-(%d+)")
	x1,y1,x2,y2 = tonumber(x1) or 0,tonumber(y1) or 0,tonumber(x2) or 0,tonumber(y2) or 0
	
	if (node.mouse == "true") then
		x2,y2 = core.mouse.position()
	end
	
	nk.nk_stroke_curve(buffer,pos.x+x1,pos.y+y1,pos.x+x1+radius,pos.y+y1,x2-radius,y2,x2,y2,width,color)
end 
---------------------------------------------
-- 					helpers
---------------------------------------------
function current()
	local cur = core.interface.backend.context().current
	return cur
end

function get_bounds()
	return nk.nk_widget_bounds(core.interface.backend.context())
end

function clearData(data)
	for k,v in pairs(data) do
		if (k ~= "__info" and k ~= "__scriptEnv" and k ~= "__args" and k ~= "__alias") then
			data[k] = nil
		end
	end
end

function errcheck(element,node,...)
	for i=1,select('#',...) do
		local field = select(i,...)
		if not (node[field]) then
			error(strformat("core.interface.import.prefab: %s missing field %s",element,field))
		end
	end
end

function getFlags(str)
	local flag = 0
	if (str) then
		-- XXX if were optimizing, here is good place to memoize
		--for s in str:gsplit("|",true) do
		for s in str:gmatch("([^|]+)") do
			if (nkEnum[s]) then
				flag = bit.bor(flag,nkEnum[s])
			end
		end
	end
	return flag
end

function getImage(path,storage)
	if (path) then
		local image_import = core.import.image
		local image = image_import.animation.exists(path) and image_import.animation(path) or image_import(path,nil,function(filename,image)
			storage = image
		end)
		return image
	end
end

-- info switches
-- parses string of infos separated by space (ex. "+info1 -info2 +info3")
-- minus denotes removal of info, plus denotes the addition of info
function parse_info(str,data)
	for s in string.gmatch(str,"([^%s.]+)") do
		local typ,info = str:sub(1,1),str:sub(2,str:len())
		if (typ == "-") then
			data.__info[info] = nil
		elseif (typ == "+") then 
			data.__info[info] = true
		end	
	end
end 

function parse_action(ctx,node,data,depth,curDepth)
	if not (node.action) then 
		return 
	end
	local script = data.__scriptEnv
	if (script and script[node.action]) then
		script[node.action](ctx,node,data,depth,curDepth)
	else
		parse_info(node.action,data)
	end
end

function parse_init(ctx,node,data,depth,curDepth)
	if not (node.init) then 
		return 
	end
	local script = data.__scriptEnv
	if (script and script[node.init]) then
		script[node.init](ctx,node,data,depth,curDepth)
	else
		parse_info(node.init,data)
	end
end

function parse_minimize(ctx,node,data,depth,curDepth)
	if not (node.minimize) then 
		return 
	end
	local script = data.__scriptEnv
	if (script and script[node.minimize]) then
		script[node.minimize](ctx,node,data,depth,curDepth)
	else
		parse_info(node.minimize,data)
	end
end

function parse_close(ctx,node,data,depth,curDepth)
	if not (node.close) then 
		return 
	end
	local script = data.__scriptEnv
	if (script and script[node.close]) then
		script[node.close](ctx,node,data,depth,curDepth)
	else
		parse_info(node.close,data)
	end
end