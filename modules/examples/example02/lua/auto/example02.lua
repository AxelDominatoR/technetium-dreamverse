function framework_run()
	CallbackSet("framework_update",framework_update)
	CallbackSet("framework_render",framework_render)
	CallbackSet("framework_load",framework_load,-9998) -- negative prior to ensure other modules load neccessary things
	CallbackSet("framework_mousemoved",framework_mousemoved)
	CallbackSet("framework_wheelmoved",framework_wheelmoved)
	CallbackSet("framework_keyboard",framework_keyboard)
end

function framework_load()
	-- load a camera prefab for the scene
	core.camera("default2D","prefab/camera/default_orthographic.camera")
	for i=1,10 do
		local ent = core.entity.create("spatial","sprite","persistable","toaderMovement")
		ent.alias = "toader"..i
		ent.component.sprite:image("toader_hop_front")
		ent.component.spatial:position(math.random(1,1000),math.random(1,1000))
	end
end

function framework_mousemoved(x,y,dx,dy)
	local cam = core.camera.active()
	if (cam) then
		cam:mousemoved(x,y,dx,dy)
	end
end

function framework_wheelmoved(dx,dy)
	local cam = core.camera.active()
	if (cam) then
		cam:wheelmoved(dx,dy)
	end
end

function framework_keyboard(key,aliases,scancode,action,mods)
	for id, ent in pairs(core.entity.all()) do
		if (ent.component.toaderMovement) then
			ent.component.toaderMovement:keyboard(key,aliases,scancode,action,mods)
		end
	end
end

function framework_update(dt,mx,my)
	for id, ent in pairs(core.entity.all()) do
		for name,t in pairs(ent.component) do
			if (t.update) then
				t:update(dt,mx,my)
			end
		end	
	end
end

function framework_render(winW,winH,fbW,fbH)
	local cam = core.camera.active("default2D")
	if not (cam) then
		return
	end

	cam:send(0) -- Set view and project matrix for view_id 0

	for id, ent in pairs(core.entity.all()) do
		ent.component.sprite:draw()
	end
end