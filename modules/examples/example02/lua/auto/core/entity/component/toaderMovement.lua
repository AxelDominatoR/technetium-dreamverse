-- core.entity.component.toaderMovement()
getmetatable(this).__call = function (self,entity)
	local t = {
		dependencies			= {"sprite"},
		active					= true,
		entity					= entity,
		update					= update,
		keyboard 				= keyboard,
		
		sfx = { 
			hop = core.import.sound("sound/hop.ogg") 
		}
	}
	
	return t
end

local hopTime = 0
local hopDir = 0

function update(self,dt,mx,my)
	if not (self.active) then
		return
	end
	if (time.time() > hopTime) then
		if (hopDir == 0) then
			self.entity.component.sprite:image("toader_idle_front")
		else
			self.entity.component.sprite:image("toader_idle_back")
		end
	end
end

function keyboard(self,key,aliases,scancode,action,mods)
	if not (self.active) then
		return
	end
	
	if not (action == glfw.RELEASE) then
		return
	end
	
	if not (aliases) then
		return
	end
	
	local x,y
	if (aliases.up) then
		y = -64
		self.entity.component.sprite:image("toader_hop_front")
		hopTime = time.time() + 0.20
		hopDir = 0
		self.sfx.hop:play()
	end
	if (aliases.down) then
		y = 64
		self.entity.component.sprite:image("toader_hop_back")
		hopDir = 1
		hopTime = time.time() + 0.20
		self.sfx.hop:play()
	end
	if (aliases.left) then
		x = -64
		if (hopDir == 0) then 
			self.entity.component.sprite:image("toader_hop_front")
		else 
			self.entity.component.sprite:image("toader_hop_back")
		end
		hopTime = time.time() + 0.15
		self.sfx.hop:play()
	end
	if (aliases.right) then
		x = 64
		if (hopDir == 0) then 
			self.entity.component.sprite:image("toader_hop_front")
		else 
			self.entity.component.sprite:image("toader_hop_back")
		end
		hopTime = time.time() + 0.15
		self.sfx.hop:play()
	end
	
	if (x or y) then
		local x1,y1 = self.entity.component.spatial:position()
		x1 = x1 + (x or 0)
		y1 = y1 + (y or 0)
		self.entity.component.spatial:position(x1,y1)
	end
end