local nk 				= require("ffi_nuklear")										; if not (nk) then error("failed to load nuklear") end

local style_list = nil
function generate_style_list(ctx,node,data,depth)
	if not (style_list) then
		style_list = {}
		table.insert(style_list,"default")
		filesystem.fileForEach("config/ui/styles",true,{"xml"},function(path,fname,fullpath)
			local theme = fname:sub(1,-5)
			table.insert(style_list,theme)
		end)
	end
	
	for i,v in ipairs(style_list) do 
		if (nk.nk_contextual_item_label(ctx,v,nk.NK_TEXT_LEFT) == 1) then
			data["combo_style"].selected = v
			if (v == "default") then
				core.interface.import.style()
			else 
				core.interface.import.style("config/ui/styles/"..v..".xml")
			end
		end	
	end
end 

function on_close(ctx,node,data,depth)
	if (node.id == "examples") then
		nk.nk_window_close(ctx,node.id)
		core.interface.import.prefab.remove(data.__alias)
	elseif (node.id == "skintest") then 
		data[node.id] = nil -- to reinit when shown again
	end
end

function button_label_press(ctx,node,data,depth)
	local p = data["button_color1"]
	if (p) then 
		p.color.r = math.random(1,255)
		p.color.g = math.random(1,255)
		p.color.b = math.random(1,255)
	end
end

function change_image(ctx,node,data,depth)
	local p = data["button_image1"]
	if not (p) then 
		return
	end
	if (data["combo_image1"].selected == "ico_lua") then
		p.image = core.import.image("ico_text")
		data["combo_image1"].selected = "ico_text"
		data["combo_image1"].image = p.image
	else 
		p.image = core.import.image("ico_lua")
		data["combo_image1"].selected = "ico_lua"
		data["combo_image1"].image = p.image
	end
end

function change_image2(ctx,node,data,depth)
	local p = data["button_image2"]
	if not (p) then 
		return
	end
	local t = {"ico_directory","ico_desktop","ico_default","ico_computer","ico_text","ico_music","ico_movie","ico_img","ico_home","ico_font","ico_lua"}
	local ico = t[ math.random(1,#t) ]
	data["combo_image1"].selected = ico
	data["combo_image1"].image = core.import.image(ico)
	p.image = core.import.image(ico)
end

function color_pick(ctx,node,data,depth)
	local p = data["color_pick1"]
	local p2 = data["combo2"]
	if not (p and p2) then 
		return
	end
	local col = p.color[0]
	local col2 = p2.color[0]
	col2.r = col.r*255
	col2.g = col.g*255
	col2.b = col.b*255
	col2.a = col.a*255
end 

function draw_rgba(ctx,node,data,depth)
	local p = data["color_pick1"]
	if not (p) then 
		return
	end
	local col = p.color[0]
	col.r = nk.nk_propertyf(ctx, "#R:", 0, col.r, 1.0, 0.01,0.005)
	col.g = nk.nk_propertyf(ctx, "#G:", 0, col.g, 1.0, 0.01,0.005)
	col.b = nk.nk_propertyf(ctx, "#B:", 0, col.b, 1.0, 0.01,0.005)
	col.a = nk.nk_propertyf(ctx, "#A:", 0, col.a, 1.0, 0.01,0.005)
	
	--core.framework.graphics.setBackgroundColor(col.r*255,col.g*255,col.b*255,col.a*255)
end

function popup1_hide(ctx,node,data,depth)
	data.__info["info_popup1"] = nil
	nk.nk_popup_close(ctx)
end

function combo1_select(ctx,node,data,depth)
	local p = data["combo1"]
	if not (p) then 
		return 
	end
	p.selected = content
	nk.nk_combo_close(ctx)
end