local nk 				= require("ffi_nuklear")										; if not (nk) then error("failed to load nuklear") end
local directory = ""					-- base directory, can't go higher; "" is PHYSFS root
local currentDirectory = directory		-- Current directory
local filter = nil						-- Table of extensions without period
local browserType = "fileSelect"		-- 'fileSelect' or 'SaveAs'
local recursive = true
local onSelect = nil

local files = nil
local selected = nil

function init(data)
	directory = data.__args.directory or directory
	currentDirectory = data.__args.currentDirectory or directory
	filter = data.__args.filter or filter
	browserType = data.__args.browserType or browserType
	recursive = data.__args.recursive == true
	
	onSelect = data.__args.onSelect or nil
	
	if (browserType == "SaveAs") then
		data.__info.info_save_as = true
	end
end

function on_close(ctx,node,data,depth)
	nk.nk_window_close(ctx,node.id)
	core.interface.import.prefab.remove(data.__alias)
end

function list_directories(ctx,node,data,depth)
	nk.nk_layout_row_dynamic(ctx,25,1)
	nk.nk_label(ctx,currentDirectory,nk.NK_TEXT_LEFT)
	
	nk.nk_layout_row_dynamic(ctx,25,4)
	
	if (currentDirectory ~= directory) then
		local t = {}
		for s in string.gmatch(currentDirectory,"(.-)/") do
			table.insert(t,s)
			s = s == "" and ".." or s
			if (nk.nk_button_label(ctx,s) == 1) then
				local path = ""
				for i,v in ipairs(t) do
					if (v ~= "") then
						path = path .. "/" .. v
					end
				end
				currentDirectory = path
				files = nil
			end
		end
	end
end

function list_directory_contents(ctx,node,data,depth)
	nk.nk_layout_row_dynamic(ctx,25,1)
	
	if not (data.__list_directory_contents) then 
		data.__list_directory_contents = {}
	end 
	
	local function create_button(fname,fullpath)
		if not (data.__list_directory_contents[fname]) then
			data.__list_directory_contents[fname] = get_image_by_ext(fname,fullpath)
		end
		local img = ffi.new("struct nk_image")
		local image = data.__list_directory_contents[fname]
		if (image) then
			img.handle = nk.nk_handle_id(image:getTextureID())
			img.w = 0
			img.h = 0
			img.region[0] = 0
			img.region[1] = 0
			img.region[2] = 0
			img.region[3] = 0
		end
		--return nk.nk_contextual_item_image_label(ctx,img,fname,nk.NK_TEXT_LEFT) == 1
		return nk.nk_button_image_label(ctx,img,fname,nk.NK_TEXT_LEFT) == 1
	end 
	
	files = files or filesystem.getDirectoryItems(currentDirectory)

	-- directories
	for i,fname in ipairs(files) do
		local fullpath = currentDirectory.."/"..fname
		
		if (filesystem.isDirectory(fullpath)) then
			if (create_button(fname,fullpath)) then
				currentDirectory = fullpath
				files = nil
			end
		end
	end
	
	files = files or filesystem.getDirectoryItems(currentDirectory)
	
	-- Files
	for i,fname in ipairs(files) do
		local fullpath = currentDirectory.."/"..fname
		
		if not (filesystem.isDirectory(fullpath)) then 
		
			if (filter) then
				for ii,ext in ipairs(filter) do
					if (fname:gsub("(.*)%.","") == ext) then
						if (create_button(fname,fullpath)) then
							if (browserType == "SaveAs") then
								if (data.save_as_filename) then
									data.save_as_filename.text = ffi.new("char[?]",data.save_as_filename.max,fname)
								end
							else
								selected = fullpath
								data.__info.info_confirm = true
							end
						end
					end 
				end 
			else
				if (create_button(fname,fullpath)) then
					if (browserType == "SaveAs") then
						if (data.save_as_filename) then
							data.save_as_filename.text = ffi.new("char[?]",data.save_as_filename.max,fname)
						end
					else
						selected = fullpath
						data.__info.info_confirm = true
					end
				end
			end
		end
	end
end

function save_as_confirm(ctx,node,data,depth)
	if (node.id == "save_as_confirm_ok") then
		local text = utils.trim(ffi.string(data.save_as_filename.text))
		if (text ~= "") then
			local ext = utils.get_ext(text)
			if (ext ~= "") then
				local fullpath = currentDirectory .. "/" .. text
				if (filesystem.exists(fullpath) and not filesystem.isDirectory(fullpath)) then
					selected = fullpath
					data.__info.info_confirm = true
				else
					if (onSelect) then
						onSelect(fullpath)
					end
					core.interface.import.prefab.remove(data.__alias)
				end
			elseif (filter) then
				selected = currentDirectory .. "/" .. text .. "." .. filter[1]
				data.__info.info_confirm = true
			end
		end
	elseif (node.id == "save_as_confirm_cancel") then
		core.interface.import.prefab.remove(data.__alias)
	end
end

function popup_confirm_update(ctx,node,data,depth)
	if (node.id == "popup_confirm_label") then
		data.popup_confirm_label.text = utils.trim_path(selected)
	end
end

function popup_confirm(ctx,node,data,depth)
	if (node.id == "popup_confirm_ok") then
		if (onSelect) then
			onSelect(selected)
		end
		core.interface.import.prefab.remove(data.__alias)
	end
	data.__info.info_confirm = nil
	nk.nk_popup_close(ctx)
end

-------------------------
-- helpers
-------------------------
function get_image_by_ext(fname,fullpath)
	local fileType = "ico_default"
	if (filesystem.isDirectory(fullpath)) then
		fileType = "ico_directory"
	else 
		local ext = utils.get_ext(fname)
		if (ext == "png" or ext == "gif" or ext == "tga" or ext == "jpg" or ext == "jpeg") then
			fileType = "ico_img"
		elseif (ext == "wav" or ext == "ogg") then
			fileType = "ico_music"
		elseif (ext == "ttf") then
			fileType = "ico_font"
		elseif (ext == "txt" or ext == "xml") then
			fileType = "ico_text"
		elseif (ext == "lua" or ext == "script" or ext == "scene") then
			fileType = "ico_lua"
		end
	end
	local image_import = core.import.image
	return image_import.animation.exists(fileType) and image_import.animation(fileType) or image_import(fileType)
end