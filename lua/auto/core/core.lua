local glfw = Libs.glfw

-------------------------------------------
-- API
-------------------------------------------
local _fps = 0
function getFPS()
	return _fps
end
-------------------------------------------
-- Callbacks
-------------------------------------------
function application_init()
	_G.Managers = {}

	local stages =
	{
		"module_init",
		"module_post_init",
		"module_load",
		"module_unload"
	}
	DeepCollect( stages )

	DeepCall("module_init")
	DeepCall("module_post_init")
	DeepCall("module_load")

	this.InputMan = _G.Managers.Input
	this.RenderMan = _G.Managers.Render
end

function application_run()
	-- framework_run callback
	DeepCall("framework_run")

	-- Save configuration
	core.config.save()
	CallbackSendOnce("framework_load")

	glfw.SetTime(0)
	local pt,frames,lastFrameTime = 0,0,0
	local function step()
		frames = frames + 1
		local tme = glfw.GetTime()
		if (tme - lastFrameTime >= 1) then
			_fps = frames
			frames = 0
			lastFrameTime = tme
		end

		local dt = tme - pt
		pt = tme
		return dt
	end

	-- Main application loop
	repeat
		InputMan.PollEvents()

		local dt = step() -- Calculate FPS and delta-time

--		CallbackSend("framework_update",dt,mouse_x,mouse_y)

-- if out of focus, don't send update (but check if there's a better way of pausing
-- the game when out of focus)
		CallbackSend("framework_update",dt)

--[[
		local winW,winH = window.size()
		local fbW,fbH = window.framebufferSize()

		CallbackSend("framework_render",winW,winH,fbW,fbH)

		glfw.SwapBuffers( wnd )
--]]
		CallbackSend("framework_render")

		CallbackSend("framework_end_frame")

		--collectgarbage("step")
		--time.sleep(0.001)
	until (glfw.WindowShouldClose( Managers.Windows.Windows.Main:Ref()) == 1)

	CallbackSendOnce("framework_exit")
end

function application_exit()
	DeepCall("module_unload")

	collectgarbage()
	collectgarbage()

	window.destroy()
	glfw.Terminate()
end