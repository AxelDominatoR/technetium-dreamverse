--[[
	Window class
--]]

Class("Window")
function Window:_init( ... )
	-- Configuration
	local config = core.config.data.window
	self.x = config and tonumber(config.x) or 0
	self.y = config and tonumber(config.y) or 0
	self.width = config and tonumber(config.width) or 1280
	self.height = config and tonumber(config.height) or 800
	local fullscreen = config and config.fullscreen == true or false

	local monitor = glfw.GetPrimaryMonitor()
	local mode = glfw.GetVideoMode(monitor)

	self.parent_window = self.parent_window or nil

--	glfw.WindowHint(glfw.CLIENT_API,glfw.NO_API)
	glfw.WindowHint(glfw.DOUBLEBUFFER,1)
	glfw.WindowHint(glfw.RED_BITS,mode.redBits)
	glfw.WindowHint(glfw.GREEN_BITS,mode.greenBits)
	glfw.WindowHint(glfw.BLUE_BITS,mode.blueBits)
	glfw.WindowHint(glfw.ALPHA_BITS,glfw.DONT_CARE)
	glfw.WindowHint(glfw.DEPTH_BITS,glfw.DONT_CARE)
	glfw.WindowHint(glfw.STENCIL_BITS,8)
	glfw.WindowHint(glfw.REFRESH_RATE,glfw.DONT_CARE)

	if (ffi.os == "OSX") then
		glfw.WindowHint(glfw.CONTEXT_VERSION_MAJOR,3)
		glfw.WindowHint(glfw.CONTEXT_VERSION_MINOR,2)
		glfw.WindowHint(glfw.OPENGL_FORWARD_COMPAT,1)
		glfw.WindowHint(glfw.OPENGL_PROFILE,glfw.OPENGL_CORE_PROFILE)
	end

	local wnd = nil
	if (fullscreen) then
		wnd = glfw.CreateWindow(self.width,self.height,"",monitor, self.parent_window )
	else
		wnd = glfw.CreateWindow(self.width,self.height,"",nil, self.parent_window )
		glfw.SetWindowPos(wnd,(mode.width-self.width)/2,(mode.height-self.height)/2)
	end

	if (wnd == 0) then
		glfw.Terminate()
		error("Glfw window creation failed")
	end

	glfw.MakeContextCurrent( wnd )

	self._wnd = wnd

	CallbackSend("framework_window_creation",wnd,self.width,self.height)

--[[
--	glfw.PollEvents() -- crash fix for OSX

	-- Setup callbacks
	glfw.SetWindowSizeCallback(wnd,function(wnd,width,height)
		CallbackSend("framework_resize",width,height)
	end)
	local last_button_click = glfw.GetTime()
	glfw.SetMouseButtonCallback(wnd,function(wnd,button,action,mods)
		local x,y = core.mouse.position()
		if (action == glfw.PRESS) then
			local dt = glfw.GetTime() - last_button_click
			if (dt > 0.02 and dt < 0.2) then
				core.mouse.is_double_click_down = true
				core.mouse.double_click_pos[1] = x
				core.mouse.double_click_pos[2] = y
			end
			last_button_click = glfw.GetTime()
		else
			core.is_double_click_down = false
		end
		CallbackSend("framework_mousebutton",x,y,button,action,mods)
	end)
	glfw.SetWindowCloseCallback(wnd,function(wnd)
		CallbackSend("framework_window_close")
	end)
	local last_x,last_y = core.mouse.position()
	glfw.SetCursorPosCallback(wnd,function(wnd,x,y)
		local dx,dy = x-last_x,y-last_y
		last_x,last_y = x,y
		CallbackSend("framework_mousemoved",x,y,dx,dy)
	end)
	glfw.SetCursorEnterCallback(wnd,function(wnd,entered)
		CallbackSend("framework_mouseentered",entered == 1)
	end)
	glfw.SetKeyCallback(wnd,function(wnd,key,scancode,action,mods)
		-- https://www.glfw.org/docs/latest/group__keys.html
		local aliases = core.keyboard.keyToAlias[key]
		CallbackSend("framework_keyboard",key,aliases,scancode,action,mods)
	end)
	glfw.SetDropCallback(wnd,function(wnd,count,paths)
		for i=1,count do
			local path = paths[ i-1 ]
			if (path ~= nil) then
				local fname = ffi.string(path)
				CallbackSend("framework_filedropped",fname)
			end
		end
	end)
	glfw.SetScrollCallback(wnd,function(wnd,ox,oy)
		core.mouse.scroll_pos[ 1 ] = core.mouse.scroll_pos[ 1 ] + ox
		core.mouse.scroll_pos[ 2 ] = core.mouse.scroll_pos[ 2 ] + oy
		CallbackSend("framework_wheelmoved",core.mouse.scroll_pos[ 1 ],core.mouse.scroll_pos[ 2 ],ox,oy)
	end)
	glfw.SetCharModsCallback(wnd,function(wnd,codepoint,mods)
		CallbackSend("framework_textinput_mods",codepoint,mods)
	end)
	glfw.SetCharCallback(wnd,function(wnd,codepoint)
		CallbackSend("framework_textinput",codepoint)
	end)
	glfw.SetJoystickCallback(function(id,event)
		if (event == glfw.CONNECTED) then
			core.joystick.connect(id)
		elseif (event == glfw.DISCONNECTED) then
			core.joystick.disconnect(id)
		end
	end)
	glfw.SetWindowMaximizeCallback(wnd,function(wnd,maximized)
		-- maximized is 0 for restored and 1 for maximized
		CallbackSend("framework_maximized",maximized)
	end)
--]]
end

function Window:Ref()
	return self._wnd
end

-- Get or Set window title
function Window:SetTitle( str )
	glfw.SetWindowTitle( self._wnd, str )
end

-- Get or Set window width and height
function Window:Size( w, h )
	if ( w or h ) then
		glfw.SetWindowSize( self._wnd, w, h )

		self.width = w
		self.height = h
	else
--		local _w, _h = ffi.new("int[1]"), ffi.new("int[1]")
--		glfw.GetWindowSize( self._wnd, _w, _h )
		return self.width, self.height
	end
end

-- Get or Set window position
function Window:Position( x, y )
	if ( x or y ) then
		glfw.SetWindowPos( self._wnd, x, y )

		self.x = x
		self.y = y
	else
--		local _x,_y = glfw.GetWindowPos(_window)
--		return _x,_y
		return self.x, self.y
	end
end

-- Get framebuffer width and height
function Window:FramebufferSize()
	local width = ffi.new('int[1]')
	local height = ffi.new('int[1]')
	glfw.GetFramebufferSize( self._wnd, width, height )
	return width[ 0 ], height[ 0 ]
end

function Window:Show()
	glfw.ShowWindow( self._wnd )
	CallbackSend("framework_window_show", self._wnd )
end

function Window:Hide()
	CallbackSend("framework_window_hide", self._wnd )
	glfw.HideWindow( self._wnd )
end

function Window:Destroy()
	if ( self._wnd ~= nil ) then
		CallbackSend("framework_window_destroy", self._wnd )
		glfw.DestroyWindow( self._wnd )
		self._wnd = nil
	end
end