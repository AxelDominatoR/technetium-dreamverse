--[[
	Window manager
--]]

local WindowManager =
{
	Windows =
	{
		Current = {}
	}
}

local glfw

function module_init()
	corelog.info("Initializing window manager")

	_G.Managers.Windows = WindowManager
	glfw = Libs.glfw
end

function module_load()
	local section = "window"
	core.config.set_default(section,"title","")
	core.config.set_default(section,"width",1280)
	core.config.set_default(section,"height",800)
	core.config.set_default(section,"msaa",0)
	core.config.set_default(section,"vsync",true)
	core.config.set_default(section,"fullscreen",false)

	local function error_cb( err, desc )
		corelog.error("GLFW ERROR: " .. err )
		corelog.error("GLFW ERROR: " .. ffi.string( desc ))
	end
	glfw.SetErrorCallback( error_cb )

	if ( glfw.Init() == 0 ) then
		corelog.fatal("glfw.Init() failed")
	end

	WindowManager.Add("Main", core.window.Window())
--	WindowManager.Add("Secondary", core.window.Window({ parent_window = WindowManager.Windows.Main:Ref()}))
	WindowManager.SetCurrent("Main")
end

function WindowManager.Add( name, window )
	WindowManager.Windows[ name ] = window
end

function WindowManager.SetCurrent( w )
	WindowManager.Windows.Current = WindowManager.Windows[ w ]
	glfw.MakeContextCurrent( WindowManager.Windows[ w ]:Ref())
end