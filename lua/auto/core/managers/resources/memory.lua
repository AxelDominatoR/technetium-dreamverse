--[[
	Memory resource submanager class
--]]

Class("Memory")
function Memory:_init( ... )
	self.name = self.name or "Resource"
	self.proto = self.proto or nil

	self.resources = self.resources or {}

	corelog.info("-   " .. self.name .. " submanager initialized")
end

function Memory:Add( uri, resource )
	self.resources[ uri ] = resource
end

function Memory:Remove( uri )
	self.resources[ uri ] = nil
end

function Memory:Get( uri, ... )
	local r = self.resources[ uri ]
	if ( r == nil ) then
		if ( self.proto ~= nil ) then
			r = self.proto( ... )
		end
	end

	if ( r == nil ) then
		corelog.error( self.name .. " manager: couldn't get \"" .. uri .. "\"!")
	end

	return r
end

function Memory:Load( uri, ... )
	local r = self.resources[ uri ]
	if ( r ~= nil ) then
		corelog.warning( self.name .. " manager: \"" .. uri .. "\" is already loaded!")
	end

	r = self.proto( ... )
	self:Add( uri, r )

	corelog.debug( self.name .. " manager loaded: \"" .. uri .. "\"")

	return r
end
