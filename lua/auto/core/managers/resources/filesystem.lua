--[[
	Filesystem resource submanager class
--]]

Class("FileSystem", "_P.memory.Memory")
function FileSystem:Get( path, do_cache )
	do_cache = do_cache or false
	local r = self.resources[ path ]
	if ( r == nil ) then
		local data, length = filesystem.read( path )
		if (( length == nil ) or ( length < 1 )) then
			corelog.error( self.name .. " manager failed to load \"" .. path .. "\"")
		else
			if ( self.proto ~= nil ) then
				r = self.proto( data, length )
			else
				r = { data = data, length = length }
			end

			corelog.debug( self.name .. " manager loaded: \"" .. path .. "\"")

			if ( do_cache == true ) then
				self:Add( path, r )
			end
		end
	end

	return r
end