--[[
	Resource manager

	to do/ideas:
	- Textures
	- OpenGL VAO/VBO
	- Music
	- Sound effects

	- cache all of the resources
	- return statistics as far as numbers of each resource utilized, cached,
	RAM usage, etc
	- check if files changed and reload automatically so you don't need to
	restart the game when testing textures/etc
	- duplicate resource conflict handling
	- resource pool
	- resource release/free memory
	- same interface for all resources (? maybe?)
	- resource streaming
	- threading

	- IMPORTANT: don't store resources directly into the manager, but only
	have pointers to them. This way you don't pass the resource to the caller
	but only the pointer. If you need to reload, you can reload the resource
	without having to reload all of the pointers

	- handle resource dependencies (if a resource depends on another one...)
	- handle missing/faulty resources gracefully: return dummy/default resources
	and handle errors
--]]

local ResourceManager = {}

function module_init()
	corelog.info("Initializing resource manager")

	_G.Managers.Resource = ResourceManager
end

function module_load()
	local FSM = filesystem.FileSystem
	local MM = memory.Memory

	ResourceManager.Sprites = MM({ name = "Sprites", proto = core.renderer.sprite.Sprite.Load })
	ResourceManager.Textures = FSM({ name = "Textures", proto = core.renderer.texture.Texture.Load })
	ResourceManager.VAOs = MM({ name = "VAO", proto = core.renderer.vertexarray.VertexArray })
	ResourceManager.VertexBufferLayouts = MM({ name = "VertexBufferLayouts", proto = core.renderer.vertexbufferlayout.VertexBufferLayout })
end