-- Storage layer that deals with texture information.
cache = {}

-- core.manager.assets.texture(alias)
getmetatable(this).__call = function (self,name)
	return cache[ name ]
end

function add(texture_info)
	cache[ texture_info.name ] = texture_info
end

function remove(texture_info)
	cache[ texture_info.name ] = nil
end