--[[
	Input manager
--]]

local InputManager = {}
function application_init()
	corelog.info("Initializing input manager")

	_G.Managers.Input = InputManager
end

function InputManager.PollEvents()
	glfw.PollEvents()

	CallbackSend("framework_input")

--	local mouse_x,mouse_y = mouse.position()
end