local lanes = Libs.lanes

-- Thread pool

maxThreads = 4
linda = nil

local threadPool = {}

-- core.thread.pool()
getmetatable(this).__call = function(self)
	return threadPool
end

function framework_run()
	if (maxThreads <= 0) then
		return
	end
	
	linda = lanes.linda()
	-- worker loop
	local function pool_worker(index,linda,package_path) -- Note: everything in this function is on a new lua state, except above local values lua lanes passes
		package.path = package_path
		_G.ffi = require("ffi")
		require("filesystem")
		require("global")
		_G.time = require("time")
		_G.identity = index
		_G.linda = linda
		
		set_finalizer(function(err,stk)
			--print("thread.pool",identity,": attempting to exit cleanly...")
			if err and type( err) ~= "userdata" then
				-- no special error: true error
				print("thread.pool",identity,": error: "..tostring(err))
			elseif type(err) == "userdata" then
				-- lane cancellation is performed by throwing a special userdata as error
				print("thread.pool",identity,": after cancel")
			else
				-- no error: we just got finalized
				--print("thread.pool",identity,": finalized")
			end
		end)

		repeat
			-- Execute a function popped from the linda queue
			local k,v = linda:receive(0,"pool-functor","pool-call")
			if (v ~= nil) then
				if (k == "pool-call") then
					if (type(v) == "table" and type(v[ 1 ]) == "string") then
						local str = v[ 1 ]
						local node = _G
						if (node) then
							local success = true
							for s in str:gmatch("[^$.]+") do -- support for embedded functor path (ie. "this.is.a.function")
								if (node[ s ]) then
									node = node[ s ]
								else
									success = false
									print("thread.pool",identity," function doesn't exist:",str)
									break
								end
							end
							if (success) then
								node(select(2, unpack(v)))
							end
						end
					elseif (type(v) == "string") then
						local node = _G
						if (node) then
							local success = true
							for s in v:gmatch("[^$.]+") do
								if (node[ s ]) then
									node = node[ s ]
								else
									success = false
									print("thread.pool",identity," function doesn't exist:",v)
									break
								end
							end
							if (success) then
								node()
							end
						end
					end
				elseif (k == "pool-functor") then
					if (type(v) == "function") then
						v()
					elseif (type(v) == "table" and type(v[ 1 ]) == "function") then
						v[ 1 ](select(2, unpack(v)))
					end
					--print("thread.pool",identity,"success")
				end
			end
			time.sleep(0.002)
		until (cancel_test())
	end
		
	for i=1, maxThreads do
		threadPool[i] = lanes.gen("*",pool_worker)(i,linda,package.path)
	end
	
	CallbackSet("framework_exit",framework_exit)
	CallbackSet("framework_update",framework_update)
end

function framework_update(dt,mx,my)
	k,v = linda:receive(0,"main-call","main-functor")
	if (v ~= nil) then
		-- A call to an existing function on main thread from a pool thread
		if (k == "main-call") then
			if (type(v) == "table" and type(v[ 1 ]) == "string") then
				local str = v[ 1 ]
				local node = _G
				if (node) then
					local success = true
					for s in str:gmatch("[^$.]+") do -- support for embedded functor path (ie. "this.is.a.function")
						if (node[ s ]) then
							node = node[ s ]
						else
							success = false
							print("thread.pool main function doesn't exist:",str)
							break
						end
					end
					if (success) then
						node(select(2, unpack(v)))
					end
				end
			elseif (type(v) == "string") then
				local node = _G
				if (node) then
					local success = true
					for s in v:gmatch("[^$.]+") do
						if (node[ s ]) then
							node = node[ s ]
						else
							success = false
							print("thread.pool main function doesn't exist:",v)
							break
						end
					end
					if (success) then
						node()
					end
				end
			end
		-- Execute a function popped from the linda queue that is meant specifically for the main thread
		elseif (k == "main-functor") then
			if (type(v) == "function") then
				v()
			elseif (type(v) == "table" and type(v[ 1 ]) == "function") then
				v[ 1 ](select(2, unpack(v)))
			end
		end
	end
	-- for i,thread in ipairs(threadPool) do
		-- if (thread.status ~= "running" and thread.status ~= "waiting") then
			-- print(i,thread.status)
		-- end
	-- end
end

function framework_exit()
	for i,thread in ipairs(threadPool) do
		thread:cancel(10)
	end
end