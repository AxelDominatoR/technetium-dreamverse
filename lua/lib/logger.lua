--
-- logger.lua
-- version 0.1.0-gr
--
-- Adapted by Axel DominatoR to fit GammaRay's needs
-- Improved by @flamendless
-- * made it more löve-oriented
-- * fixed indentations
-- * localized standard functions for efficiency
--

--[[
Copyright (c) 2016 rxi


Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

--]]

Logger =
{
	brackets = {'[', ']'},
	level = "trace",
	outfile = nil
}

function Logger:new( o )
	o = o or {}

	self.__index = self
	setmetatable( o, self )

	o:_init()
	return o
end

local ceil = math.ceil
local concat = table.concat
local date = os.date
local floor = math.floor
local format = string.format
local getinfo = debug.getinfo
local strlen = string.len
local strrep = string.rep
local open = io.open

local modes =
{
	{ name = "trace",   color = "\27[34m",   trace = true },
	{ name = "debug",   color = "\27[36m",   trace = false },
	{ name = "profile", color = "\27[35m",   trace = false },
	{ name = "info",    color = "\27[32m",   trace = false },
	{ name = "warning", color = "\27[33m",   trace = true },
	{ name = "error",   color = "\27[31m",   trace = true },
	{ name = "fatal",   color = "\27[1;31m", trace = true, terminate = true }
}

local levels = {}
local max_length = 0
for i, v in ipairs( modes ) do
	levels[ v.name ] = i
	local l = strlen( v.name )
	if ( l > max_length ) then
		max_length = l
	end
end

local round = function(x, increment)
	increment = increment or 1
	x = x/increment
	return (x > 0 and floor(x + 0.5) or ceil(x - 0.5)) * increment
end

local _tostring = tostring
local tostring = function(...)
	local t = {}
	for i = 1, select("#", ...) do
		local x = select(i, ...)
		if type(x) == "number" then
			x = round(x, 0.01)
		end
		t[#t + 1] = _tostring(x)
	end
	return concat(t, " ")
end

local function pad_right( text, width, symbol )
	symbol = symbol or ' '
	text = type( text ) == "string" and text or tostring( text )

	return strrep( symbol, width - strlen( text )) .. text
end

local function pad_left( text, width, symbol )
	symbol = symbol or ' '
	text = type( text ) == "string" and text or tostring( text )

	return text .. strrep( symbol, width - strlen( text ))
end

local function banner_top( l )
	l.output("+==============================================================================+")
end

local function banner_bottom( l )
	l.output("+==============================================================================+")
end

function Logger:_init()
	local brackets = self.brackets

	if self.outfile then
		local fp = open( CommandLine.workingDirectory .. self.outfile, 'w')
		fp:close()
	end

	for i, x in ipairs(modes) do
		self.output = function( to_screen, to_file )
			print( to_screen )

			if to_file then
				local fp = open( CommandLine.workingDirectory .. self.outfile, 'a')
				fp:write( to_file )
				fp:close()
			end
		end

		local nameupper = x.name:upper()
		self[x.name] = function(...)
			if i < levels[self.level] then return end

			local msg = tostring(...)
			local info = getinfo(2, "Sl")
			local lineinfo = ( x.trace ~= false ) and ("\n\t\t\t\t@" .. info.short_src .. ":" .. info.currentline ) or ""
			local str = format(
				"%s%s%s (%s) %s%s",
				self:ColorPrint( x.color, brackets[ 1 ]),
				self:ColorPrint( x.color, nameupper, max_length ),
				self:ColorPrint( x.color, brackets[ 2 ]),
				self:ColorPrint("\27[90m", date("%H:%M:%S")),
				msg,
				lineinfo
			)

			if self.outfile then
				local to_file = format(
					"[%-" .. max_length .. "s] (%s) %s: %s\n",
					nameupper,
					date(),
					lineinfo,
					msg
				)

				self.output( str, to_file )
			else
				self.output( str )
			end

			if ( x.terminate == true ) then
				os.exit()
			end
		end

		self.banner = function( ... )
-- If timestamp is required, maybe integrate it inside the banner as an option
--			self.info()
			banner_top( self )
			for _, line in ipairs({ ... }) do
				self.output('|  ' .. pad_left( line, 74 ) .. '  |')
			end
			banner_bottom( self )
		end

		self.banner_rtl = function( ... )
			banner_top( self )
			for _, line in ipairs({ ... }) do
				self.output('|  ' .. pad_right( line, 74 ) .. '  |')
			end
			banner_bottom( self )
--			self.info()
		end
	end
end

function Logger:ColorPrint( color, text, padding )
	if ( text == '') then
		return text
	end

	local padding = padding or 0
	local usecolor = self.usecolor or (( ffi.os ~= "Windows") or false )

	if ( padding > 0 ) then
		text = format("%-" .. padding .. "s", text )
	end

	if ( usecolor ~= false ) then
		text = color .. text .. "\27[0m"
	end

	return text
end

#if _E.DEBUG then
	corelog = Logger:new({ level = "debug", outfile = "core.log"})
	log = Logger:new({ level = "debug", outfile = "client.log", brackets = {'<', '>'}})
#else
	corelog = Logger:new({ level = "info", outfile = "core.log"})
	log = Logger:new({ level = "info", outfile = "client.log", brackets = {'<', '>'}})
#end