--[[
	Metatype
--]]

local __no_gc = ArrayOf({})

-- Handles the instantiation of a metatype.
local function handle_call( t, init_data, ... )
	-- This late initialization must stay here to give the metatype a chance
	-- to declare additional methods, as the metatable is immutable after
	-- calling ffi.metatype().
	if ( t.__inited == false ) then
		t.__inited = true

		local mt =
		{
			__index = t
		}

		local deinit = rawget( t, "_deinit")
		if ( deinit ~= nil ) then
			mt["__gc"] = deinit
		end

		local to_string = rawget( t, "_tostring")
		if ( to_string ~= nil ) then
			mt["__tostring"] = to_string
		end

		t.__metatype = ffi.metatype( t.__ctype, mt )
	end

	local O = t.__metatype()

	local params = {}

	-- If initialization data is provided, use it
	-- Named fields are copied directly
	-- Anonymous fields are passed in-order to __init()
	if type( init_data ) == "table" then
		for k, v in pairs( init_data ) do
			if ( type( k ) == "number") then
				params[ k ] = v
			else
				O[ k ] = v
			end
		end
	end

	-- maybe use __new instead?
	local init = rawget( t, "_init")
	init( O, unpack( params ))

	return O
end

local function Metatype( name, ctype )
	local MT =
	{
		__inited = false,
		__ctype = ctype
	}

	function MT:__allocate( id, ... )
		self[ id ] = ffi.new( ... )
		__no_gc[ self ][ id ] = self[ id ]
	end

	function MT:__cast( id, ... )
		self[ id ] = ffi.cast( ... )
		__no_gc[ self ][ id ] = self[ id ]
	end

	function MT:__free( id )
		self[ id ] = nil
		__no_gc[ self ][ id ] = nil

		if ( next( rawget( __no_gc, self )) == nil ) then
			__no_gc[ self ] = nil
		end
	end

	function MT:_destruct()
		__no_gc[ self ] = nil
	end

	-- Parse and then replace the cdef with the Metatype structure itself
	local caller = getfenv( 2 )
	if ( caller[ name ] == nil ) then
		corelog.error("Undeclared initializer for metatype: " .. name )
		return nil
	end

	ffi.cdef( caller[ name ])

	caller[ name ] = MT

	local mt =
	{
		__call = handle_call
	}
	setmetatable( MT, mt )

	return MT
end

return Metatype