--[[
	DeepCaller - recursive function calling management

	How to use:
	DeepCall("my_func") - will execute any "my_func" found in lua/auto
	DeepCall("my_func", cb ) - same as above, but cb will be called to parse the results of each "my_func"
	DeepCall("my_func", cb, arg1, arg2 ... ) - you can pass any number of args to my_func

	Before a DeepCall it's a good idea to call:
	DeepCollect("my_func", "my_other_func", ... )
	This will parse any lua file under lua/auto and cache the list of
	functions provided as arguments.
	Instead of passing a whole list of arguments, the first one can be an array:
	DeepCollect({"my_func", "my_other_func"}, "another_func", ... )

	DeepCall will still work even without an explicit DeepCollect, but it
	will not be optimized (as when DeepCollect is called with a list of
	parameters it will parse the lua file tree only once).

	DeepCalls can do dependency checks. For example:

	function application_init()
		if not SatisfiedDeps("core/managers/render") then return end
	...

	will check if "application_init()" has been called in core/managers/render
	already. If that's the case, then it will continue executing, otherwise
	it will return immediately and the application_init will be called again
	at the end of the call loop.
	This method is still a bit crude and there's a chance it will create deps
	resulting in never-ending loops.
--]]

local _calls = {}

function DeepCall( call, cb, ... )
	if ( _calls[ call ] == nil ) then
		corelog.profile("Automatically DeepCollecting " .. call )
		DeepCollect( call )
		if ( _calls[ call ] == nil ) then
			corelog.warning("Empty deepcall: " .. call )
			return
		end
	end

	local lcalls = {}
	for path, node in pairs( _calls[ call ]) do
		lcalls[ path ] = node
	end

	repeat
		for path, node in pairs( lcalls ) do
			local _deps_env =
			{
				_lcalls = lcalls,
				_missing_deps = false,
				ipairs = ipairs
			}
			setfenv( SatisfiedDeps, _deps_env )

			local res = node[ call ]( ... )

			if ( _deps_env._missing_deps == false ) then
				lcalls[ path ] = nil
				if ( cb ~= nil ) then
					cb( res )
				end
			end
		end
	until ( next( lcalls ) == nil )
end

local function _AddCall( call, node, path )
	if ( _calls[ call ] == nil ) then
		_calls[ call ] = {}
	end

	_calls[ call ][ path ] = node
end

function DeepCollect( tcall, ... )
	local calls
	if ( type( tcall ) == "table") then
		calls = tcall
		for _, c in pairs({ ... }) do
			table.insert( calls, c )
		end
	else
		calls = { tcall, ... }
	end

	local function iterator( path, fname, fullpath )
		-- Split paths and create tables for each directory
		local node = _G
		local namespace = "_G"
		local pp = path:sub(10) -- remove lua/auto/

		for s in pp:gmatch("([^/]+)") do
			if (s ~= "") then
				if (node[ s ]) then
					node = node[ s ]
					namespace = s
				end
			end
		end

		local sname = fname:sub(1,-5)
		if (sname ~= namespace) then
			node = node[ sname ]
		end

		if ( node ) then
			for _, n in pairs( calls ) do
				if ( node[ n ]) then
					_AddCall( n, node, pp .. '/' .. sname )
				end
			end
		end
	end

	filesystem.fileForEach("lua/auto", true, {"lua"}, iterator )
end

function SatisfiedDeps( ... )
	for _, d in ipairs({ ... }) do
		if ( _lcalls[ d ] ~= nil ) then
			_missing_deps = true
			return false
		end
	end

	return true
end

return false -- NbAxel: prevents require() from returning an error