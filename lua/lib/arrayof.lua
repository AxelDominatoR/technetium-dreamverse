--[[
	Array with automatic instancing
--]]

function ArrayOf( proto )
	local A = {}

	local mt =
	{
		__index = function( t, k )
				local data

				if ( type( proto ) == "string") then
					local ns
					for w in string.gmatch( proto, "[%w_]+") do
						ns = ns[ w ]
					end
					proto = ns
				end

				if ( is_class( proto ) == true ) then
					data = proto()
				else
					data = {}
					for i, v in pairs( proto ) do
						data[ i ] = v
					end
				end

				t[ k ] = data
				return data
			end
	}
	setmetatable( A, mt )

	return A
end

return ArrayOf