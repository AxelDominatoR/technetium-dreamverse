local ProtoClass =
{
	__class_name = "ProtoClass",
	__class_initialized = true
}

function ProtoClass:is_a( class )
	local c = self

	repeat
		if ( c == class ) then
			return true
		else
			c = c.__class_parent
		end
	until ( c == nil )

	return false
end

function ProtoClass:is_instance_of( class )
	local c = self

	repeat
		if ( c.__class_name == class.__class_name ) then
			return true
		else
			c = c.__class_parent
		end
	until ( c == nil )

	return false
end

function ProtoClass:Debug()
	corelog.banner("Debug class: " .. self.__class_name )
	corelog.debug("Parent class: " .. self.__class_parent.__class_name )
	for _, addon in pairs( self.__class_addons ) do
		corelog.debug("Addon: " .. addon.__class_name )
	end

	for k, v in pairs( self ) do
		corelog.debug( k, v )
	end
	corelog.banner(" ")
end

function is_class( o )
	if ( o.is_a == ProtoClass.is_a ) then
		return true
	else
		return false
	end
end

function set_auto_index( tbl, proto )
	local mt =
	{
		__index = function( t, k )
				local data
				if ( is_class( proto ) == true ) then
					data = proto()
				else
					data = {}
					for i, v in pairs( proto ) do
						data[ i ] = v
					end
				end

				t[ k ] = data
				return data
			end
	}
	setmetatable( tbl, mt )
end

local function deep_copy( src, dest )
	for k, v in pairs( src ) do
		if type( v ) == "table" then
			dest[ k ] = {}
			deep_copy( v, dest[ k ])
		else
			dest[ k ] = v
		end
	end
end

local function class_from_path( path, start )
	local ns = start or _G
	for w in string.gmatch( path, "[%w_]+") do
		if ( ns[ w ] ~= nil ) then
			ns = ns[ w ]
		else
			corelog.error("Invalid class path: ", path )
			return
		end
	end
	return ns
end

local function init_class( C, key )
	if ( rawget( C, "__class_initialized") ~= true ) then
		C.__class = C

		local env = rawget( C, "__class_environment")

		local P = rawget( C, "__class_parent")
		if ( type( P ) == "string") then
			P = class_from_path( P, env )
			rawset( C, "__class_parent", P )
		end
		init_class( P )

		local addons = {}
		for k, addon in pairs( rawget( C, "__class_addons")) do
			if ( type( addon ) == "string") then
				addons[ k ] = class_from_path( addon, env )
			else
				addons[ k ] = addon
			end

			init_class( addons[ k ])
		end
		rawset( C, "__class_addons", addons )

		local mt = getmetatable( C )
		if next( addons ) == nil then
			mt.__index = rawget( C, "__class_parent")
		else
			mt.__index = function( CC, key )
				local p_res = CC.__class_parent[ key ]
				if ( p_res ~= nil ) then
					return p_res
				end

				for _, addon in pairs( CC.__class_addons ) do
					local res = addon[ key ]
					if ( res ~= nil ) then
						return res
					end
				end
			end
		end
		setmetatable( C, mt )

		rawset( C, "__class_initialized", true )
	end

	-- This is to make this function work when called through __index
	if ( key ~= nil ) then
		return C[ key ]
	end
end

local function recursive_populate( O, P )
	if ( rawget( P, "__class_parent") ~= nil ) then
		recursive_populate( O, rawget( P, "__class_parent"))
	end

	if ( type( rawget( P, "__class_addons")) == "table") then
		for _, addon in pairs( rawget( P, "__class_addons")) do
			recursive_populate( O, addon )
		end
	end

	local initializer = rawget( P, "__class_initializer")
	if type( initializer ) == "table" then
		deep_copy( initializer, O )
	end
end

local function recursive_init( O, P, ... )
	if ( P.__class_parent ~= nil ) then
		recursive_init( O, P.__class_parent, ... )
	end

	if ( type( rawget( P, "__class_addons")) == "table") then
		for _, addon in pairs( rawget( P, "__class_addons")) do
			recursive_init( O, addon, ... )
		end
	end

	local init = rawget( P, "_init")
	if ( init ~= nil ) then
		init( O, ... )
	end
end

local function Class( name, parent, ... )
	local C = {}

	-- Decide whether to use the advanced metatable or not
	local advanced_mt = false

	local parent = parent or ProtoClass

	-- Copy addons before anything else, for safety:
	-- property conflicts should always be won by the initializer.
	local addons = { ... }
--[[
	for _, addon in pairs( addons ) do
		if ( type( addon ) == "string") then
			addon = class_from_path( addon, this )
		end

		deep_copy( addon, C )
		deep_copy( addon(), C )
	end
--]]

	-- Parse and then replace the initializer with the Class structure itself
	local caller = getfenv( 2 )
	if ( caller[ name ] ~= nil ) then
		local initializer = caller[ name ]
		C.__class_initializer = initializer

		-- Move __set_once to the Class itself for efficiency
		if ( initializer._set_once ~= nil ) then
			C.__set_once = initializer._set_once
			initializer._set_once = nil
			advanced_mt = true
		end
	end
	caller[ name ] = C

	-- deep will do a full copy for performance reasons, but you lose the
	-- ability to auto-update inherited methods

	-- WARNING! THIS DOESN'T WORK PROPERLY RIGHT NOW
	-- Rewrite it to be more fine-grained. Ideally choosing single methods
	-- to be deep-copied at runtime, which should be a much better solution
--[[
	if ( deep == true ) then
		for i, v in pairs( parent ) do
			C[ i ] = v
		end
	end
--]]

	-- This must be after the copy or else the fields will be overwritten
	C.__class_name = name  -- maybe only enable __class_name when debugging
	C.__class_parent = parent
	C.__class_addons = addons
	C.__class_environment = this

	local mt =
	{
		__index = init_class,
		__call = function( t, init_data, ... )
				local O = {}
				local params = {}

				init_class( t )

				-- Deep copy from the parents' initializers
				recursive_populate( O, t )

				-- If initialization data is provided, use it
				-- Named fields are copied directly
				-- Anonymous fields are passed in-order to __init()
				if type( init_data ) == "table" then
					for k, v in pairs( init_data ) do
						if ( type( k ) == "number") then
							params[ k ] = v
						else
							O[ k ] = v
						end
					end
				end

				if ( advanced_mt == true ) then
					local mt =
					{
						__index = function( ot, key )
							-- t = Class structure
							-- ot = instance

							if ( t[ key ] ~= nil ) then
								return t[ key ]
							else
								if ( t.__set_once[ key ] ~= nil ) then
									local res = t.__set_once[ key ]( ot )
									rawset( ot, key, res )
									return res
								end
							end
						end
					}

					setmetatable( O, mt )
				else
					setmetatable( O, { __index = t })
				end

				recursive_init( O, t, unpack( params ))

				return O
			end
	}
	setmetatable( C, mt )

	return C
end

return Class