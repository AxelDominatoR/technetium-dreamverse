-------------------------------------------------------------
-- Initialize some early environment (_E) structures
-------------------------------------------------------------

_E =
{
	DEBUG = nil,
	InitialG = {},
	RunModes =
	{
		RELEASE = 1,
		DEBUG = 2,
		CURRENT
	}
}

_E.Init = function ()
	for k, v in pairs( _G ) do
		_E.InitialG[ k ] = true
	end

	_E.RunModes.CURRENT = _E.RunModes.RELEASE
end

_E.ParseCmdArgs = function ()
	if ( CommandLine["DEBUG"] ~= nil ) then
		_E.DEBUG = true
	end
end

_E.ListDefinedGlobals = function ()
	for k, v in pairs( _G ) do
		if ( _E.InitialG[ k ] == nil ) then
			print( k .. ':' .. tostring( v ))
		end
	end
end

_E.Init()