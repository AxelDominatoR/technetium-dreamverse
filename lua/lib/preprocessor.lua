--[[
	Preprocessor

Usage:
- any line beginning with a hash (#) is considered a preprocessor instruction
#if DEBUG then
	function X( arg ) print( arg ) end
#else
	function X() end
#end

- preprocessor expressions can be inlined anywhere else in the code by encasing
  them in #()
#for i = 0, 10 do
	var#( i ) = #( i + i )
#end

--]]

-- Optimizations
local string_byte = string.byte
local string_char = string.char
local table_insert = table.insert
local table_concat = table.concat
local table_remove = table.remove

-- Variables used by the FSM
local _str_buffer = {}
local _passthru_buffer = {}
local _has_bracket = false
local _in_double_brackets = false
local _pdata = {}
local _prev_char
local _bracket_level = 0

--[[ Character types ]]--
local CTYPE_DEFAULT                = 1
local CTYPE_NUMBER                 = 2
local CTYPE_UPPERALPHA             = 3
local CTYPE_LOWERALPHA             = 4
local CTYPE_WHITESPACE             = 5
local CTYPE_TAB                    = 6
local CTYPE_NEWLINE                = 7
local CTYPE_MINUS                  = 8
local CTYPE_PERCENT                = 9
local CTYPE_BACKSLASH              = 10
local CTYPE_PERIOD                 = 11
local CTYPE_COMMA                  = 12
local CTYPE_HASH                   = 13
local CTYPE_COLON                  = 14
local CTYPE_SEMICOLON              = 15
local CTYPE_UNDERSCORE             = 16
local CTYPE_QUOTES                 = 17
local CTYPE_OPENROUNDBRACKET       = 18
local CTYPE_CLOSEDROUNDBRACKET     = 19
local CTYPE_OPENSQUAREBRACKET      = 20
local CTYPE_CLOSEDSQUAREBRACKET    = 21
local CTYPE_OPENCURLYBRACKET       = 22
local CTYPE_CLOSEDCURLYBRACKET     = 23
local CTYPE_OPERATOR               = 24
local CTYPE_EOF                    = 666

--[[ States ]]--
local state_generic            = { name = "state_generic"}
local state_hashing            = { name = "state_hashing"}
local state_hashline           = { name = "state_hashline"}
local state_hashexpr           = { name = "state_hashexpr"}

--[[ State stack ]]--
local _stack
local _state_default = state_generic
local _state = state_generic

local function fsm_init()
	_str_buffer = {}
	_passthru_buffer = {}
	_has_bracket = false
	_in_double_brackets = false
	_prev_char = nil
	_bracket_level = 0

	_stack = {}
	_state = state_generic
end

local function stack_peek()
	return _stack[ #_stack ]
end

local function stack_push( act )
	table_insert( _stack, act )
	_state = act
end

local function stack_pop()
	table_remove( _stack )
	_state = stack_peek() or _state_default
end

local function stack_swapontop( act )
	table_remove( _stack )
	table_insert( _stack, act )
	_state = act
end

--[[ State actions ]]--

-- Ignore character
local function stact_ignore() end

-- Clear the string buffer
local function stact_clear_buffer()
	_str_buffer = {}
end

-- Save character in the string buffer
local function stact_save_character( c )
	table_insert( _str_buffer, string_char( c ))
end

-- Passthru buffer stuff
local function queue_to_passthru( wrap )
	if ( wrap == true ) then
		local pre = ( _has_bracket == true ) and "[=[" or "[["
		local post = ( _has_bracket == true ) and "]=]" or "]]"

		local nl = ( _str_buffer[ 1 ] == "\n") and "\n" or ""

		table_insert( _passthru_buffer, pre .. nl .. table_concat( _str_buffer ) .. post )
	else
		table_insert( _passthru_buffer, table_concat( _str_buffer ))
	end

	_str_buffer = {}
	_has_bracket = false
end

local function flush_passthru()
	table_insert( _pdata, "_O(" .. table_concat( _passthru_buffer, ',') .. ")\n")
	_passthru_buffer = {}
end

--[[ State functions ]]--

-- state_generic
state_generic[ CTYPE_DEFAULT ] = stact_save_character
state_generic[ CTYPE_OPENSQUAREBRACKET ] =
	function( c )
		stact_save_character( c )
		_has_bracket = true

		if ( _prev_char == 91 ) then -- [
			_in_double_brackets = true
		end
	end
state_generic[ CTYPE_CLOSEDSQUAREBRACKET ] =
	function( c )
		stact_save_character( c )
		_has_bracket = true

		if ( _prev_char == 93 ) then -- ]
			_in_double_brackets = false
		end
	end
state_generic[ CTYPE_HASH ] =
	function()
		stack_push( state_hashing )
		return true
	end
state_generic[ CTYPE_EOF ] =
	function()
		queue_to_passthru( true )
		flush_passthru()
	end

-- state_hashing
state_hashing[ CTYPE_DEFAULT ] =
	function()
		table_insert( _str_buffer, '#')
		stack_pop()
		return true
	end
state_hashing[ CTYPE_HASH ] =
	function()
		-- Only parse a hashline if immediately after a newline and
		-- not inside double square brackets.
		if (( _prev_char == 10 ) or ( _prev_char == 13 ) or ( _prev_char == nil )) and ( _in_double_brackets == false ) then
			if ( _prev_char ~= nil ) then
				queue_to_passthru( true )
				flush_passthru()
			end

			stack_swapontop( state_hashline )
		end
	end
state_hashing[ CTYPE_OPENROUNDBRACKET ] =
	function()
		if ( _prev_char == 35 ) then -- #
			queue_to_passthru( true )

			stack_swapontop( state_hashexpr )
		end
	end

-- state_hashline
state_hashline[ CTYPE_DEFAULT ] = stact_save_character
state_hashline[ CTYPE_NEWLINE ] =
	function( c )
		table_insert( _pdata, table_concat( _str_buffer ) .. string_char( c ))
		_str_buffer = {}

		stack_pop()
	end
state_hashline[ CTYPE_EOF ] =
	function()
		table_insert( _pdata, table_concat( _str_buffer ))

		stack_pop()
	end

-- state_hashexpr
state_hashexpr[ CTYPE_DEFAULT ] = stact_save_character
state_hashexpr[ CTYPE_OPENSQUAREBRACKET ] =
	function( c )
		stact_save_character( c )
		_has_bracket = true

		if ( _prev_char == 91 ) then -- [
			_in_double_brackets = true
		end
	end
state_hashexpr[ CTYPE_CLOSEDSQUAREBRACKET ] =
	function( c )
		stact_save_character( c )
		_has_bracket = true

		if ( _prev_char == 93 ) then -- ]
			_in_double_brackets = false
		end
	end
state_hashexpr[ CTYPE_OPENROUNDBRACKET ] =
	function( c )
		stact_save_character( c )
		_bracket_level = _bracket_level + 1
	end
state_hashexpr[ CTYPE_CLOSEDROUNDBRACKET ] =
	function( c )
		if ( _bracket_level > 0 ) then
			_bracket_level = _bracket_level - 1
			stact_save_character( c )
		else
			queue_to_passthru( false )
			stack_pop()
		end
	end
state_hashexpr[ CTYPE_EOF ] =
	function()
		table_insert( _pdata, table_concat( _str_buffer ))
		stack_pop()
	end

local function fsm_iterate( data )
	fsm_init()

	local c, ctype, parse_again
	for i = 1, #data do
		c = string_byte( data, i, i + 1 )

		if     ( c == 32 )                                 then ctype = CTYPE_WHITESPACE
		elseif ( c >= 48 ) and ( c <= 57 )                 then ctype = CTYPE_NUMBER
		elseif ( c >= 97 ) and ( c <= 122 )                then ctype = CTYPE_LOWERALPHA
		elseif ( c >= 65 ) and ( c <= 90 )                 then ctype = CTYPE_UPPERALPHA
		elseif ( c == 44 )                                 then ctype = CTYPE_COMMA
		elseif ( c == 46 )                                 then ctype = CTYPE_PERIOD
		elseif ( c == 58 )                                 then ctype = CTYPE_COLON
		elseif ( c == 9 )                                  then ctype = CTYPE_TAB
		elseif ( c == 10 ) or ( c == 13 )                  then ctype = CTYPE_NEWLINE
		elseif ( c == 95 )                                 then ctype = CTYPE_UNDERSCORE
		elseif ( c == 35 )                                 then ctype = CTYPE_HASH
		elseif ( c == 45 )                                 then ctype = CTYPE_MINUS
		elseif ( c == 37 )                                 then ctype = CTYPE_PERCENT
		elseif ( c == 34 )                                 then ctype = CTYPE_QUOTES
		elseif ( c == 40 )                                 then ctype = CTYPE_OPENROUNDBRACKET
		elseif ( c == 41 )                                 then ctype = CTYPE_CLOSEDROUNDBRACKET
		elseif ( c == 91 )                                 then ctype = CTYPE_OPENSQUAREBRACKET
		elseif ( c == 93 )                                 then ctype = CTYPE_CLOSEDSQUAREBRACKET
		elseif ( c == 123 )                                then ctype = CTYPE_OPENCURLYBRACKET
		elseif ( c == 125 )                                then ctype = CTYPE_CLOSEDCURLYBRACKET
		elseif ( c == 92 )                                 then ctype = CTYPE_BACKSLASH
		elseif ( c == 43 ) or ( c == 45 ) or ( c == 61 )   then ctype = CTYPE_OPERATOR
		else                                                    ctype = CTYPE_DEFAULT
		end

		repeat
			if _state[ ctype ] == nil then
				parse_again = _state[ CTYPE_DEFAULT ]( c )
			else
				parse_again = _state[ ctype ]( c )
			end
		until parse_again ~= true

		_prev_char = c
	end

	repeat
		parse_again = _state[ CTYPE_EOF ]()
	until parse_again ~= true
end

local _pdata_head =
[[
local _ti = table.insert
local _tc = table.concat
local _R = {}

local function _O( ... )
	_ti( _R, _tc({ ... }))
end
]]

-- Important! That empty line before return is needed, don't remove it!
-- If removed, any # statement at the very end of a file will be touching
-- the return statement without white space/new lines in between
local _pdata_tail =
[[

return table.concat( _R )
]]

function preprocessor_parse( data )
	_pdata = {} -- empty the result array
	table_insert( _pdata, _pdata_head )
	fsm_iterate( data ) -- _pdata is filled here
	table_insert( _pdata, _pdata_tail )

	local res, err = loadstring( table_concat( _pdata ))
	if ( res == nil ) then
		print( err )
	end

	return res()
end

--[[
	Install a temporary custom loader to preprocess "require"s until the
	proper loader is in place
]]--

local function pp_loader( fname )
	local path = package.searchpath( fname, package.path )
	if ( path == nil ) then
		return
	end

	local f = assert( io.open( path, 'r'))

	local res, err = loadstring( preprocessor_parse( f:read("*all")))
	if ( res == nil ) then
		print("ERROR while pp_loading: " .. fname )
		print( err )
	end

	f:close()

	return res
end

table.insert( package.loaders, 2, pp_loader )