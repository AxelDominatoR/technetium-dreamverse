-------------------------------------------------------------
-- Initialize third-party libraries into global namespaces
-------------------------------------------------------------

local _require = require
require = function( modname )
	local mod = _require( modname )
	if ( mod ~= true ) then
		return mod
	else
		corelog.error("Failed to load module:", modname )
	end
end

Class = require("class")
ArrayOf = require("arrayof")
Metatype = require("metatype")

require("deepcaller") -- Used to recursively call functions in lua files

local function Load( libname, L, cdefs )
	if type( libname ) == "table" then
		libname = libname[ ffi.os ]
	end

	L.__lib = ffi.load( libname, false )
	ffi.cdef( cdefs )

	local mt =
	{
		__index = function( t, key ) return t.__lib[ key ] end
	}

	setmetatable( L, mt )

	return L
end

_G.Libs =
{
	Load = Load,

	dbg          = require("debugger"),
	kazmath      = require("ffi_kazmath"),
	glfw         = require("ffi_glfw"),
	-- bgfx      = require("bgfx")
	-- nuklear   = require("nuklear")
	physfs       = require("ffi_physfs"),
	lanes        = require("lanes").configure(),
	time         = require("time")
}
