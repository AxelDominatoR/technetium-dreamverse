-------------------------------------------------------------
-- Copyright 2019
-------------------------------------------------------------
IDENTITY_ORGANIZATION = "ToadRiderStudios"
IDENTITY_APPLICATION = "ToadEngine"
-------------------------------------------------------------
-- Initialize FFI as global
-------------------------------------------------------------
ffi = require("ffi")
bit = require("bit")

-------------------------------------------------------------
-- Initialize app environment
-------------------------------------------------------------
require("lib/appenv")

-------------------------------------------------------------
-- Restructure the CommandLine arguments
-------------------------------------------------------------
CommandLine = {}
do
	local last_key = nil
	for _, a in ipairs( arg ) do
		local first = a:sub( 1, 1 )
		if ( first == '-') then
			last_key = a:sub( 2 )
			CommandLine[ last_key ] = true
		else
			if ( last_key ~= nil ) then
				if ( first == '{') then
					local val_r = {}
					local val_i = 1
					for val in string.gmatch( a:sub( 2, -2 ), "%w+") do
						val_r[ val_i ] = val
						val_i = val_i + 1
					end

					CommandLine[ last_key ] = val_r
				else
					CommandLine[ last_key ] = a
				end

				last_key = nil
			else
				CommandLine[ a ] = true
			end
		end
	end
end

_E.ParseCmdArgs()

if ( jit.os == "Windows" ) then
	-- Get working directory
	local workingdir = arg[ 1 ]

	-- fix path by adding a final slash
	if (workingdir:match("^.*()[\\/]") < workingdir:len()) then
		workingdir = workingdir .. "/"
	end

	CommandLine.workingDirectory = workingdir:gsub("\\","/")

	-- Set DLL directory (necessary for ffi.load)
	ffi.cdef[[
		int __stdcall SetDllDirectoryA(const char* lpPathName);
	]]

	ffi.C.SetDllDirectoryA(workingdir .. "bin")

	-- lib and include pathes
	package.path = "lua/lib/?.lua;lua/?.lua;lua/lib/?/init.lua;"

	package.cpath = workingdir .. "bin\\?.dll;"				.. package.cpath
	package.cpath = workingdir .. "bin\\loadall.dll;"		.. package.cpath
elseif (ffi.os == "Linux") then
	-- Get working directory
	local workingdir = arg[ 1 ]

	-- fix path by adding a final slash
	if (workingdir:match("^.*()[\\/]") < workingdir:len()) then
		workingdir = workingdir .. "/"
	end

	CommandLine.workingDirectory = workingdir:gsub("\\","/")

	-- Add Windows LUA_LDIR paths
	local ldir = "./?.lua;!lua/?.lua;!lua/?/init.lua;"
	package.path = package.path:gsub("^%./%?%.lua;",ldir)
	package.path = package.path:gsub("!",workingdir)

	-- lib and include pathes
	package.path = "lua/lib/?.lua;lua/?.lua;lua/lib/?/init.lua;"

	package.cpath = "bin/?.so;"			.. package.cpath
	package.cpath = "bin/loadall.so;"	.. package.cpath
end

-------------------------------------------------------------
-- Initialize early modules
-------------------------------------------------------------
require("preprocessor")
require("logger")

-------------------------------------------------------------
-- Initialize the filesystem (PhysicsFS)
-------------------------------------------------------------
require("filesystem")

VFS_PATHS = {}
do
	if not (filesystem.init("",true)) then
		corelog.fatal("filesystem.init",filesystem.getLastError())
		return
	end

	-- Temporarily mount /modules in order to look for vfs.xml in subdirs
	filesystem.mount("modules", "modules", true )

	local modules = {}
	local function it( dir, fname, fullpath, ... )
		local m_name = string.match( dir, ".*/(.+)$")
		modules[ m_name ] = dir
	end
	filesystem.find("modules", {"vfs.xml"}, 2, it )
	filesystem.unmount("modules")

	-- load virtual file system mounting points from xml (default is vfs.xml)
	local vfs_path = "vfs.xml" -- Can pass path through to command line. (ie. -vfs vfs_custom)
	local vfs_req = CommandLine["vfs"]
	if ( vfs_req ) then
		if ( modules[ vfs_req ] ~= nil ) then
			vfs_path = modules[ vfs_req ] .. "/vfs.xml"
		else
			vfs_path = vfs_req .. ".xml"
		end
	end

	local f = assert(io.open(vfs_path,"r"))
	local data = f:read("*all")
	f:close()

	local expat = require("ffi_expat")
	local vfs = expat.luaparse(data,data:len())

	-- mount
	for i,node in ipairs(vfs.xml[1].module) do
		local t = { node[ 1 ].name, node[ 1 ].path, node[ 1 ].mount, node[ 1 ].relative == "true" }
		table.insert(VFS_PATHS,t)
		filesystem.mount(t[ 2 ],t[ 3 ] or "/",t[ 4 ])
	end

	-- Mount appdata/user directory as write path
	local appdata_path = filesystem.getAppdataDirectory()
	filesystem.setWriteDirectory(appdata_path)
	filesystem.mkdir(IDENTITY_ORGANIZATION)
	filesystem.setWriteDirectory(appdata_path..IDENTITY_ORGANIZATION.."/")
	filesystem.mkdir(IDENTITY_APPLICATION)
	filesystem.setWriteDirectory(appdata_path..IDENTITY_ORGANIZATION.."/"..IDENTITY_APPLICATION.."/")
	filesystem.mount(appdata_path..IDENTITY_ORGANIZATION.."/"..IDENTITY_APPLICATION.."/","/",false)
end