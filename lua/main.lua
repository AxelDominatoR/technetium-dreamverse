require("init")
require("lib")
require("global")
require("environment")

----------------------------------------------------------
-- Collect application callbacks
----------------------------------------------------------

local stages =
{
	"register_callbacks",
	"application_init",
	"application_run",
	"application_exit"
}
DeepCollect( stages )

local function pcb( cbs )
	for n, cb in pairs( cbs ) do
		CallbackSet( n, cb )
	end
end

DeepCall("register_callbacks", pcb )
--CallbackPrint()

----------------------------------------------------------
-- Trigger application callbacks
----------------------------------------------------------
do
	-- touch all files so they are compiled

	-- application_init callback
	DeepCall("application_init")
	-- application_run callback
	DeepCall("application_run")
	-- application_exit callback
	DeepCall("application_exit")
end

print("application exited successfully...")